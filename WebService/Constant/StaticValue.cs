﻿//using Ado.Mssql.Table;
using Ado.Mssql.Table;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace WebService.Constant
{
    public class StaticValue
    {
        private List<Model.Table.Mssql.mastertAccessToken> _mastertAccessToken;
        public List<Model.Table.Mssql.mastertAccessToken> mastertAccessToken { get { return _mastertAccessToken; } }

        private List<Model.Table.Mssql.mastertToken> _mastertToken;
        public List<Model.Table.Mssql.mastertToken> mastertToken { get { return _mastertToken; } }

        private List<Model.Table.Mssql.master_s_user> _master_s_user;
        public List<Model.Table.Mssql.master_s_user> master_s_user { get { return _master_s_user; } }

        private static StaticValue instant { get; set; }

        public static StaticValue GetInstant()
        {
            if (instant == null) instant = new StaticValue();
            return instant;
        }

        public void LoadInstantAll()
        {
            this.AccessKey();
            this.TokenKey();
            this.UserData();
        }

        internal dynamic GetUserDetail(object updateBy)
        {
            throw new NotImplementedException();
        }

        public void AccessKey()
        {
            this._mastertAccessToken?.Clear();
            this._mastertAccessToken = AccessToken.GetInstant().ListActive();
        }

        public void TokenKey()
        {
            //this._mastertToken?.Clear();
            var tmp = Token.GetInstant().ListActive();
            if (this._mastertToken?.Count > 0)
            {
                this._mastertToken.Where(t => !tmp.Any(v => v.Code == t.Code)).ToList().ForEach(v => this._mastertToken.Remove(v));
                tmp.Where(t => !this._mastertToken.Any(v => v.Code == t.Code)).ToList().ForEach(v => this._mastertToken.Add(v));
            }
            else
            {
                this._mastertToken = tmp;
            }
        }

        public void UserData()
        {
            this._master_s_user?.Clear();
            this._master_s_user = new List<Model.Table.Mssql.master_s_user>();
            Ado.Mssql.Table.User.GetInstant().Search(new Model.Request.User.UserSearchReq()).ForEach(x =>
            {
                this._master_s_user.Add(new Model.Table.Mssql.master_s_user()
                {
                    USERNAME = x.USERNAME,
                    PASSWORD = x.PASSWORD,
                    EMPID = x.EMPID,
                    EMPNAME = x.EMPNAME,
                    CODPOS = x.CODPOS,
                    CODPOSNAME = x.CODPOSNAME,
                    CODCOMP = x.CODCOMP,
                    CODCOMPNAME = x.CODCOMPNAME,
                    USER_ID = x.USER_ID,
                    USER_DATE = x.USER_DATE,
                    Expired_date = x.Expired_date,
                    Day_Expired = x.Day_Expired
                });
            });
        }

    }
}
