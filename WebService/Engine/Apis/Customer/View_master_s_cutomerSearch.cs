﻿using Model.Request.Customer;
using Model.Response;
using Model.Response.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Customer
{
    public class View_master_s_cutomerSearch : Base<View_master_s_cutomerSearchReq>
    {
        public View_master_s_cutomerSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_cutomerSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_cutomerSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Customer.master_v_customer.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_cutomerSearchRes() 
                {
                    cuscod = x.CUSCOD,
                    tname = x.TNAME,
                    fname = x.FNAME,
                    lname = x.LNAME,
                    shname = x.SHNAME,
                    acczone = x.ACCZONE,
                    countrycode = x.COUNTYYCODE,
                    saleofficer = x.SALEOFFICER,
                    areamngid = x.AREAMNGID,
                    assisid = x.ASSISID,
                    typecus = x.TYPECUS,
                    tsale = x.TSALE,
                    dstk_flag = x.DSTK_FLAG,
                    active = x.ACTIVE,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }
}
