﻿using Model.Request.GetLabelCode;
using Model.Response;
using Model.Response.GetLabelCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.GetLabelCode
{
    public class Master_s_LabelPaperTypeSearch : Base<Master_s_LabelPaperTypeSearchReq>
    {
        public Master_s_LabelPaperTypeSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_LabelPaperTypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_LabelPaperTypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.GetLabelCode.master_s_labelpapertype.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_LabelPaperTypeSearchRes
                {
                    labelpapertype_cd = x.labelpapertype_cd,
                    labelpapertype_id = x.labelpapertype_id,
                    labelpapertype_name = x.labelpapertype_name
                });
            });
        }

    }
}
