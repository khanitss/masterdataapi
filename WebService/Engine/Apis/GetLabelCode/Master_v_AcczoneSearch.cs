﻿using Model.Request.GetLabelCode;
using Model.Response;
using Model.Response.GetLabelCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.GetLabelCode
{
    public class Master_v_AcczoneSearch : Base<Master_v_AcczoneSearchReq>
    {
        public Master_v_AcczoneSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_v_AcczoneSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_v_AcczoneSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.GetLabelCode.master_v_acczone.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_v_AcczoneSearchRes
                {
                    DESC1 = x.DESC1,
                    MAINZONE = x.MAINZONE,
                    REGZONECODE = x.REGZONECODE,
                    ZONECODE = x.ZONECODE
                });
            });
        }

    }
}
