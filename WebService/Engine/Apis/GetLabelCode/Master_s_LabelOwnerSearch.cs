﻿using Model.Request.GetLabelCode;
using Model.Response;
using Model.Response.GetLabelCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.GetLabelCode
{
    public class Master_s_LabelOwnerSearch : Base<Master_s_LabelOwnerSearchReq>
    {
        public Master_s_LabelOwnerSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_LabelOwnerSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_LabelOwnerSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.GetLabelCode.master_s_labelowner.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_LabelOwnerSearchRes
                {
                    labelowner_id = x.labelowner_id,
                    labelowner_name = x.labelowner_name
                });
            });
        }

    }
}
