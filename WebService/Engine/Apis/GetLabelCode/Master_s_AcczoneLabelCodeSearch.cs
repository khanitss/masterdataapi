﻿using Model.Request.GetLabelCode;
using Model.Response;
using Model.Response.GetLabelCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.GetLabelCode
{
    public class Master_s_AcczoneLabelCodeSearch : Base<Master_s_AcczoneLabelCodeSearchReq>
    {
        public Master_s_AcczoneLabelCodeSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_AcczoneLabelCodeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_AcczoneLabelCodeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.GetLabelCode.master_acczonelabelcode.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_AcczoneLabelCodeSearchRes
                {
                    acczone = x.acczone,
                    acczonelabelcode_cd = x.acczonelabelcode_cd,
                    acczonelabelcode_id = x.acczonelabelcode_id,
                    acczonelabelcode_name = x.acczonelabelcode_name
                });
            });
        }

    }
}
