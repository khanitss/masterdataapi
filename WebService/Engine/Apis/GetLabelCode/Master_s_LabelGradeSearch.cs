﻿using Model.Request.GetLabelCode;
using Model.Response;
using Model.Response.GetLabelCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.GetLabelCode
{
    public class Master_s_LabelGradeSearch : Base<Master_s_LabelGradeSearchReq>
    {
        public Master_s_LabelGradeSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_LabelGradeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_LabelGradeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.GetLabelCode.master_s_labelgrade.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_LabelGradeSearchRes
                {
                    labelgrade_id = x.labelgrade_id,
                    labelgrade_cd = x.labelgrade_cd,
                    labelgrade_name = x.labelgrade_name
                });
            });
        }

    }
}
