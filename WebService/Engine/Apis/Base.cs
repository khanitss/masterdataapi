﻿using Core.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Model.Enum;
using Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebService.Constant;

namespace WebService.Engine.Apis
{
    public abstract class Base<TRequest>
    {
        protected string AccessToken { get; set; }
        protected string IPAddress { get; set; }
        protected string UserAgent { get; set; }

        //protected string HostBase { get; set; }

        protected string HostReq { get; set; }

        protected string Token { get; set; }


        protected string PermissionKey = "PUBLIC";
        protected bool AllowAnonymous = false;
        protected bool CheckVerify = false;

        protected SqlTransaction transac = null;

        //protected Model.Table.Mssql.master_s_user User { get; set; }

        protected int UserID { get; set; }

        protected abstract void ExecuteChild(TRequest dataReq, ResponseAPI dataRes);

        public T ExecuteResponse<T>(TRequest dataReq, int userID, SqlTransaction transac = null)
        {
            this.UserID = userID;
            ResponseAPI res = new ResponseAPI();
            this.transac = transac;
            this.ExecuteChild(dataReq, res);
            string json = JsonConvert.SerializeObject(res.data);
            return JsonConvert.DeserializeObject<T>(json);
            //return (T)res.data;
        }

        public ResponseAPI Execute(HttpContext context, dynamic dataReq = null)
        {
            ResponseAPI res = new ResponseAPI();

            StringValues HToken;
            context.Request.Headers.TryGetValue("Token", out HToken);
            Token = HToken.ToString();

            DateTime StartTime = DateTime.Now;
            string StackTraceMsg = string.Empty;
            try
            {
                StringValues HAccessToken;
                context.Request.Headers.TryGetValue("AccessToken", out HAccessToken);
                AccessToken = HAccessToken.ToString();

                StringValues HHostReq;
                context.Request.Headers.TryGetValue("Origin", out HHostReq);
                HostReq = HHostReq.ToString();
                IPAddress = context.Connection.RemoteIpAddress.ToString();
                StringValues HUserAgent;
                context.Request.Headers.TryGetValue("User-Agent", out HUserAgent);
                UserAgent = HUserAgent.ToString();

                if (!this.GetType().Name.Equals("OauthAccessTokenGet")) this.ValidatePermission();


                if (dataReq != null)
                {
                    try
                    {
                        dataReq = this.MappingRequest(dataReq);
                    }
                    catch (Exception)
                    {
                        dataReq = this.MappingRequestArr(dataReq);
                    }

                }

                this.ExecuteChild(dataReq, res);

                res.code = "S0001";
                res.message = "SUCCESS";
                res.status = "S";

            }
            catch (Exception ex)
            {
                StackTraceMsg = ex.StackTrace;
                //map error code, message
                ErrorCode error = EnumUtil.GetEnum<ErrorCode>(ex.Message);
                res.code = error.ToString();
                if (res.code == ErrorCode.U000.ToString())
                {
                    res.message = ex.Message;
                }
                else
                {
                    res.message = error.GetDescription();
                }

                res.status = "F";
            }
            finally
            {
            }
            return res;
        }

        private TRequest MappingRequest(dynamic dataReq)
        {
            string json = JsonConvert.SerializeObject(dataReq);
            return JsonConvert.DeserializeObject<TRequest>(json);
        }

        private List<TRequest> MappingRequestArr(dynamic dataReq)
        {
            string json = JsonConvert.SerializeObject(dataReq);
            return JsonConvert.DeserializeObject<List<TRequest>>(json);
        }

        private void ValidatePermission()
        {

            // check access token
            var accesskey = StaticValue.GetInstant().mastertAccessToken.Where(x => x.Code.Equals(AccessToken)).FirstOrDefault();
            if (accesskey == null) { throw new Exception(ErrorCode.O000.ToString()); }

            // check token
            if (!AllowAnonymous)
            {
                var token = StaticValue.GetInstant().mastertToken?.Where(x => x.Code.Equals(Token) && x.AccessToken_Code.Equals(AccessToken) && x.Type == Model.Enum.TokenType.Login.GetValueString()).FirstOrDefault();

                if (token == null) { throw new Exception(ErrorCode.O000.ToString()); }
                if (token.Status != "A") { throw new Exception(ErrorCode.O001.ToString()); }
                if (token.ExpiryTime.ToLocalTime().Ticks < DateTime.Now.Ticks) { throw new Exception(ErrorCode.O002.ToString()); }

                // set user id
                //UserID = token.User_ID;

                if (!PermissionKey.Equals("PUBLIC"))
                {
                    //if (!PermissionADO.GetInstant().CheckPermission(this.token, "C", this.PermissionKey).Any(x => x.Code == this.PermissionKey)) { throw new Exception(ErrorCode.P000.ToString()); }
                }

            }
        }
    }
}
