﻿using Model.Request.User;
using Model.Response;
using Model.Response.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.User
{
    public class RegisSearch : Base<RegisSearchReq>
    {
        public RegisSearch()
        {
            AllowAnonymous = true;
            PermissionKey = "PUBLIC";
        }


        protected override void ExecuteChild(RegisSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<UserSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.User.GetInstant().SearchRegis(dataReq).ForEach(x =>
            {
                res.Add(new UserSearchRes()
                {
                    USERNAME = x.USERNAME,
                    STAEMP = x.STAEMP,
                    Expired_date = x.Expired_date
                });
                }
            );
        }
    }
}
