﻿using Model.Response;
using Model.Response.User;
using Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.User
{
    public class UserSave : Base<UserSaveReq>
    {
        public UserSave()
        {
            AllowAnonymous = true;
            PermissionKey = "PUBLIC";
        }

        protected override void ExecuteChild(UserSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new UserSaveRes();
            dataRes.data = res;

            Model.Table.Mssql.User userSaveReq;

            var pass = Core.Util.EncryptUtil.Hash(dataReq.PASSWORD.Trim());

            Ado.Mssql.Table.User.GetInstant().Save(dataReq).ForEach(x =>
            {
                userSaveReq = new Model.Table.Mssql.User();
                userSaveReq.USERNAME = x.USERNAME;
                userSaveReq.PASSWORD = pass;
                userSaveReq.EMPID = x.EMPID;
                userSaveReq.EMPNAME = x.EMPNAME;
                userSaveReq.CODPOS = x.CODPOS;
                userSaveReq.CODPOSNAME = x.CODPOSNAME;
                userSaveReq.CODCOMP = x.CODCOMP;
                userSaveReq.CODCOMPNAME = x.CODCOMPNAME;
                userSaveReq.STAEMP = x.STAEMP;
                userSaveReq.USER_ID = x.USER_ID;
                userSaveReq.USER_DATE = x.USER_DATE;
                userSaveReq.Expired_date = x.Expired_date;
                userSaveReq.Day_Expired = x.Day_Expired;
            });

            res._status = "S";
            res._message = "SUCCESS";

        }
    }
}
