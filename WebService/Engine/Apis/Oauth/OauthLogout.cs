﻿using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Constant;

namespace WebService.Engine.Apis.Oauth
{
    public class OauthLogout : Base<dynamic>
    {
        public OauthLogout()
        {
            //AllowAnonymous = true;
        }
        protected override void ExecuteChild(dynamic dataReq, ResponseAPI dataRes)
        {
            Ado.Mssql.Table.Token.GetInstant().Delete(this.Token, UserID);
            StaticValue.GetInstant().TokenKey();

            var res = new ResponseAPI();
            dataRes.data = res;

            res.data = this.Token;
            res.status = "S";
            res.code = "DELETE Token 'n Change Status To 'C' ";
            res.message = "SUCCESS";
        }
    }
}
