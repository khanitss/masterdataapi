﻿using Core.Util;
using GoogleAuthenticatorService.Core;
using Model.Request;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Constant;

namespace WebService.Engine.Apis.Oauth
{
    public class OauthLogin : Base<OauthLoginReq>
    {
        public OauthLogin()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(OauthLoginReq dataReq, ResponseAPI dataRes)
        {
            var res = new OauthLoginRes();

            var user = Ado.Mssql.Table.User.GetInstant().Search(new Model.Request.User.UserSearchReq()
            {
                Username = new List<string>()
                {
                    dataReq.user_id.Trim()
                }
            }).FirstOrDefault();

            if (user == null)
            {
                throw new Exception(Model.Enum.ErrorCode.V006.ToString());
            }

            if (user.PASSWORD == Core.Util.EncryptUtil.Hash(dataReq.user_password))
            {
                res.token = dataReq.user_password.NewID();
                res.user_id = user.USER_ID;
                res.user_fullname = user.EMPNAME;

                Ado.Mssql.Table.Token.GetInstant().Insert(new Model.Table.Mssql.mastertToken()
                {
                    User_ID  = user.USER_ID,
                    AccessToken_Code = this.AccessToken,
                    Code = res.token,
                    Status = Model.Enum.Status.Active.GetValueString(),
                    Type = Model.Enum.TokenType.Login.GetValueString(),
                    ExpiryTime = DateTime.Now.AddHours(8)
                });


                dataRes.data = res;

                StaticValue.GetInstant().TokenKey();
            }
            else
            {
                throw new Exception(Model.Enum.ErrorCode.V010.ToString());
            }
        }
    }
}
