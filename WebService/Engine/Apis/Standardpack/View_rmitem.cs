﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class View_rmitem : Base<View_rmitemSerchbyItemCodeReq>
    {
        public View_rmitem()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_rmitemSerchbyItemCodeReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_itemSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.master_s_rmitem.GetInstant().SearchAllItemCode(dataReq).ForEach(x =>
            {
                res.Add(new View_itemSearchRes()
                {
                    ItemCode = x.ItemCode,
                    ItemName = x.ItemName
                });
            });
        }
    }
}
