﻿using Model.Request.Standardpack.CanvasnBarSpec;
using Model.Response;
using Model.Response.Standardpack.CanvasnBarSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBarSpec
{
    public class Master_s_CanvasnBarSpecMove : Base<canvasnbarspec_movemodel>
    {
        public Master_s_CanvasnBarSpecMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(canvasnbarspec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasnBarSpecMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec _Canvasnbarspec;
            foreach (var x in dataReq.canvasnbarspec_move)
            {
                var resp = new Model.Response.Standardpack.CanvasnBarSpec.Master_s_CanvasnBarSpecMoveRes();
                try
                {
                    _Canvasnbarspec = new Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec();
                    _Canvasnbarspec.canvasnbarspec_id = x.canvasnbarspec_id;
                    _Canvasnbarspec.user_id = x.user_id;
                    _Canvasnbarspec.user_name = x.user_name;

                    Ado.Mssql.Table.CanvasnBarSpec.master_s_canvasnbarspec.GetInstant().Move(_Canvasnbarspec);
                    resp.canvasnbarspec_id = x.canvasnbarspec_id;
                    resp.cancel_id = x.user_id;
                    resp.cancel_name = x.user_name;
                    resp._status = "S";
                    resp._message = "S : ทำรายการสำเร็จ (Success)";

                    res.Add(resp);

                }
                catch (Exception ex)
                {
                    resp._status = "F";
                    resp._message = ex.ToString();

                    res.Add(resp);
                }

            }
        }
    }
}
