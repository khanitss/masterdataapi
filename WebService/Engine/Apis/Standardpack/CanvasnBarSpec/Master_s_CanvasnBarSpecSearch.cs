﻿using Model.Request.Standardpack.CanvasnBarSpec;
using Model.Response;
using Model.Response.Standardpack.CanvasnBarSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebService.Engine.Apis.Standardpack.CanvasnBarSpec
{
    public class Master_s_CanvasnBarSpecSearch : Base<Master_s_CanvasnBarSpecSearchReq>
    {
        public Master_s_CanvasnBarSpecSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_CanvasnBarSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasnBarSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.CanvasnBarSpec.master_s_canvasnbarspec.GetInstant().SearchView(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_CanvasnBarSpecSearchRes()
                {
                    canvasnbarspec_id = x.canvasnbarspec_id,
                    canvasnbar_id = x.canvasnbar_id,
                    canvasnbar_cd = x.canvasnbar_cd,
                    canvasnbar_name = x.canvasnbar_name,
                    cus_cod = x.cus_cod,
                    cus_id = x.cus_id,
                    cus_name = x.cus_name,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    tsale = x.tsale,

                    producttype_code = x.producttype_code,
                    producttype_desc = x.producttype_desc,
                    productcate_grp = x.productcate_grp,
                    twinesize_max = x.twinesize_max,
                    twinesize_min = x.twinesize_min,
                    max_line_no = x.max_line_no,
                    max_productcate_grp = x.max_productcate_grp,
                    max_product_grp = x.max_product_grp,
                    max_product_type = x.max_product_type,
                    max_twine_no = x.max_twine_no,
                    max_word = x.max_word,
                    min_line_no = x.min_line_no,
                    min_productcate_grp = x.min_productcate_grp,
                    min_product_grp = x.min_product_grp,
                    min_product_type = x.min_product_type,
                    min_twine_no = x.min_twine_no,
                    min_word = x.min_word,
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name = x.label_name,

                    _status = "S",
                    _message = "S : ทำรายการสำเร็จ (success)"
                });
            });
        }
    }
}
