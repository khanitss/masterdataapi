﻿using Model.Request.Standardpack.CanvasnBarSpec;
using Model.Response;
using Model.Response.Standardpack.CanvasnBarSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBarSpec
{
    public class Master_s_CanvasnBarSpecSave : Base<Master_s_CanvasnBarSpecSaveReq>
    {
        public Master_s_CanvasnBarSpecSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_CanvasnBarSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_CanvasnBarSpecSaveRes();
            dataRes.data = res;

            try
            {
                int canvasnbarspec_id = Ado.Mssql.Table.CanvasnBarSpec.master_s_canvasnbarspec.GetInstant().Save(new Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec()
                {
                    canvasnbarspec_id = dataReq.canvasnbarspec_id,
                    canvasnbar_id = dataReq.canvasnbar_id,
                    canvasnbar_cd = dataReq.canvasnbar_cd,
                    canvasnbar_name = dataReq.canvasnbar_name,
                    cus_cod = dataReq.cus_cod,
                    cus_id = dataReq.cus_id,
                    cus_name = dataReq.cus_name,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name,
                    tsale = dataReq.tsale,

                    producttype_code = dataReq.producttype_code,
                    producttype_desc = dataReq.producttype_desc,
                    productcate_grp = dataReq.productcate_grp,
                    twinesize_max = dataReq.twinesize_max,
                    twinesize_min = dataReq.twinesize_min,
                    max_line_no = dataReq.max_line_no,
                    max_productcate_grp = dataReq.max_productcate_grp,
                    max_product_grp = dataReq.max_product_grp,
                    max_product_type = dataReq.max_product_type,
                    max_twine_no = dataReq.max_twine_no,
                    max_word = dataReq.max_word,
                    min_line_no = dataReq.min_line_no,
                    min_productcate_grp = dataReq.min_productcate_grp,
                    min_product_grp = dataReq.min_product_grp,
                    min_product_type = dataReq.min_product_type,
                    min_twine_no = dataReq.min_twine_no,
                    min_word = dataReq.min_word,
                    label_id = dataReq.label_id,
                    label_cd = dataReq.label_cd,
                    label_name = dataReq.label_name
                });

                res.canvasnbarspec_id = canvasnbarspec_id;
                res.canvasnbar_id = dataReq.canvasnbar_id;
                res.canvasnbar_cd = dataReq.canvasnbar_cd;
                res.canvasnbar_name = dataReq.canvasnbar_name;
                res.cus_id = dataReq.cus_id;
                res.cus_cod = dataReq.cus_cod;
                res.cus_name = dataReq.cus_name;
                res.effdate_st = dataReq.effdate_st;
                res.effdate_en = dataReq.effdate_en;
                res.status = dataReq.status;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.tsale = dataReq.tsale;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";

            }
            catch (Exception ex)
            {
                res.status = "F";
                res._message = ex.ToString();
            }

        }
    }
}
