﻿using Model.Request.Standardpack.FoldSpec;
using Model.Response;
using Model.Response.Standardpack.FoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecSearchCancel : Base<Master_s_FoldSpecSearchReq>
    {
        public Master_s_FoldSpecSearchCancel()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_FoldSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_FoldSpecSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.FoldSpec.master_u_foldspec.GetInstant().SearchCancelByID(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_FoldSpecSearchCancelRes()
                {
                    foldspec_id = x.foldspec_id,
                    fold_id = x.fold_id,
                    color_code = x.color_code,
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    depamt_max = x.depamt_max,
                    depamt_min = x.depamt_min,
                    depsize_max = x.depsize_max,
                    depsize_min = x.depsize_min,
                    effdate_en = x.effdate_en,
                    effdate_st = x.effdate_st,
                    foldspec_information = x.foldspec_information,
                    fold_cd = x.fold_cd,
                    fold_name = x.fold_name,
                    knottype_code = x.knottype_code,
                    len_max = x.len_max,
                    len_min = x.len_min,
                    linetype_code = x.linetype_code,
                    max_line_no = x.max_line_no,
                    max_productcate_grp = x.max_productcate_grp,
                    max_product_grp = x.max_product_grp,
                    max_product_type = x.max_product_type,
                    max_twine_no = x.max_twine_no,
                    max_word = x.max_word,
                    min_line_no = x.min_line_no,
                    min_productcate_grp = x.min_productcate_grp,
                    min_product_grp = x.min_product_grp,
                    min_product_type = x.min_product_type,
                    min_twine_no = x.min_twine_no,
                    min_word = x.min_word,
                    new_color_code = x.new_color_code,
                    old_color_code = x.old_color_code,
                    pqgradepd = x.pqgradepd,
                    pqgrade_desc = x.pqgrade_desc,
                    producttype_code = x.producttype_code,
                    producttype_desc = x.producttype_desc,
                    status = x.status,
                    stdwei_max = x.stdwei_max,
                    stdwei_min = x.stdwei_min,
                    stretchingtype_code = x.stretchingtype_code,
                    tsale = x.tsale,
                    twinesize_max = x.twinesize_max,
                    twinesize_min = x.twinesize_min,
                    user_date = x.user_date,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    cancel_name = x.cancel_name,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date

                });
            });
        }
    }
}
