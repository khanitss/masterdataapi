﻿using Model.Request.Standardpack.FoldSpec;
using Model.Response;
using Model.Response.Standardpack.FoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecMove : Base<foldspec_movemodel>
    {
        public Master_s_FoldSpecMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(foldspec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_FoldSpecMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.FoldSpec.master_s_foldspec _S_Foldspec;
            foreach (var x in dataReq.foldspec_move)
            {
                var resp = new Model.Response.Standardpack.FoldSpec.Master_s_FoldSpecMoveRes();
                try
                {
                    _S_Foldspec = new Model.Table.Mssql.FoldSpec.master_s_foldspec();
                    _S_Foldspec.foldspec_id = x.foldspec_id;
                    _S_Foldspec.user_id = x.cancel_id;
                    _S_Foldspec.user_name = x.cancel_name;

                    Ado.Mssql.Table.FoldSpec.master_s_foldspec.GetInstant().Move(_S_Foldspec);
                    resp.foldspec_id = x.foldspec_id;
                    resp.cancel_id = x.cancel_id;
                    resp.cancel_name = x.cancel_name;

                    resp.status = "S";
                    resp.message = "S : ทำรายการสำเร็จ (Success)";
                    res.Add(resp);
                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.message = ex.ToString();
                    res.Add(resp);
                }

            }
        }
    }
}
