﻿using Model.Request.Standardpack.FoldSpec;
using Model.Response;
using Model.Response.Standardpack.FoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecSave : Base<Master_s_FoldSpecSaveReq>
    {
        public Master_s_FoldSpecSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_FoldSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_FoldSpecSaveRes();
            dataRes.data = res;


            try
            {
                int foldspec_id = Ado.Mssql.Table.FoldSpec.master_s_foldspec.GetInstant().Save(new Model.Table.Mssql.FoldSpec.master_s_foldspec()
                {
                    foldspec_id = dataReq.foldspec_id,
                    foldspec_information = dataReq.foldspec_information,
                    fold_id = dataReq.fold_id,
                    fold_cd = dataReq.fold_cd,
                    fold_name = dataReq.fold_name,
                    color_code = dataReq.color_code,
                    cus_cod = dataReq.cus_cod,
                    cus_name = dataReq.cus_name,
                    depamt_max = dataReq.depamt_max,
                    depamt_min = dataReq.depamt_min,
                    depsize_max = dataReq.depsize_max,
                    depsize_min = dataReq.depsize_min,
                    effdate_en = dataReq.effdate_en,
                    effdate_st = dataReq.effdate_st,
                    knottype_code = dataReq.knottype_code,
                    len_max = dataReq.len_max,
                    len_min = dataReq.len_min,
                    linetype_code = dataReq.linetype_code,
                    max_line_no = dataReq.max_line_no,
                    max_productcate_grp = dataReq.max_productcate_grp,
                    max_product_grp = dataReq.max_product_grp,
                    max_product_type = dataReq.max_product_type,
                    max_twine_no = dataReq.max_twine_no,
                    max_word = dataReq.max_word,
                    min_line_no = dataReq.min_line_no,
                    min_productcate_grp = dataReq.min_productcate_grp,
                    min_product_grp = dataReq.min_product_grp,
                    min_product_type = dataReq.min_product_type,
                    min_twine_no = dataReq.min_twine_no,
                    min_word = dataReq.min_word,
                    new_color_code = dataReq.new_color_code,
                    old_color_code = dataReq.old_color_code,
                    pqgradepd = dataReq.pqgradepd,
                    pqgrade_desc = dataReq.pqgrade_desc,
                    producttype_code = dataReq.producttype_code,
                    producttype_desc = dataReq.producttype_desc,
                    status = dataReq.status,
                    stdwei_max = dataReq.stdwei_max,
                    stdwei_min = dataReq.stdwei_min,
                    stretchingtype_code = dataReq.stretchingtype_code,
                    tsale = dataReq.tsale,
                    twinesize_max = dataReq.twinesize_max,
                    twinesize_min = dataReq.twinesize_min,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });


                res.foldspec_id = foldspec_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }

    }
}
