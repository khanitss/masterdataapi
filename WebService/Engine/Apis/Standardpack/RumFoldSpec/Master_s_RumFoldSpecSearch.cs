﻿using Model.Request.Standardpack.RumFoldSpec;
using Model.Response;
using Model.Response.Standardpack.RumFoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecSearch : Base<Master_s_RumFoldSpecSearchReq> 
    {
        public Master_s_RumFoldSpecSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_RumFoldSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_RumFoldSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.RumFoldSpec.master_s_rumfoldspec.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_RumFoldSpecSearchRes()
                {
                    rumfoldspec_id = x.rumfoldspec_id,
                    fold_id = x.fold_id,
                    fold_code = x.fold_code,
                    fold_name = x.fold_name,
                    rumtypeid = x.rumtypeid,
                    rumtypename = x.rumtypename,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name
                });
            });
        }
    }
}
