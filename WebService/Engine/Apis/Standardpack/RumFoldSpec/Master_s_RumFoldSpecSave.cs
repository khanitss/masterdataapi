﻿using Model.Request.Standardpack.RumFoldSpec;
using Model.Response;
using Model.Response.Standardpack.RumFoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecSave : Base<Master_s_RumFoldSpecSaveReq>
    {
        public Master_s_RumFoldSpecSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RumFoldSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RumFoldSpecSaveRes();
            dataRes.data = res;

            try
            {
                int rumfoldspec_id = Ado.Mssql.Table.RumFoldSpec.master_s_rumfoldspec.GetInstant().Save(new Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec()
                {
                    rumfoldspec_id = dataReq.rumfoldspec_id,
                    fold_id = dataReq.fold_id,
                    fold_code = dataReq.fold_code,
                    fold_name = dataReq.fold_name,
                    rumtypeid = dataReq.rumtypeid,
                    rumtypename = dataReq.rumtypename,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });
                res.rumfoldspec_id = rumfoldspec_id;
                res.rumtypeid = dataReq.rumtypeid;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }
    }
}
