﻿using Model.Request.Standardpack.RumFoldSpec;
using Model.Response;
using Model.Response.Standardpack.RumFoldSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecMove : Base<rumfoldspec_movemodel>
    {
        public Master_s_RumFoldSpecMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(rumfoldspec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RumFoldSpecMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec _Rumfoldspec;

            foreach ( var x in dataReq.rumfoldspec_move)
            {
                try
                {
                    _Rumfoldspec = new Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec();
                    _Rumfoldspec.rumfoldspec_id = x.rumfoldspec_id;
                    _Rumfoldspec.user_id = x.user_id;
                    _Rumfoldspec.user_name = x.user_name;

                    Ado.Mssql.Table.RumFoldSpec.master_s_rumfoldspec.GetInstant().Move(_Rumfoldspec);
                    res.rumfoldspec_id = x.rumfoldspec_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;

                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();
                }
            }
        }
    }
}
