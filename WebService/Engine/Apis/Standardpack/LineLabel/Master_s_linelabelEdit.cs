﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class Master_s_linelabelEdit : Base<Master_s_linelabelSaveDReq>
    {
        public Master_s_linelabelEdit()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_linelabelSaveDReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes>();
            dataRes.data = res;
            foreach (var x in dataReq.label_d)
            {
                var resp = new Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes();
                try
                {
                        Ado.Mssql.Table.LineLabel.master_s_linelabel_d.GetInstant().Update(new Model.Table.Mssql.LineLabel.master_s_linelabel_d()
                        {
                            linelabel_id = x.linelabel_id,
                            lineitemno_id = x.lineitemno_id,
                            lineitemno = x.lineitemno,
                            linesubject_id =x.linesubject_id,
                            show_flag = x.show_flag,
                            user_id = x.user_id,
                            user_name = x.user_name,
                            linelabel_desc = x.linelabel_desc,
                            path_pic = x.path_pic,
                            effdate_st = x.effdate_st,
                            effdate_en = x.effdate_en

                        });

                        resp.lineitemno = x.lineitemno;
                        resp.linelabel_id = x.linelabel_id;
                        resp.linesubject_id = x.linesubject_id;
                        resp.status = "S";
                        resp.message = "Success";
                        resp.lineitemno_id = x.lineitemno_id;

                    res.Add(resp);
                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.message = "Failed";
                    resp.message = ex.Message;

                    res.Add(resp);
                }
            }
        }
    }
}
