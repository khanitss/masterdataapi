﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearchID : Base<View_master_s_linelabelSearchByIDReq>
    {
        public View_master_s_linelabelSearchID()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_linelabelSearchByIDReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_linelabelSearchByIDRes>();
            dataRes.data = res;

            Ado.Mssql.Table.LineLabel.master_s_linelabel_h.GetInstant().SearchByID(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_linelabelSearchByIDRes()
                {
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name_th = x.label_name_th,
                    path_pic = x.path_pic,
                    
                });
            });
        }
    }
}
