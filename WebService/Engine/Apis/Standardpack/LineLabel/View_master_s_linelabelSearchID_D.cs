﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearchID_D : Base<View_master_s_linelabelSearchByIDReq>
    { 
        public View_master_s_linelabelSearchID_D()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_linelabelSearchByIDReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_linelabelSearchByIDDRes>();
            dataRes.data = res;

            Ado.Mssql.Table.LineLabel.master_s_linelabel_d.GetInstant().SearchByID(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_linelabelSearchByIDDRes()
                {
                    linelabel_id = x.linelabel_id,
                    lineitemno_id = x.lineitemno_id,
                    lineitemno = x.lineitemno,
                    linesubject_id = x.linesubject_id,
                    show_flag = x.show_flag,

                });
            });
        }
    }
}
