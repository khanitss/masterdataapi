﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class Master_s_linelabelMove : Base<linelabel_movemodel>
    {
        public Master_s_linelabelMove() 
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(linelabel_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_linelabelMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.LineLabel.master_s_linelabel_h _LineLableMoveH;
            //Model.Table.Mssql.LineLabel.master_s_linelabel_d _LineLableMoveD;

            foreach (var x in dataReq.linelabel_move)
            {
                var resp = new Master_s_linelabelMoveRes();
                try
                {
                    _LineLableMoveH = new Model.Table.Mssql.LineLabel.master_s_linelabel_h();
                    _LineLableMoveH.linelabel_id = x.linelabel_id;
                    _LineLableMoveH.user_id = x.user_id;
                    _LineLableMoveH.user_name = x.user_name;
                        

                    Ado.Mssql.Table.LineLabel.master_s_linelabel_h.GetInstant().Move(_LineLableMoveH);
                    resp.status = "s";
                    resp.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                    resp.cancel_id = _LineLableMoveH.user_id;
                    resp.cancel_name = _LineLableMoveH.user_name;
                    resp.cancel_date = _LineLableMoveH.user_date;
                    resp.labelline_id = _LineLableMoveH.linelabel_id;

                    res.Add(resp);
                }
                catch (Exception ex)
                {
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp.status = "F";
                    resp.message = ex.Message;
                    res.Add(resp);
                }
            }
        }
    }
}
