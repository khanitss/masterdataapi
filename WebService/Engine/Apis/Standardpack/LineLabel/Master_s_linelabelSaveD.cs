﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class Master_s_linelabelSaveD : Base<Master_s_linelabelSaveDReq>
    {
        public Master_s_linelabelSaveD()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_linelabelSaveDReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes>();
            dataRes.data = res;

            foreach (var x in dataReq.label_d)
            {
                var resp = new Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes();
                try
                {
                    Ado.Mssql.Table.LineLabel.master_s_linelabel_d.GetInstant().Save(new Model.Table.Mssql.LineLabel.master_s_linelabel_d()
                    {
                        linelabel_id = x.linelabel_id,
                        lineitemno = x.lineitemno,
                        linesubject_id = x.linesubject_id,
                        show_flag = x.show_flag,
                        user_id = x.user_id,
                        user_name = x.user_name
                    });

                    resp.status = "S";
                    resp.message = "Success";
                    resp.linelabel_id = x.linelabel_id;
                    resp.lineitemno = x.lineitemno;
                    resp.linesubject_id = x.linesubject_id;

                    res.Add(resp);

                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.message = "Failed";
                    resp.message = ex.Message;

                    res.Add(resp);
                }
            }
        }
    }
}
