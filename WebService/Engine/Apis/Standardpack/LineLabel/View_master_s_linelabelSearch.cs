﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearch : Base<View_master_s_linelabelSearchReq>
    {
        public View_master_s_linelabelSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_linelabelSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_linelabelSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.LineLabel.master_s_linelabel_h.GetInstant().Search(dataReq).ForEach(x => 
            {
                res.Add(new View_master_s_linelabelSearchRes() 
                { 
                    linelabel_id = x.linelabel_id,
                    cus_id = x.cus_id,
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name_th = x.label_name_th,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    linelabel_desc = x.linelabel_desc,
                    _status = x.status,
                    path_pic = x.path_pic,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }

}
