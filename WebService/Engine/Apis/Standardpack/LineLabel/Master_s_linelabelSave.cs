﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class Master_s_linelabelSave : Base<Master_s_linelabelSaveReq>
    {
        public Master_s_linelabelSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_linelabelSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes>();
            dataRes.data = res;

            foreach (var x in dataReq.linelabel_save)
            {
                var resp = new Model.Response.Standardpack.LineLabel.Master_s_linelabelSaveRes();
                try
                {
                    int linelabel_id = Ado.Mssql.Table.LineLabel.master_s_linelabel_h.GetInstant().Save(new Model.Table.Mssql.LineLabel.master_s_linelabel_h()
                    {
                        cus_id = x.cus_id,
                        cus_cod = x.cus_cod,
                        cus_name = x.cus_name,
                        label_id = x.label_id,
                        label_cd = x.label_cd,
                        effdate_st = x.effdate_st,
                        effdate_en = x.effdate_en,
                        status = x.status,
                        path_pic = x.path_pic,
                        user_id = x.user_id,
                        user_name = x.user_name,
                        label_name_th = x.label_name_th,
                        linelabel_desc = x.linelabel_desc
                    });
                  
                    resp.status = "S";
                    resp.message = "Success";
                    resp.linelabel_id = linelabel_id;

                    res.Add(resp);

                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.message = "Failed";
                    resp.message = ex.Message;

                    res.Add(resp);
                }
            }
        }
    }
}
