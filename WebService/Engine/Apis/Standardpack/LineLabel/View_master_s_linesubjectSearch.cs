﻿using Model.Request.Standardpack.LineLabel;
using Model.Response;
using Model.Response.Standardpack.LineLabel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LineLabel
{
    public class View_master_s_linesubjectSearch : Base<View_master_s_linesubjectSearchReq>
    {
        public View_master_s_linesubjectSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_linesubjectSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_linesubjectSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.LineLabel.master_s_linesubject.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_linesubjectSearchRes()
                {
                    linesubject_id = x.linesubject_id,
                    linesubject_desc = x.linesubject_desc,
                    user_date = x.user_date,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    statusdesc = "S : ทำรายการสำเร็จ(Success)",
                    status = "S",
                    message = ""
                }) ;
            });
        }
    }
}
