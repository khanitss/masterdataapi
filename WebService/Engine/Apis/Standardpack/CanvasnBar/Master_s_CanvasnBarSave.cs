﻿using Model.Request.Standardpack.CanvasnBar;
using Model.Response;
using Model.Response.Standardpack.CanvasnBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSave : Base<Master_s_CanvasnBarSaveReq>
    {
        public Master_s_CanvasnBarSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasnBarSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_CanvasnBarSaveRes();
            dataRes.data = res;

            try
            {
                int canvasnbar_id = Ado.Mssql.Table.CanvasnBar.master_s_canvasnbar.GetInstant().Save(new Model.Table.Mssql.CanvasnBar.master_s_canvasnbar()
                {
                    barcolor_id = dataReq.barcolor_id,
                    barcolor_cd = dataReq.barcolor_cd,
                    barcolor_name = dataReq.barcolor_name,
                    canvasnbar_id = dataReq.canvasnbar_id,
                    canvasnbar_cd = dataReq.canvasnbar_cd,
                    canvasnbar_name = dataReq.canvasnbar_name,
                    canvas_id = dataReq.canvas_id,
                    canvas_cd = dataReq.canvas_cd,
                    canvas_name = dataReq.canvas_name,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    status = dataReq.status,
                    user_id = dataReq.user_id,
                    user_date = dataReq.user_date,
                    user_name = dataReq.user_name,
                });
                res.status = "S";
                res.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                res.canvasnbar_id = canvasnbar_id;
                res.canvasnbar_cd = dataReq.canvasnbar_cd;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;

            }
            catch (Exception ex)
            {
                res.status = "F";
                res.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                res.message = ex.ToString();
            }

        }
    }
}