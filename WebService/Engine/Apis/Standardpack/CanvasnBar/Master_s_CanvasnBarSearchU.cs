﻿using Model.Request.Standardpack.CanvasnBar;
using Model.Response;
using Model.Response.Standardpack.CanvasnBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace WebService.Engine.Apis.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSearchU : Base<Master_s_CanvasnBarSearchReq>
    {
        public Master_s_CanvasnBarSearchU()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasnBarSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasnBarSearchURes>();
            dataRes.data = res;

            Ado.Mssql.Table.CanvasnBar.master_u_canvasnbar.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_CanvasnBarSearchURes()
                {
                    barcolor_id = x.barcolor_id,
                    barcolor_cd = x.barcolor_cd,
                    barcolor_name = x.barcolor_name,
                    canvasnbar_id = x.canvasnbar_id,
                    canvasnbar_cd = x.canvasnbar_cd,
                    canvasnbar_name = x.canvasnbar_name,
                    canvas_id = x.canvas_id,
                    canvas_cd = x.canvas_cd,
                    canvas_name = x.canvas_name,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    path_pic = x.path_pic,
                    status = x.status,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date,
                    cancel_name = x.cancel_name

                });
            });
        }
    }
}