﻿using Model.Request.Standardpack.CanvasnBar;
using Model.Response;
using Model.Response.Standardpack.CanvasnBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSearchActive : Base<Master_s_CanvasnBarSearchReq>
    {
        public Master_s_CanvasnBarSearchActive()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasnBarSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasnBarSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.CanvasnBar.master_s_canvasnbar.GetInstant().SearchActive(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_CanvasnBarSearchRes()
                {
                    barcolor_id = x.barcolor_id,
                    barcolor_cd = x.barcolor_cd,
                    barcolor_name = x.barcolor_name,
                    canvasnbar_id = x.canvasnbar_id,
                    canvasnbar_cd = x.canvasnbar_cd,
                    canvasnbar_name = x.canvasnbar_name,
                    canvas_id = x.canvas_id,
                    canvas_cd = x.canvas_cd,
                    canvas_name = x.canvas_name,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    path_pic = x.path_pic,
                    status = x.status,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name,
                    _status = "S",
                    _message = "S : ทำรายการสำเร็จ (success)"
                });
            });
                
        }
    }
}
