﻿using Model.Request.Standardpack.CanvasnBar;
using Model.Response;
using Model.Response.Standardpack.CanvasnBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSearchUsed : Base<Master_s_CanvasnBarSearchUsedReq>
    {
        public Master_s_CanvasnBarSearchUsed()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasnBarSearchUsedReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasnBarSearchUsedRes>();
            dataRes.data = res;

            Ado.Mssql.Table.CanvasnBar.master_s_canvasnbar.GetInstant().SearchUsed(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_CanvasnBarSearchUsedRes()
                {
                   barcolor_id = x.barcolor_id,
                   canvasnbar_id = x.canvasnbar_id,
                   canvas_id = x.canvas_id
                });
            });
        }
    }
}
