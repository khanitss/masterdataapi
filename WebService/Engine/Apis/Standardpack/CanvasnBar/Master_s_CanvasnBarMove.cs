﻿using Model.Request.Standardpack.CanvasnBar;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarMove : Base<canvasnbar_movemodel>
    {
        public Master_s_CanvasnBarMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(canvasnbar_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.CanvasnBar.Master_s_CanvasnBarMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.CanvasnBar.master_s_canvasnbar _canvasnberReq;
            foreach (var x in dataReq.canvasnbar_move)
            {
                var resp = new Model.Response.Standardpack.CanvasnBar.Master_s_CanvasnBarMoveRes();
                try
                {
                    _canvasnberReq = new Model.Table.Mssql.CanvasnBar.master_s_canvasnbar();
                    _canvasnberReq.canvasnbar_cd = x.canvasnbar_cd;
                    _canvasnberReq.canvasnbar_id = x.canvasnbar_id;
                    _canvasnberReq.user_id = x.user_id;
                    _canvasnberReq.user_name = x.user_name;


                    Ado.Mssql.Table.CanvasnBar.master_s_canvasnbar.GetInstant().Move(_canvasnberReq);
                    resp.canvasnbar_id = x.canvasnbar_id;
                    resp.canvasnbar_cd = x.canvasnbar_cd;
                    resp.cancel_id = x.user_id;
                    resp.cancel_name = x.user_name;
                    resp.status = "S";
                    resp.statusdesc = "S : ทำรายการสำเร็จ (Success)";

                    res.Add(resp);
                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp.message = ex.ToString();

                    res.Add(resp);
                }
            }
        }
    }
}
