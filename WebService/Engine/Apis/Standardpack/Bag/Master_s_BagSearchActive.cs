﻿using Model.Request.Standardpack.Bag;
using Model.Response;
using Model.Response.Standardpack.Bag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Bag
{
    public class Master_s_BagSearchActive : Base<Master_s_BagSearchReq>
    {
        public Master_s_BagSearchActive()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_BagSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_BagSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Bag.master_s_bag.GetInstant().SearchActive(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_BagSearchRes()
                {
                    bag_id = x.bag_id,
                    bag_cd = x.bag_cd,
                    bag_cd_old = x.bag_cd_old,
                    bag_name_en = x.bag_name_en,
                    bag_name_th = x.bag_name_th,
                    effdate_en = x.effdate_en,
                    effdate_st = x.effdate_st,
                    width = x.width,
                    length = x.length,
                    path_pic = x.path_pic,
                    status = x.status,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name
                });
            });
        }
    }
}
