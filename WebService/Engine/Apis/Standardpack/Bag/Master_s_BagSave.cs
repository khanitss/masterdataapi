﻿using Model.Request.Standardpack.Bag;
using Model.Response;
using Model.Response.Standardpack.Bag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Bag
{
    public class Master_s_BagSave : Base<Master_s_BagSaveReq>
    {
        public Master_s_BagSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_BagSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_BagSaveRes();
            dataRes.data = res;

            try
            {
                int bag_id = Ado.Mssql.Table.Bag.master_s_bag.GetInstant().Save(new Model.Table.Mssql.Bag.master_s_bag()
                {
                    bag_id = dataReq.bag_id,
                    bag_cd= dataReq.bag_cd,
                    bag_cd_old = dataReq.bag_cd_old,
                    bag_name_th = dataReq.bag_name_th,
                    bag_name_en = dataReq.bag_name_en,
                    width = dataReq.width,
                    length = dataReq.length,
                    status = dataReq.status,
                    path_pic = dataReq.path_pic,
                    effdate_st = dataReq.effdate_st,
                    effdate_en =dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });

                res.bag_id = bag_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }
    }
}
