﻿using Model.Request.Standardpack.Bag;
using Model.Response;
using Model.Response.Standardpack.Bag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Bag
{
    public class Master_s_BagMove : Base<bag_movemodel>
    {
        public Master_s_BagMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(bag_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_BagMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.Bag.master_s_bag _Bag;

            foreach (var x in dataReq.bag_move)
            {
                try
                {
                    _Bag = new Model.Table.Mssql.Bag.master_s_bag();
                    _Bag.bag_id = x.bag_id;
                    _Bag.user_id = x.user_id;
                    _Bag.user_name = x.user_name;

                    Ado.Mssql.Table.Bag.master_s_bag.GetInstant().Move(_Bag);
                    res.bag_id = x.bag_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;

                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();
                }

            }
            
        }
    }
}
