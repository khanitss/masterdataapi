﻿using Model.Request.Standardpack.BagSpec;
using Model.Response;
using Model.Response.Standardpack.BagSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.BagSpec
{
    public class Master_s_BagSpecSearchCus : Base<Master_s_BagSpecSearchReq>
    {
        public Master_s_BagSpecSearchCus()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_BagSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_BagSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.BagSpec.master_s_bagspec.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_BagSpecSearchRes
                {
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    status = x.status,
                    tsale = x.tsale,
                });
            });
        }

    }
}

