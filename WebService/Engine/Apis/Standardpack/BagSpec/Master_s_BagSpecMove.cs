﻿using Model.Request.Standardpack.BagSpec;
using Model.Response;
using Model.Response.Standardpack.BagSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.BagSpec
{
    public class Master_s_BagSpecMove : Base<bagspec_movemodel>
    
    {
        public Master_s_BagSpecMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(bagspec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_BagSpecMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.BagSpec.master_s_bagspec _bagspec;

            foreach ( var x in dataReq.bagspec_move)
            {
                try
                {
                    _bagspec = new Model.Table.Mssql.BagSpec.master_s_bagspec();
                    _bagspec.bagspec_id = x.bagspec_id;
                    _bagspec.user_id = x.user_id;
                    _bagspec.user_name = x.user_name;

                    Ado.Mssql.Table.BagSpec.master_s_bagspec.GetInstant().Move(_bagspec);
                    res.bagspec_id = x.bagspec_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;
                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();
                }
            }
        }
    }
}
