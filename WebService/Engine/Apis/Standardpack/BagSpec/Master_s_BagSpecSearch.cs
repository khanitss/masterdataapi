﻿using Model.Request.Standardpack.BagSpec;
using Model.Response;
using Model.Response.Standardpack.BagSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.BagSpec
{
    public class Master_s_BagSpecSearch : Base<Master_s_BagSpecSearchReq>
    {
        public Master_s_BagSpecSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_BagSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_BagSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.BagSpec.master_s_bagspec.GetInstant().SearchByValue(dataReq).ForEach(x =>
           {
               res.Add(new Master_s_BagSpecSearchRes
               {
                   bagspec_id = x.bagspec_id,
                   bag_cd = x.bag_cd,
                   bag_id = x.bag_id,
                   bag_name = x.bag_name,
                   cus_cod = x.cus_cod,
                   cus_name = x.cus_name,
                   effdate_en = x.effdate_en,
                   effdate_st = x.effdate_st,
                   label_cd = x.label_cd,
                   label_id = x.label_id,
                   label_name =x.label_name,
                   produccttype_code = x.produccttype_code,
                   producttype_desc = x.producttype_desc,
                   stretchingtype_code = x.stretchingtype_code,
                   stretchingtype_desc = x.stretchingtype_desc,
                   status = x.status,
                   tsale = x.tsale,
                   user_id = x.user_id,
                   user_date = x.user_date,
                   user_name = x.user_name,
                   cancel_id = x.cancel_id,
                   cancel_date = x.cancel_date,
                   cancel_name = x.cancel_name                 
                   
               });
           });
        }

    }
}
