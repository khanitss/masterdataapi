﻿using Model.Request.Standardpack.BagSpec;
using Model.Response;
using Model.Response.Standardpack.BagSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.BagSpec
{
    public class Master_s_BagSpecSave : Base<Master_s_BagSpecSaveReq>
    {
        public Master_s_BagSpecSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_BagSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_BagSpecSaveRes();
            dataRes.data = res;

            try
            {
                int bagspec_id = Ado.Mssql.Table.BagSpec.master_s_bagspec.GetInstant().Save(new Model.Table.Mssql.BagSpec.master_s_bagspec()
                {
                    bagspec_id = dataReq.bagspec_id,
                    bag_id = dataReq.bag_id,
                    bag_cd = dataReq.bag_cd,
                    bag_name = dataReq.bag_name,
                    cus_cod = dataReq.cus_cod,
                    cus_name = dataReq.cus_name,
                    effdate_en = dataReq.effdate_en,
                    effdate_st = dataReq.effdate_st,
                    label_id = dataReq.label_id,
                    label_cd = dataReq.label_cd,
                    label_name = dataReq.label_name,
                    produccttype_code = dataReq.produccttype_code,
                    producttype_desc = dataReq.producttype_desc,
                    stretchingtype_code = dataReq.stretchingtype_code,
                    stretchingtype_desc = dataReq.stretchingtype_desc,
                    status = dataReq.status,
                    tsale = dataReq.tsale,
                    user_id = dataReq.user_id,
                    user_name =dataReq.user_name
                });
                res.bagspec_id = bagspec_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }
    }
}
