﻿using Model.Request.Standardpack.Rope;
using Model.Response;
using Model.Response.Standardpack.Rope;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Rope
{
    public class Master_s_RopeSearchActive : Base<Master_s_RopeSearchReq>
    {
        public Master_s_RopeSearchActive()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RopeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_RopeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Rope.master_s_rope.GetInsant().SearchActive(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_RopeSearchRes()
                {
                    rope_id = x.rope_id,
                    rope_cd = x.rope_cd,
                    rope_name = x.rope_name,
                    rope_cd_old = x.rope_cd_old,
                    path_pic = x.path_pic,
                    itemcode = x.itemcode,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                });
            });
        }
    }
}
