﻿using Model.Request.Standardpack.Rope;
using Model.Response;
using Model.Response.Standardpack.Rope;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Rope
{
    public class Master_s_RopeMove : Base<rope_movemodel>
    {
        public Master_s_RopeMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(rope_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RopeMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.Rope.master_s_rope _rope;

            foreach (var x in dataReq.rope_move)
            {
                try
                {
                    _rope = new Model.Table.Mssql.Rope.master_s_rope();
                    _rope.rope_id = x.rope_id;
                    _rope.user_id = x.user_id;
                    _rope.user_name = x.user_name;

                    Ado.Mssql.Table.Rope.master_s_rope.GetInsant().Move(_rope);
                    res.rope_id = x.rope_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;

                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();
                }
            }
        }
    }
}
