﻿using Model.Request.Standardpack.Rope;
using Model.Response;
using Model.Response.Standardpack.Rope;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Rope
{
    public class Master_s_RopeSave : Base<Master_s_RopeSaveReq>
    {
        public Master_s_RopeSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RopeSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RopeSaveRes();
            dataRes.data = res;

            try
            {
                int rope_id = Ado.Mssql.Table.Rope.master_s_rope.GetInsant().Save(new Model.Table.Mssql.Rope.master_s_rope()
                {

                    rope_id = dataReq.rope_id,
                    rope_cd = dataReq.rope_cd,
                    rope_name = dataReq.rope_name,
                    rope_cd_old = dataReq.rope_cd_old,
                    path_pic = dataReq.path_pic,
                    status = dataReq.status,
                    itemcode = dataReq.itemcode,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });
                res.rope_id = rope_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch ( Exception ex )
            {
                res.status = "F";
                res.message = ex.ToString();
            }

        }

    }
}
