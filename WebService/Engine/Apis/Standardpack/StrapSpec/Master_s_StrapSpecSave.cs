﻿using Model.Request.Standardpack.StrapSpec;
using Model.Response;
using Model.Response.Standardpack.StrapSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StrapSpec
{
    public class Master_s_StrapSpecSave : Base<Master_s_StrapSpecSaveReq>
    {
        public Master_s_StrapSpecSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StrapSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StrapSpecSaveRes();
            dataRes.data = res;

            try
            {
                int strapspec_id = Ado.Mssql.Table.StrapSpec.master_s_strapspec.GetInstant().Save(new Model.Table.Mssql.StrapSpec.master_s_strapspec()
                {
                    strapspec_id = dataReq.strapspec_id,
                    strap_id = dataReq.strap_id,
                    strap_cd = dataReq.strap_cd,
                    strap_name = dataReq.strap_name,
                    label_id = dataReq.label_id,
                    label_cd = dataReq.label_cd,
                    label_name = dataReq.label_name,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });
                res.strapspec_id = strapspec_id;
                res.strap_id = dataReq.strap_id;
                res.strap_cd = dataReq.strap_cd;
                res.strap_name = dataReq.strap_name;
                res.label_id = dataReq.label_id;
                res.label_cd = dataReq.label_cd;
                res.label_name = dataReq.label_name;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";

            }
            catch (Exception ex)
            {
                res._status = "F";
                res._message = ex.ToString();
            }
        }
    }
}
