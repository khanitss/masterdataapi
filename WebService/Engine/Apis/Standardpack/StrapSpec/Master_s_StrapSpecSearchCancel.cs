﻿using Model.Request.Standardpack.StrapSpec;
using Model.Response;
using Model.Response.Standardpack.StrapSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StrapSpec
{
    public class Master_s_StrapSpecSearchCancel : Base<Master_s_StrapSpecSearchReq>
    {
        public Master_s_StrapSpecSearchCancel()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StrapSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StrapSpecSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.StrapSpec.master_u_strapspec.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_StrapSpecSearchCancelRes()
                {
                    strapspec_id = x.strapspec_id,
                    strap_id = x.strap_id,
                    strap_cd = x.strap_cd,
                    strap_name = x.strap_name,
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name = x.label_name,
                    status = x.status,
                    effdate_en = x.effdate_en,
                    effdate_st = x.effdate_st,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date,
                    cancel_name = x.cancel_name
                });
            });
        }
    }
}
