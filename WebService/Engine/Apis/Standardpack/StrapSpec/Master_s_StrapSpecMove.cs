﻿using Model.Request.Standardpack.StrapSpec;
using Model.Response;
using Model.Response.Standardpack.StrapSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StrapSpec
{
    public class Master_s_StrapSpecMove : Base<strapspec_movemodel>
    {
        public Master_s_StrapSpecMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(strapspec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StrapSpecMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.StrapSpec.master_s_strapspec _Strapspec;

            foreach (var x in dataReq.strapspec_move)
            {
                try
                {
                    _Strapspec = new Model.Table.Mssql.StrapSpec.master_s_strapspec();
                    _Strapspec.strapspec_id = x.strapspec_id;
                    _Strapspec.user_id = x.user_id;
                    _Strapspec.user_name = x.user_name;

                    Ado.Mssql.Table.StrapSpec.master_s_strapspec.GetInstant().Move(_Strapspec);
                    res.strapspec_id = x.strapspec_id;
                    res.user_id = x.user_id;
                    res.user_name = x.user_name;
                    res._status = "S";
                    res._message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res._status = "F";
                    res._message = ex.ToString();
                }
            }
        }
    }
}
