﻿using Model.Request.Standardpack.RopeSpec;
using Model.Response;
using Model.Response.Standardpack.RopeSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSearchCus : Base<Master_s_RopeSpecSearchReq>
    {
        public Master_s_RopeSpecSearchCus()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RopeSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_RopeSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.RopeSpec.master_s_ropespec.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_RopeSpecSearchRes
                {
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    tsale = x.tsale,
                    status = x.status,
                });
            });
        }

    }
}
