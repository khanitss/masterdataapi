﻿using Model.Request.Standardpack.RopeSpec;
using Model.Response;
using Model.Response.Standardpack.RopeSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSave : Base<Master_s_RopeSpecSaveReq>
    {
        public Master_s_RopeSpecSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_RopeSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RopeSpecSaveRes();
            dataRes.data = res;

            try
            {
                int ropespec_id = Ado.Mssql.Table.RopeSpec.master_s_ropespec.GetInstant().Save(new Model.Table.Mssql.RopeSpec.master_s_ropespec()
                {
                    ropespec_id = dataReq.ropespec_id,
                    tsale = dataReq.tsale,
                    cus_cod = dataReq.cus_cod,
                    cus_name = dataReq.cus_name,
                    produccttype_code = dataReq.produccttype_code,
                    producttype_desc = dataReq.producttype_desc,
                    rope_id = dataReq.rope_id,
                    rope_cd = dataReq.rope_cd,
                    rope_name = dataReq.rope_name,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });
                res.ropespec_id = ropespec_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }
    }
}
