﻿using Model.Request.Standardpack.RopeSpec;
using Model.Response;
using Model.Response.Standardpack.RopeSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSearch : Base<Master_s_RopeSpecSearchReq>
    {
        public Master_s_RopeSpecSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RopeSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_RopeSpecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.RopeSpec.master_s_ropespec.GetInstant().SearchByValue(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_RopeSpecSearchRes
                {
                    ropespec_id = x.ropespec_id,
                    tsale = x.tsale,
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    produccttype_code = x.produccttype_code,
                    producttype_desc = x.producttype_desc,
                    rope_id = x.rope_id,
                    rope_cd = x.rope_cd,
                    rope_name = x.rope_name,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name,
                    cancel_date = x.cancel_date,
                    cancel_id = x.cancel_id,
                    cancel_name = x.cancel_name
                });
            });
        }

    }
}
