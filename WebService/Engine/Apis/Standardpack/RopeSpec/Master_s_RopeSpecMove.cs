﻿using Model.Request.Standardpack.RopeSpec;
using Model.Response;
using Model.Response.Standardpack.RopeSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecMove : Base<ropespec_movemodel>
    {
        public Master_s_RopeSpecMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(ropespec_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RopeSpecMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.RopeSpec.master_s_ropespec _ropespec;

            foreach (var x in dataReq.ropespec_move)
            {
                try
                {
                    _ropespec = new Model.Table.Mssql.RopeSpec.master_s_ropespec();
                    _ropespec.ropespec_id = x.ropespec_id;
                    _ropespec.user_id = x.user_id;
                    _ropespec.user_name = x.user_name;

                    Ado.Mssql.Table.RopeSpec.master_s_ropespec.GetInstant().Move(_ropespec);
                    res.ropespec_id = x.ropespec_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;
                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();
                }
            }
        }
    }
}
