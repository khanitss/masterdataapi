﻿using Model.Request.Standardpack.Canvas;
using Model.Response;
using Model.Response.Standardpack.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Canvas
{
    public class Master_s_CanvasSave : Base<Master_s_CanvasSaveReq>
    {
        public Master_s_CanvasSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_CanvasSaveRes();
            dataRes.data = res;

            try
            {

                int canvas_id = Ado.Mssql.Table.Canvas.master_s_canvas.GetInstant().Save(new Model.Table.Mssql.Canvas.master_s_canvas()
                {
                    canvas_id = dataReq.canvas_id,
                    canvas_cd = dataReq.canvas_cd,
                    canvas_name = dataReq.canvas_name,
                    effdate_en = dataReq.effdate_en,
                    effdate_st = dataReq.effdate_st,
                    itemcode = dataReq.itemcode,
                    path_pic = dataReq.path_pic,
                    status = dataReq.status,
                    user_date = dataReq.user_date,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name

                });
                res.status = "S";
                res.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                res.canvas_id = canvas_id;
                res.canvas_cd = dataReq.canvas_cd;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                res.message = ex.ToString();

            }
        }
    }
}
