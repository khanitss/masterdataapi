﻿using Model.Request.Standardpack.Canvas;
using Model.Response;
using Model.Response.Standardpack.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebService.Engine.Apis.Standardpack.Canvas
{
    public class Master_s_CanvasSearchU : Base<Master_s_CanvasSearchReq>
    {
        public Master_s_CanvasSearchU()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_CanvasSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_CanvasSearchURes>();
            dataRes.data = res;

            Ado.Mssql.Table.Canvas.master_u_canvas.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_CanvasSearchURes()
                {
                    canvas_id = x.canvas_id,
                    canvas_cd = x.canvas_cd,
                    canvas_name = x.canvas_name,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    path_pic = x.path_pic,
                    itemcode = x.itemcode,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    status = x.status,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date,
                    cancel_name = x.cancel_name,

                });
            });
        }
    }
}
