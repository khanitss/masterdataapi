﻿using Model.Request.Standardpack.Canvas;
using Model.Response;
using Model.Response.Standardpack.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Canvas
{
    public class Master_s_CanvasMove : Base<canvas_movemodel>
    {
        public Master_s_CanvasMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(canvas_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.Canvas.Master_s_CanvasMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.Canvas.master_s_canvas _canvasReq;
            foreach (var x in dataReq.canvas_move)
            {
                var resp = new Model.Response.Standardpack.Canvas.Master_s_CanvasMoveRes();
                try
                {
                    _canvasReq = new Model.Table.Mssql.Canvas.master_s_canvas();
                    _canvasReq.canvas_cd = x.canvas_cd;
                    _canvasReq.canvas_id = x.canvas_id;
                    _canvasReq.user_id = x.user_id;
                    _canvasReq.user_name = x.user_name;


                    Ado.Mssql.Table.Canvas.master_s_canvas.GetInstant().Move(_canvasReq);
                    resp.canvas_id = x.canvas_id;
                    resp.canvas_cd = x.canvas_cd;
                    resp.cancel_id = x.user_id;
                    resp.cancel_name = x.user_name;
                    resp.status = "S";
                    resp.statusdesc = "S : ทำรายการสำเร็จ (Success)";

                    res.Add(resp);
                }
                catch ( Exception ex)
                {
                    resp.status = "F";
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp.message = ex.ToString();

                    res.Add(resp);
                }
            }
        }
    }
}
