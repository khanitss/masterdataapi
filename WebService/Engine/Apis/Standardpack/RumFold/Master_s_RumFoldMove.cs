﻿using Model.Request.Standardpack.RumFold;
using Model.Response;
using Model.Response.Standardpack.RumFold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFold
{
    public class Master_s_RumFoldMove : Base<rumfold_movemodel>
    {
        public Master_s_RumFoldMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(rumfold_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RumFoldMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.RumFold.master_s_rumfold _rumfold;

            foreach (var x in dataReq.rumfold_move)
            {
                try
                {
                    _rumfold = new Model.Table.Mssql.RumFold.master_s_rumfold();
                    _rumfold.fold_id = x.fold_id;
                    _rumfold.user_id = x.user_id;
                    _rumfold.user_name = x.user_name;

                    Ado.Mssql.Table.RumFold.master_s_rumfold.GetInstant().Move(_rumfold);
                    res.fold_id = x.fold_id;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;
                         
                    res.status = "S";
                    res.message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res.status = "F";
                    res.message = ex.ToString();                         
                }
            }
        }
    }
}
