﻿using Model.Request.Standardpack.RumFold;
using Model.Response;
using Model.Response.Standardpack.RumFold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFold
{
    public class Master_s_RumFoldSave : Base<Master_s_RumFoldSaveReq>
    {
        public Master_s_RumFoldSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_RumFoldSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_RumFoldSaveRes();
            dataRes.data = res;

            try
            {
                int fold_id = Ado.Mssql.Table.RumFold.master_s_rumfold.GetInstant().Save(new Model.Table.Mssql.RumFold.master_s_rumfold()
                {
                    fold_id = dataReq.fold_id,
                    fold_code = dataReq.fold_code,
                    fold_name = dataReq.fold_name,
                    fold_information = dataReq.fold_information,
                    path_pic = dataReq.path_pic,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name

                });

                res.fold_id = fold_id;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = "S";
                res.message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.status = "F";
                res.message = ex.ToString();
            }
        }

    }
}
