﻿using Model.Request.Standardpack.RumFold;
using Model.Response;
using Model.Response.Standardpack.RumFold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.RumFold
{
    public class Master_s_RumFoldSearchCancel : Base<Master_s_RumFoldSearchReq>
    {
        public Master_s_RumFoldSearchCancel()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_RumFoldSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_RumFoldSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.RumFold.master_u_rumfold.GetInstant().SearchCancel(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_RumFoldSearchCancelRes()
                {
                    fold_id = x.fold_id,
                    fold_code = x.fold_code,
                    fold_name = x.fold_name,
                    fold_information = x.fold_information,
                    path_pic = x.path_pic,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_date = x.user_date,
                    user_name = x.user_name,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date,
                    cancel_name = x.cancel_name
                });
            });
        }
    }
}
