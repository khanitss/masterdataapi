﻿using Model.Request.Standardpack.Sticker;
using Model.Response;
using Model.Response.Standardpack.Sticker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Sticker
{
    public class Master_s_StickerSave : Base<Master_s_StickerSaveReq>
    {
        public Master_s_StickerSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StickerSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StickerSaveRes();
            dataRes.data = res;

            try
            {
                int sticker_id = Ado.Mssql.Table.Sticker.master_s_sticker.GetInstant().Save(new Model.Table.Mssql.Sticker.master_s_sticker()
                {
                    sticker_id = dataReq.sticker_id,
                    sticker_cd = dataReq.sticker_cd,
                    sticker_name = dataReq.sticker_name,
                    status = dataReq.status,
                    itemcode = dataReq.itemcode,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name,
                    sticker_type = dataReq.sticker_type,
                    version = dataReq.version,
                    path_pic = dataReq.path_pic,
                    remark = dataReq.remark
                });
                res.sticker_id = sticker_id;
                res.sticker_cd = dataReq.sticker_cd;
                res.sticker_name = dataReq.sticker_name;
                res.status = dataReq.status;
                res.itemcode = dataReq.itemcode;
                res.effdate_st = dataReq.effdate_st;
                res.effdate_en = dataReq.effdate_en;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res._status = "F";
                res._message = ex.ToString();
            }
        }

    }
}
