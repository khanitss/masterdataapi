﻿using Model.Request.Standardpack.Sticker;
using Model.Response;
using Model.Response.Standardpack.Sticker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Sticker
{
    public class Master_s_StickerSearchView : Base<Master_s_StickerSearchReq>
    {
        public Master_s_StickerSearchView()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StickerSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StickerSearchViewRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Sticker.master_s_sticker.GetInstant().SearchView(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_StickerSearchViewRes()
                {
                    sticker_id = x.sticker_id,
                    sticker_cd = x.sticker_cd, 
                    sticker_name = x.sticker_name,
                    status = x.status,
                    itemcode = x.itemcode,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    remark = x.remark,
                    path_pic = x.path_pic,
                    version = x.version,
                    sticker_type = x.sticker_type
                });
            });
        }
    }
}
