﻿using Model.Request.Standardpack.Sticker;
using Model.Response;
using Model.Response.Standardpack.Sticker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Sticker
{
    public class Master_s_StickerMove : Base<sticker_movemodel>
    {
        public Master_s_StickerMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(sticker_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StickerMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.Sticker.master_s_sticker _sticker;

            foreach (var x in dataReq.sticker_move)
            {
                try
                {
                    _sticker = new Model.Table.Mssql.Sticker.master_s_sticker();
                    _sticker.sticker_id = x.sticker_id;
                    _sticker.user_id = x.user_id;
                    _sticker.user_name = x.user_name;

                    Ado.Mssql.Table.Sticker.master_s_sticker.GetInstant().Move(_sticker);
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;
                    res._status = "S";
                    res._message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res._status = "F";
                    res._message = ex.ToString();
                }
            }
        }
    }
}
