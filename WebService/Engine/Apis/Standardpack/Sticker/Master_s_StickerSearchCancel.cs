﻿using Model.Request.Standardpack.Sticker;
using Model.Response;
using Model.Response.Standardpack.Sticker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Sticker
{
    public class Master_s_StickerSearchCancel : Base<Master_s_StickerSearchReq>
    {
        public Master_s_StickerSearchCancel()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StickerSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StickerSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Sticker.master_u_sticker.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_StickerSearchCancelRes()
                {
                    sticker_id = x.sticker_id,
                    sticker_cd = x.sticker_cd,
                    sticker_name = x.sticker_name,
                    status = x.status,
                    itemcode = x.itemcode,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id =x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    cancel_id = x.cancel_id,
                    cancel_name = x.cancel_name,
                    cancel_date = x.cancel_date,
                    sticker_type = x.sticker_type,
                    version = x.version,
                    path_pic = x.path_pic,
                    remark = x.remark
                });
            });
        }
    }
}
