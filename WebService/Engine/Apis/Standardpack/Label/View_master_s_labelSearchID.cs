﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class View_master_s_labelSearchID : Base<View_master_s_labelSearchReq>
    {
        public View_master_s_labelSearchID()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_labelSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_labelSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.master_s_label.GetInstant().SearchID(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_labelSearchRes()
                {
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name_th = x.label_name_th,
                    label_name_en = x.label_name_en,
                    label_cd_old = x.label_cd_old,
                    width = x.width,
                    length = x.length,
                    label_type = x.label_type,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    status = x.status,
                    itemcode = x.itemcode,
                    path_pic = x.path_pic,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    version = x.version,
                    remark = x.remark,
                    acczonelabelcode_cd = x.acczonelabelcode_cd,
                    acczonelabelcode_id = x.acczonelabelcode_id,
                    cus_cod = x.cus_cod,
                    digit1 = x.digit1,
                    digit23 = x.digit23,
                    digit4 = x.digit4,
                    labelgrade_cd = x.labelgrade_cd,
                    labelgrade_id = x.labelgrade_id,
                    labelowner_id = x.labelowner_id,
                    labelpapertype_cd = x.labelpapertype_cd,
                    labelpapertype_id = x.labelpapertype_id,
                    zone_code = x.zone_code

                });
            });



        }
    }
}
