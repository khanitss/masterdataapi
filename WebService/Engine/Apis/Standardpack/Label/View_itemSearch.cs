﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class View_itemSearch : Base<View_itemSearchReq>
    {
        public View_itemSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_itemSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_itemSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.master_v_rmitemlabel.GetInstant().SearchItem(dataReq).ForEach(x =>
            {
                res.Add(new View_itemSearchRes()
                {
                    ItemCode = x.ItemCode,
                    ItemName = x.ItemName
                });
            });

        }
    }
}
