﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class Master_s_lableSearchLabelCd : Base<Master_s_lableSearchLabelCdReq>
    {
        public Master_s_lableSearchLabelCd()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_lableSearchLabelCdReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_lableSearchLabelCdRes>();
            dataRes.data = res;

            Ado.Mssql.View.master_v_labelorder.GetInstant().SearchLabelCd(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_lableSearchLabelCdRes()
                {
                    labelcd = x.labelcd
                }) ;
               
            });
        }
    }
}
