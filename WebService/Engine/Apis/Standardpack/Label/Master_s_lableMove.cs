﻿using Model.Request.Standardpack;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class Master_s_lableMove : Base<label_movemodel>
    {
        // จะทำการย้ายจาก master_s ไป master_u โดยมีเงื่อนไขว่า ถ้ามีสถานะการใช้จาก view ที่พี่เอกจะสร้าง ว่ามีการนำไปใช้รึยังถ้ามีแล้วจะยกเลิก(ย้าย) ไม่ได้
        public Master_s_lableMove()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(label_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Stadardpack.Master_s_lableMoveRes>();

            dataRes.data = res;

            Model.Table.Mssql.master_u_label _labelMoveReq;

            foreach (var x in dataReq.label_move)
            {
                var resp = new Model.Response.Stadardpack.Master_s_lableMoveRes();
                try
                {
                    _labelMoveReq = new Model.Table.Mssql.master_u_label();
                    _labelMoveReq.label_cd = x.label_cd;
                    _labelMoveReq.label_id = x.label_id;
                    _labelMoveReq.user_id = x.user_id;
                    _labelMoveReq.user_name = x.user_name;


                    resp.label_id = Ado.Mssql.Table.master_s_label.GetInstant().Move(_labelMoveReq);
                    resp.label_cd = x.label_cd;
                    resp.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                    resp._status = "S";
                    resp._message = "";

                    res.Add(resp);
                }

                catch (Exception ex)
                {
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp._status = "F";
                    resp._message = ex.Message;

                    res.Add(resp);
                }
            }
        }
    }
}
