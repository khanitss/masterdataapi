﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Standardpack.Label;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Label
{
    public class Master_s_LabelSearchLastID : Base<View_master_s_labelSearchReq>
    {
        public Master_s_LabelSearchLastID()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_labelSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_LabelSearchLastIDRes();
            dataRes.data = res;

           int max_id = Ado.Mssql.Table.master_s_label.GetInstant().SearchLastID(new Model.Table.Mssql.master_s_label()
            {
            });
            res.LastID = max_id;
        }
    }
}
