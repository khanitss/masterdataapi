﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class Master_s_lableEdit : Base<Master_s_lableSaveReq>
    {
        public Master_s_lableEdit()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_lableSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_lableSaveRes();

            dataRes.data = res;

            //Model.Table.Mssql.master_s_label LabelReq;

            try
            {
                //Ado.Mssql.Table.master_s_label.GetInstant().Save(dataReq).ForEach(x =>
                //{
                //    LabelReq = new Model.Table.Mssql.master_s_label();
                //    LabelReq.label_cd = x.label_cd;
                //    LabelReq.label_id = x.label_id;
                //    LabelReq.length = x.length;
                //    LabelReq.status = "S";
                //    res.user_name = x.user_name;
                //});

                res.user_id = Ado.Mssql.Table.master_s_label.GetInstant().Update(dataReq).ToString();
                res.user_date = DateTime.Today;
                res.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                res._status = "S";
                res._message = "";

            }
            catch (Exception ex)
            {
                res.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                res._status = "F";
                res._message = ex.Message;
            }
        }

    }
}
