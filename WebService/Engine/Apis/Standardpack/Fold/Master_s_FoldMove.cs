﻿using Model.Request.Standardpack.Fold;
using Model.Response;
using Model.Response.Standardpack.Fold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Fold
{
    public class Master_s_FoldMove : Base<fold_movemodel>
    {
        public Master_s_FoldMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(fold_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_FoldMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.Fold.master_s_fold _fold;

            foreach (var x in dataReq.fold_move)
            {
                try
                {
                    _fold = new Model.Table.Mssql.Fold.master_s_fold();
                    _fold.fold_id = x.fold_id;
                    _fold.user_id = x.user_id;
                    _fold.user_name = x.user_name;

                    Ado.Mssql.Table.Fold.master_s_fold.GetInstant().Move(_fold);
                    res.cancel_id = x.user_id;
                    res.user_name = x.user_name;
                    res.fold_id = x.fold_id;
                    res._status = "S";
                    res._message = "S : ทำรายการสำเร็จ (Success)";
                }
                catch (Exception ex)
                {
                    res._status = "F";
                    res._message = ex.ToString();
                }
            }
        }
    }
}
