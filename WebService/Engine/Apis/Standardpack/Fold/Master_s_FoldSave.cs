﻿using Model.Request.Standardpack.Fold;
using Model.Response;
using Model.Response.Standardpack.Fold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Fold
{
    public class Master_s_FoldSave : Base<Master_s_FoldSaveReq>
    {
        public Master_s_FoldSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_FoldSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_FoldSaveRes();
            dataRes.data = res;

            try
            {
                int fold_id = Ado.Mssql.Table.Fold.master_s_fold.GetInstant().Save(new Model.Table.Mssql.Fold.master_s_fold()
                {
                    fold_id = dataReq.fold_id,
                    fold_code = dataReq.fold_code,
                    fold_name = dataReq.fold_name,
                    fold_information = dataReq.fold_information,
                    path_pic = dataReq.path_pic,
                    status = dataReq.status,
                    effdate_st = dataReq.effdate_st,
                    effdate_en= dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });

                res.fold_id = fold_id;
                res.fold_code = dataReq.fold_code;
                res.fold_name = dataReq.fold_name;
                res.fold_information = dataReq.fold_information;
                res.status = dataReq.status;
                res.path_pic = dataReq.path_pic;
                res.effdate_st = dataReq.effdate_st;
                res.effdate_en = dataReq.effdate_en;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";

            }
            catch (Exception ex)
            {
                res._status = "F";
                res._message = ex.ToString();
            }
        }
    }
}
