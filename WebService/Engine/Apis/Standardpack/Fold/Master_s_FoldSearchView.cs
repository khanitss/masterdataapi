﻿using Model.Request.Standardpack.Fold;
using Model.Response;
using Model.Response.Standardpack.Fold;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Fold
{
    public class Master_s_FoldSearchView : Base<Master_s_FoldSearchReq>
    {
        public Master_s_FoldSearchView()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_FoldSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_FoldSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Fold.master_s_fold.GetInstant().SearchView(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_FoldSearchRes()
                {
                    fold_id = x.fold_id,
                    fold_code = x.fold_code,
                    fold_name = x.fold_name,
                    fold_information = x.fold_information,
                    path_pic = x.path_pic,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date
                });
            });
        }
    }
}
