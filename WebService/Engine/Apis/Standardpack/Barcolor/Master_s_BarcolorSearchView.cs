﻿using Model.Request.Standardpack.Barcolor;
using Model.Response;
using Model.Response.Standardpack.Barcolor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Barcolor
{
    public class Master_s_BarcolorSearchView : Base<Master_s_BarcolorSearchReq>

    {
        public Master_s_BarcolorSearchView()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_BarcolorSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_BarcolorSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.BarColor.master_s_barcolor.GetInstant().SearchView(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_BarcolorSearchRes()
                {
                    barcolor_id = x.barcolor_id,
                    barcolor_cd = x.barcolor_cd,
                    barcolor_name = x.barcolor_name,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    status = x.status,
                    path_pic = x.path_pic,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    _status = "S",
                    _message = "S : ทำรายการสำเร็จ (success)"
                });
            });
        }
    }
}
