﻿using Model.Request.Standardpack.Barcolor;
using Model.Response;
using Model.Response.Standardpack.Barcolor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Barcolor
{
    public class Master_s_BarcolorMove : Base<barcolor_movemodel>
    {
        public Master_s_BarcolorMove ()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(barcolor_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Standardpack.Barcolor.Master_s_BarcolorMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.BarColor.master_s_barcolor _barcolorReq;
            foreach (var x in dataReq.barcolor_move)
            {
                var resp = new Model.Response.Standardpack.Barcolor.Master_s_BarcolorMoveRes();
                try
                {
                    _barcolorReq = new Model.Table.Mssql.BarColor.master_s_barcolor();
                    _barcolorReq.barcolor_id = x.barcolor_id;
                    _barcolorReq.barcolor_cd = x.barcolor_cd;
                    _barcolorReq.user_id = x.user_id;
                    _barcolorReq.user_name = x.user_name;

                    Ado.Mssql.Table.BarColor.master_s_barcolor.GetInstant().Move(_barcolorReq);
                    resp.barcolor_id = x.barcolor_id;
                    resp.barcolor_cd = x.barcolor_cd;
                    resp.cancel_id = x.user_id;
                    resp.cancel_name = x.user_name;
                    resp.status = "S";
                    resp.statusdesc = "S : ทำรายการสำเร็จ (Success)";

                    res.Add(resp);
                }
                catch (Exception ex)
                {
                    resp.status = "F";
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp.message = ex.ToString();
                    res.Add(resp);

                }
            }
        }


    }
}
