﻿using Model.Request.Standardpack.Strap;
using Model.Response;
using Model.Response.Standardpack.Strap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Strap
{
    public class Master_s_StrapSearchCancel : Base<Master_s_StrapSearchReq>
    {
        public Master_s_StrapSearchCancel()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StrapSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StrapSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.Strap.master_u_strap.GetInstant().SearchView(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_StrapSearchCancelRes()
                {
                    strap_id = x.strap_id,
                    strap_cd = x.strap_cd,
                    strap_name = x.strap_name,
                    status = x.status,
                    itemcode = x.itemcode,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                    cancel_id = x.cancel_id,
                    cancel_name = x.cancel_name,
                    cancel_date = x.cancel_date
                });
            });
        }
    }
}
