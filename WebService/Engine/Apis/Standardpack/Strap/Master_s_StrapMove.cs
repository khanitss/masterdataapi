﻿using Model.Request.Standardpack.Strap;
using Model.Response;
using Model.Response.Standardpack.Strap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Strap
{
    public class Master_s_StrapMove : Base<strap_movemodel>
    {
        public Master_s_StrapMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(strap_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StrapMoveRes();
            dataRes.data = res;

            Model.Table.Mssql.Strap.master_s_strap _strap;
            foreach (var x in dataReq.strap_move)
            {
                try
                {
                    _strap = new Model.Table.Mssql.Strap.master_s_strap();
                    _strap.strap_id = x.strap_id;
                    _strap.user_id = x.user_id;
                    _strap.user_name = x.user_name;

                    Ado.Mssql.Table.Strap.master_s_strap.GetInstant().Move(_strap);
                    res.strap_id = x.strap_id;
                    res.strap_name = x.strap_name;
                    res.cancel_id = x.user_id;
                    res.cancel_name = x.user_name;
                    res._status = "S";
                    res._message = "S : ทำรายการสำเร็จ (Success)";

                }
                catch (Exception ex)
                {
                    res._status = "F";
                    res._message = ex.ToString();
                }
            }
        }
    }
}
