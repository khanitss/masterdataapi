﻿using Model.Request.Standardpack.Strap;
using Model.Response;
using Model.Response.Standardpack.Strap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.Strap
{
    public class Master_s_StrapSave : Base<Master_s_StrapSaveReq>
    {
        public Master_s_StrapSave()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_StrapSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StrapSaveRes();
            dataRes.data = res;

            try
            {
                int strap_id = Ado.Mssql.Table.Strap.master_s_strap.GetInstant().Save(new Model.Table.Mssql.Strap.master_s_strap()
                { 
                    strap_id = dataReq.strap_id,
                    strap_cd = dataReq.strap_cd,
                    strap_name = dataReq.strap_name,
                    status = dataReq.status,
                    itemcode = dataReq.itemcode,
                    effdate_st = dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name =dataReq.user_name
                });
                res.strap_id = strap_id;
                res.strap_cd = dataReq.strap_cd;
                res.strap_name = dataReq.strap_name;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res.status = dataReq.status;
                res.itemcode = dataReq.itemcode;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res._status = "F";
                res._message = ex.ToString();
            }
        }
    }
}
