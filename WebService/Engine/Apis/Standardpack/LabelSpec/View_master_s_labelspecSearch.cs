﻿using Model.Request.Standardpack;
using Model.Response;
using Model.Response.Stadardpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack
{
    public class View_master_s_labelspecSearch : Base<View_master_s_labelspecSearchReq>
    {
        public View_master_s_labelspecSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_labelspecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_labelspecSearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.LabelSpec.master_s_labelspec.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_labelspecSearchRes()
                {
                    labelspec_id = x.labelspec_id,
                    tsale = x.tsale,
                    cus_id = x.cus_id,
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    producttype_code = x.producttype_code,
                    producttype_desc = x.producttype_desc,
                    pqgradepd = x.pqgradepd,
                    pqgrade_desc = x.pqgrade_desc,
                    stdwei_min = x.stdwei_min,
                    stdwei_max = x.stdwei_max,
                    twinesize_min = x.twinesize_min,
                    twinesize_max = x.twinesize_max,
                    depsize_min = x.depamt_min,
                    depsize_max = x.depsize_max,
                    depamt_min = x.depamt_min,
                    depamt_max = x.depamt_max,
                    len_min = x.len_min,
                    len_max = x.len_max,
                    knottype_code = x.knottype_code,
                    linetype_code = x.linetype_code,
                    stretchingtype_code = x.stretchingtype_code,
                    old_color_code = x.old_color_code,
                    new_color_code = x.new_color_code,
                    color_code = x.color_code,
                    color_name = x.color_name,
                    label_id = x.label_id,
                    label_cd = x.label_cd,
                    label_name = x.label_name,
                    effdate_st = x.effdate_st,
                    effdate_en = x.effdate_en,
                    status = x.status,
                    min_product_type = x.min_product_type,
                    min_productcate_grp = x.min_productcate_grp,
                    min_twine_no = x.min_twine_no,
                    min_line_no = x.min_line_no,
                    min_word = x.min_word,
                    min_product_grp = x.min_product_grp,
                    max_product_type = x.max_product_type,
                    max_productcate_grp = x.max_productcate_grp,
                    max_twine_no = x.max_twine_no,
                    max_line_no = x.max_line_no,
                    max_word = x.max_word,
                    max_product_grp = x.max_product_grp,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date = x.user_date,
                });
            });
        }
    }
}
