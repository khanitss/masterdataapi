﻿using Model.Request.Standardpack.LabelSpec;
using Model.Response;
using Model.Response.Stadardpack.LabelSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LabelSpec
{
    public class Master_s_labelspecUpdate : Base<Master_s_labelspecSaveReq>
    {
        public Master_s_labelspecUpdate()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_labelspecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_labelspecSaveRes();

            dataRes.data = res;

            //Model.Table.Mssql.master_s_labelspec LabelSpecReq;
            try
            {
                Ado.Mssql.Table.LabelSpec.master_s_labelspec.GetInstant().Update(dataReq).ToString();
                res.status = "S";
                res.statusdesc = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                res.status = "F";
                res.message = ex.Message;
            }
        }
    }
}
