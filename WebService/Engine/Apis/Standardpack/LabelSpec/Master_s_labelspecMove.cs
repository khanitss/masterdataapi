﻿using Model.Request.Standardpack.LabelSpec;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.LabelSpec
{
    public class Master_s_labelspecMove : Base<label_movemodel>
    {
        public Master_s_labelspecMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(label_movemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Model.Response.Stadardpack.LabelSpec.Master_s_labelspecMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.master_s_labelspec _LableSpecMov;


            foreach (var x in dataReq.label_move)
            {
                var resp = new Model.Response.Stadardpack.LabelSpec.Master_s_labelspecMoveRes();
                try
                {
                    _LableSpecMov = new Model.Table.Mssql.master_s_labelspec();
                    _LableSpecMov.labelspec_id = x.labelspec_id;
                    _LableSpecMov.user_id = x.cancel_id;
                    _LableSpecMov.user_name = x.cancel_name;

                    Ado.Mssql.Table.LabelSpec.master_s_labelspec.GetInstant().Move(_LableSpecMov);

                    resp.status = "s";
                    resp.statusdesc = "S : ทำรายการสำเร็จ(Success)";
                    resp.cancel_id = _LableSpecMov.user_id;
                    resp.cancel_name = _LableSpecMov.user_name;
                    resp.cancel_date = DateTime.Now;
                    resp.labelspec_id = _LableSpecMov.labelspec_id;
                    res.Add(resp);

                }
                catch (Exception ex)
                {
                    resp.statusdesc = "F : ทำรายการไม่สำเร็จ (Fail)";
                    resp.status = "F";
                    resp.message = ex.Message;
                    res.Add(resp);
                }
            }
        }
    }
}
