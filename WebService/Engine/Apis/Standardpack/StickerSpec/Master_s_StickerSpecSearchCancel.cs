﻿using Model.Request.Standardpack.StickerSpec;
using Model.Response;
using Model.Response.Standardpack.StickerSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StickerSpec
{
    public class Master_s_StickerSpecSearchCancel : Base<Master_s_StickerSpecSearchReq>
    {
        public Master_s_StickerSpecSearchCancel ()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(Master_s_StickerSpecSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StickerSpecSearchCancelRes>();
            dataRes.data = res;

            Ado.Mssql.Table.StickerSpec.master_u_stickerspec.GetInstant().SearchCancel(dataReq).ForEach(x =>
            {
                res.Add(new Master_s_StickerSpecSearchCancelRes()
                {
                    stickerspec_id = x.stickerspec_id,
                    tsale = x.tsale,
                    cus_id = x.cus_id,
                    cus_cod = x.cus_cod,
                    cus_name = x.cus_name,
                    label_id =x.label_id,
                    label_cd= x.label_cd,
                    label_name = x.label_name,
                    sticker_id = x.sticker_id,
                    sticker_cd = x.sticker_cd,
                    sticker_name = x.sticker_name,
                    stretchingtype_code = x.stretchingtype_code,
                    stretchingtype_name = x.stretchingtype_name,
                    status = x.status,
                    effdate_st = x.effdate_st,
                    effdate_en =x.effdate_en,
                    user_id = x.user_id,
                    user_name = x.user_name,
                    user_date= x.user_date,
                    cancel_id = x.cancel_id,
                    cancel_date = x.cancel_date,
                    cancel_name = x.cancel_name
                });
            });

        }

    }
}
