﻿using Model.Request.Standardpack.StickerSpec;
using Model.Response;
using Model.Response.Standardpack.StickerSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StickerSpec
{
    public class Master_s_StickerSpecMove : Base<stickerspec_mavemodel>
    {
        public Master_s_StickerSpecMove()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(stickerspec_mavemodel dataReq, ResponseAPI dataRes)
        {
            var res = new List<Master_s_StickerSpecMoveRes>();
            dataRes.data = res;

            Model.Table.Mssql.StickerSpec.master_s_stickerspec _Stickerspec;
            foreach (var x in dataReq.stickerspec_move)
            {
                var resp = new Model.Response.Standardpack.StickerSpec.Master_s_StickerSpecMoveRes();
                try
                {
                    _Stickerspec = new Model.Table.Mssql.StickerSpec.master_s_stickerspec();
                    _Stickerspec.stickerspec_id = x.stickerspec_id;
                    _Stickerspec.user_id = x.user_id;
                    _Stickerspec.user_name = x.user_name;

                    Ado.Mssql.Table.StickerSpec.master_s_stickerspec.GetInstant().Move(_Stickerspec);
                    resp.stickerspec_id = x.stickerspec_id;
                    resp.cancel_id = x.user_id;
                    resp.cancel_name = x.user_name;
                    resp._status = "S";
                    resp._message = "S : ทำรายการสำเร็จ (Success)";

                    res.Add(resp);

                }
                catch (Exception ex)
                {
                    resp._status = "F";
                    resp._message = ex.ToString();

                    res.Add(resp);
                }
            }
        }

    }
}
