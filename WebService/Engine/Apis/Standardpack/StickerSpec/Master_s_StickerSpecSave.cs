﻿using Model.Request.Standardpack.StickerSpec;
using Model.Response;
using Model.Response.Standardpack.StickerSpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Standardpack.StickerSpec
{
    public class Master_s_StickerSpecSave : Base<Master_s_StickerSpecSaveReq>
    {
        public Master_s_StickerSpecSave()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(Master_s_StickerSpecSaveReq dataReq, ResponseAPI dataRes)
        {
            var res = new Master_s_StickerSpecSaveRes();
            dataRes.data = res;

            try
            {
                int stickerspec_id = Ado.Mssql.Table.StickerSpec.master_s_stickerspec.GetInstant().Save(new Model.Table.Mssql.StickerSpec.master_s_stickerspec()
                {
                    stickerspec_id = dataReq.stickerspec_id,
                    sticker_id = dataReq.sticker_id,
                    sticker_cd = dataReq.sticker_cd,
                    sticker_name = dataReq.sticker_name,
                    stretchingtype_code = dataReq.stretchingtype_code,
                    stretchingtype_name = dataReq.stretchingtype_name,
                    label_id =dataReq.label_id,
                    label_cd = dataReq.label_cd,
                    label_name = dataReq.label_name,
                    cus_id = dataReq.cus_id,
                    cus_cod = dataReq.cus_cod,
                    cus_name = dataReq.cus_name,
                    tsale = dataReq.tsale,
                    status = dataReq.status,
                    effdate_st =dataReq.effdate_st,
                    effdate_en = dataReq.effdate_en,
                    user_id = dataReq.user_id,
                    user_name = dataReq.user_name
                });
                res.stickerspec_id = stickerspec_id;
                res.sticker_id = dataReq.sticker_id;
                res.sticker_cd = dataReq.sticker_cd;
                res.sticker_name = dataReq.sticker_name;
                res.stretchingtype_code = dataReq.stretchingtype_code;
                res.stretchingtype_name = dataReq.stretchingtype_name;
                res.label_id = dataReq.label_id;
                res.label_cd = dataReq.label_cd;
                res.label_name = dataReq.label_name;
                res.cus_id = dataReq.cus_id;
                res.cus_cod = dataReq.cus_cod;
                res.cus_name = dataReq.cus_name;
                res.tsale = dataReq.tsale;
                res.status = dataReq.status;
                res.effdate_st = dataReq.effdate_st;
                res.effdate_en = dataReq.effdate_en;
                res.user_id = dataReq.user_id;
                res.user_name = dataReq.user_name;
                res._status = "S";
                res._message = "S : ทำรายการสำเร็จ(Success)";
            }
            catch (Exception ex)
            {
                res._status = "F";
                res._message = ex.ToString();
            }

        }

    }
}
