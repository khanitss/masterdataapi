﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_colorSearch : Base<View_master_s_colorSearchReq>
    {
        public View_master_s_colorSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_colorSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_colorSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_color.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_colorSearchRes
                {
                    color_code = x.color_code,
                    color_name = x.color_name,
                    new_color_code = x.new_color_code,
                    new_color_name = x.new_color_name,
                    old_color_code = x.old_color_code,
                    old_color_name = x.old_color_name
                });
            });
        }
    }
}
