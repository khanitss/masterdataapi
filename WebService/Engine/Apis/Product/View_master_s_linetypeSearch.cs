﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_linetypeSearch : Base<View_master_s_linetypeSearchReq>
    {
        public View_master_s_linetypeSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_linetypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_linetypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_linetype.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_linetypeSearchRes()
                {
                    producttypecode = x.PRODUCTTYPECODE,
                    linetypecode = x.LINETYPECODE,
                    desc1 = x.DESC1,
                    qty1 = x.QTY1,
                    qty1unit = x.QTY1UNIT,
                    qty2 = x.QTY2,
                    qty2unit = x.QTY2UNIT,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }
}
