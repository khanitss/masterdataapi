﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_v_bagorderSearch : Base<View_master_v_bagorderSearchReq>
    {
       public View_master_v_bagorderSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_v_bagorderSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_v_bagorderSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_bagorder.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_v_bagorderSearchRes
                {
                    bag_id = x.bag_id,
                    bag_cd= x.bag_cd,
                    bag_cd_old = x.bag_cd_old,
                    bag_name_en = x.bag_name_en,
                    length = x.length,
                    width = x.width,
                    bag_name_th = x.bag_name_th,
                    last_date_use = x.last_date_use,
                    last_order_use = x.last_order_use,
                    last_user_datetime = x.last_user_datetime,
                    last_user_id = x.last_user_id,
                    last_user_name = x.last_user_name
                });
            });
        }
    }
}
