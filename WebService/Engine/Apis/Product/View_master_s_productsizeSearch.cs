﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_productsizeSearch : Base<View_master_s_productsizeSearchReq>
    {
        public View_master_s_productsizeSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_productsizeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_productsizeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_productsize.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_productsizeSearchRes()
                {
                    product_size = x.PRODUCT_SIZE,
                    product_size_desc = x.PRODUCT_SIZE_DESC,
                    product_type = x.PRODUCT_TYPE,
                    product_grp = x.PRODUCT_GRP,
                    productcate = x.PRODUCTCATE,
                    twine_no = x.TWINE_NO,
                    line_no = x.LINE_NO,
                    word = x.WORD,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }
}
