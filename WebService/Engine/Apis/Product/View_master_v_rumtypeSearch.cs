﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_v_rumtypeSearch : Base<View_master_v_rumtypeSearchReq>
    {
        public View_master_v_rumtypeSearch()
        {
            AllowAnonymous = true;

        }
        protected override void ExecuteChild(View_master_v_rumtypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_v_rumtypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_rumtype.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_v_rumtypeSearchRes
                {
                    rumtypeid = x.rumtypeid,
                    rumtypename = x.rumtypename,
                    rumtypenameen = x.rumtypenameen
                });
            });
        }

    }
}
