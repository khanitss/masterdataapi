﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_stretchingtypeSearch : Base<View_master_s_stretchingtypeSearchReq>
    {
        public View_master_s_stretchingtypeSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_stretchingtypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_stretchingtypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_stretchingtype.GetInstant().Search(dataReq).ForEach(x =>
           {
               try
               {
                    res.Add(new View_master_s_stretchingtypeSearchRes()
                    {
                        stretching_desc = x.STRETCHING_DESC,
                        stretching_type = x.STRETCHING_TYPE,
                        status = "S",
                        statusdesc = "S : ทำรายการสำเร็จ(Success)"
                    });
               }
               catch (Exception ex)
               {

                   res.Add(new View_master_s_stretchingtypeSearchRes()
                   {
                       status = "F",
                       statusdesc = "S : ทำรายการสำเร็จ(Success)",
                       message = ex.ToString()

                   });
               }
           });
        }
    }
}
