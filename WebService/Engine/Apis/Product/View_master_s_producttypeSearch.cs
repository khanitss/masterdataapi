﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_producttypeSearch : Base<View_master_s_producttypeSearchReq>
    {
        public View_master_s_producttypeSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_producttypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_producttypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_producttype.GetInstant().Search(dataReq).ForEach(x =>
            {
                try
                {
                    res.Add(new View_master_s_producttypeSearchRes() 
                    {
                        producttypecode = x.PRODUCTTYPECODE,
                        desc1 = x.DESC1,
                        tsale = x.TSALE,
                        digit6 = x.DIGIT6,
                        rum_flag = x.RUM_FLAG,
                        productcate_grp = x.PRODUCTCATE_GRP,
                        product_type = x.PRODUCT_TYPE,
                        productcate_grp_name = x.PRODUCTCATE_GRP_NAME,
                        status = "S",
                        statusdesc = "S : ทำรายการสำเร็จ(Success)"
                    });

                }
                catch (Exception ex)
                {
                    res.Add(new View_master_s_producttypeSearchRes()
                    {
                        status = "F",
                        statusdesc = "S : ทำรายการสำเร็จ(Success)",
                        message = ex.ToString()

                    });
                }
            });
        }
    }
}
