﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_pqgradeSearch : Base<View_master_s_pqgradeSearchReq>
    {
        public View_master_s_pqgradeSearch()
        {
            AllowAnonymous = true;
        }
        protected override void ExecuteChild(View_master_s_pqgradeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_pqgradeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_pqgrade.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_pqgradeSearchRes()
                {
                    pqgradecd = x.PQGradeCD,
                    pqgradedesc = x.PQGradeDesc,
                    pqgradecd_std = x.PQGradeCD_STD,
                    pqgradecd_std_desc = x.PQGradeCD_STD_Desc,
                    pqgradesale = x.PQGRADESALE,
                    pqgradesale_desc = x.PQGradeSale_Desc,
                    outs_id = x.OUTS_ID,
                    outs_name = x.OUTS_NAME,
                    outsource_flag = x.OutSource_Flag,
                    shortdesc = x.ShortDesc,
                    plan_flag = x.plan_Flag,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }
}
