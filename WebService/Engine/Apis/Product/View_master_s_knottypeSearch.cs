﻿using Model.Request.Product;
using Model.Response;
using Model.Response.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Engine.Apis.Product
{
    public class View_master_s_knottypeSearch : Base<View_master_s_knottypeSearchReq>
    {
        public View_master_s_knottypeSearch()
        {
            AllowAnonymous = true;
        }

        protected override void ExecuteChild(View_master_s_knottypeSearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<View_master_s_knottypeSearchRes>();
            dataRes.data = res;

            Ado.Mssql.View.Product.master_v_knottype.GetInstant().Search(dataReq).ForEach(x =>
            {
                res.Add(new View_master_s_knottypeSearchRes
                {
                    knot_type = x.KNOT_TYPE,
                    knot_desc = x.KNOT_DESC,
                    status = "S",
                    statusdesc = "S : ทำรายการสำเร็จ(Success)"
                });
            });
        }
    }
}
