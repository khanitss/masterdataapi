﻿using Microsoft.AspNetCore.Authorization;
using Model.Request.Job;
using Model.Response;
using Model.Response.Job;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Engine.Apis;

namespace WebService.Engine.Jobs
{
    public class vTemploy1Search : Base<vTemploy1SearchReq>
    {
        public vTemploy1Search()
        {
            AllowAnonymous = true;
            PermissionKey = "PUBLIC";
        }

        protected override void ExecuteChild(vTemploy1SearchReq dataReq, ResponseAPI dataRes)
        {
            var res = new List<vTemploy1SearchRes>();
            dataRes.data = res;

            Ado.Mssql.Table.vTemploy1.GetInstant().Search(dataReq).ForEach(x =>
           {
               res.Add(new vTemploy1SearchRes()
               {
                   CODEMPID = x.CODEMPID,
                   NAMEEMPT = x.NAMEEMPT,
                   CODPOS = x.CODPOS,
                   CODPOSNAME = x.CODPOSNAME,
                   CODCOMP = x.CODCOMP,
                   CODCOMPNAME = x.CODCOMPNAME,
                   STAEMP = x.STAEMP
               });
           });
        }
    }
}
