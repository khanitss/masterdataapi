﻿using Microsoft.AspNetCore.Mvc;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace WebService.Controllers
{
    public abstract class Base : ControllerBase
    {
        public dynamic ResponeValid(ResponseAPI res)
        {
            if (res.status == "F")
            {
                return StatusCode(404, new { res.code, message = res.message });
            }
            return res.data;
        }
    }
}
