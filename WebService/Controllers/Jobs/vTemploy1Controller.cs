﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Jobs;

namespace WebService.Controllers.Jobs
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class vTemploy1Controller : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new vTemploy1Search();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
