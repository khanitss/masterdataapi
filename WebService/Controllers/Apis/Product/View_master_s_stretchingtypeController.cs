﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Product;

namespace WebService.Controllers.Apis.Product
{
    [Route("v1/Product/[controller]")]
    [ApiController]
    public class View_master_s_stretchingtypeController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new View_master_s_stretchingtypeSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
