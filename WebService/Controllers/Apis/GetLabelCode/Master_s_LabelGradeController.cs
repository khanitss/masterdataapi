﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.GetLabelCode;

namespace WebService.Controllers.Apis.GetLabelCode
{
    [Route("v1/[controller]")]
    [ApiController]
    public class Master_s_LabelGradeController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_LabelGradeSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
