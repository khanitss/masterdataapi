﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Customer;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Customer/[controller]")]
    [ApiController]
    public class View_master_s_cutomerController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new View_master_s_cutomerSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCusName")]
        public dynamic SearchCusName([FromBody] dynamic data)
        {
            var res = new View_master_s_cutomerSearchCusname();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
