﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack;
using WebService.Engine.Apis.Standardpack.LabelSpec;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class View_master_s_LabelSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new View_master_s_labelspecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        
        [HttpPost("SearchbyCus")]
        public dynamic SearchbyCus([FromBody] dynamic data)
        {
            var res = new View_master_s_labelspecSearchbyCus();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("SearchByID")]
        public dynamic SearchByID([FromBody] dynamic data)
        {
            var res = new View_master_s_labelspecSearchByID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
