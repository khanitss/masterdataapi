﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Fold;

namespace WebService.Controllers.Apis.Standardpack.Fold
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_FoldController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchView")]
        public dynamic SearchView([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSearchView();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_FoldMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
