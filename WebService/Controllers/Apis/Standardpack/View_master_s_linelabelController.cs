﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.LineLabel;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class View_master_s_linelabelController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new View_master_s_linelabelSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID")]
        public dynamic SearchID([FromBody] dynamic data)
        {
            var res = new View_master_s_linelabelSearchID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID_D")]
        public dynamic SearchID_D([FromBody] dynamic data)
        {
            var res = new View_master_s_linelabelSearchID_D();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID_D_U")]
        public dynamic SearchID_D_U([FromBody] dynamic data)
        {
            var res = new View_master_s_linelabelSearchID_D_U();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
