﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.StickerSpec;

namespace WebService.Controllers.Apis.Standardpack.StickerSpec
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_StickerSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_StickerSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_StickerSpecSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_StickerSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_StickerSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
