﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Bag;

namespace WebService.Controllers.Apis.Standardpack.Bag
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_BagController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_BagSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchActive")]
        public dynamic SearchActive([FromBody] dynamic data)
        {
            var res = new Master_s_BagSearchActive();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_BagSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_BagSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_BagMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
