﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Strap;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_StrapController : Base
    {
        [HttpPost("SearchView")]
        public dynamic SearchView([FromBody] dynamic data)
        {
            var res = new Master_s_StrapSearchView();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_StrapSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_StrapSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_StrapSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_StrapMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
