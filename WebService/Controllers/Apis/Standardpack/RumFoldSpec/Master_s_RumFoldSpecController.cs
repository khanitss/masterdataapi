﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.RumFoldSpec;

namespace WebService.Controllers.Apis.Standardpack.RumFoldSpec
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_RumFoldSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_RumFoldSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_RumFoldSpecSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_RumFoldSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_RumFoldSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }

    }
}
