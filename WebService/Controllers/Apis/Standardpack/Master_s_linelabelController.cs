﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.LineLabel;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_linelabelController : Base
    {
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_linelabelSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SaveD")]
        public dynamic SaveD([FromBody] dynamic data)
        {
            var res = new Master_s_linelabelSaveD();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_linelabelMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Edit")]
        public dynamic Edit([FromBody] dynamic data)
        {
            var res = new Master_s_linelabelEdit();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
