﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.BagSpec;

namespace WebService.Controllers.Apis.Standardpack.BagSpec
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_BagSpecController : Base
    {

        [HttpPost("SearchCus")]
        public dynamic SearchCus([FromBody] dynamic data)
        {
            var res = new Master_s_BagSpecSearchCus();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_BagSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_BagSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_BagSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
