﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.CanvasnBarSpec;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_CanvasnBarSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCancel")]
        public dynamic SearchCancel([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSpecSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
