﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack;

namespace WebService.Controllers.Apis
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_labelController : Base
    {
        [HttpPost("Save")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_lableSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_lableMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
       
        [HttpPost("Edit")]
        public dynamic Edit([FromBody] dynamic data)
        {
            var res = new Master_s_lableEdit();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
