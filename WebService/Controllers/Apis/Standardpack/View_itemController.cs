﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class View_itemController : Base
    {
        [HttpPost("SearchItem")]
        public dynamic SearchItem([FromBody] dynamic data)
        {
            var res = new View_itemSearch();
            return ResponeValid(res.Execute(HttpContext, data));

        }
        [HttpPost("SearchAllItemCode")]
        public dynamic SearchAllItemCode([FromBody] dynamic data)
        {
            var res = new View_rmitem();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchItemCode")]
        public dynamic SearchItemCode([FromBody] dynamic data)
        {
            var res = new View_rmitemSearchItemCode();
            return ResponeValid(res.Execute(HttpContext, data));
        }

    }
}
