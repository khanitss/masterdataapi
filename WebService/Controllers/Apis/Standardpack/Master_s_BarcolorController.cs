﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Barcolor;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_BarcolorController : Base
    {
        [HttpPost("SearchView")]
        public dynamic SearchView([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorSearchView();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchU")]
        public dynamic SearchU([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorSearchU();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID")]
        public dynamic SearchID([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorSearchID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_BarcolorMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
