﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Canvas;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_CanvasController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchView")]
        public dynamic SearchView([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasSearchView();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID")]
        public dynamic SearchID([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasSearchID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchU")]
        public dynamic SearchU([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasSearchU();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
