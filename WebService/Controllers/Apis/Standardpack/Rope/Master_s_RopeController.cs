﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.Rope;

namespace WebService.Controllers.Apis.Standardpack.Rope
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_RopeController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchActive")]
        public dynamic SearchActive([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSearchActive();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_RopeMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
