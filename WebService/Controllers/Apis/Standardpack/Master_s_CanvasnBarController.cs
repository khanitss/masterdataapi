﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.CanvasnBar;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_CanvasnBarController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchU")]
        public dynamic SearchU([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSearchU();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchActive")]
        public dynamic SearchActive([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSearchActive();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchUsed")]
        public dynamic SearchUsed([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSearchUsed();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_CanvasnBarMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
