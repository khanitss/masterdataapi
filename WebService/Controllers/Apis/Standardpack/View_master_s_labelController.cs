﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack;
using WebService.Engine.Apis.Standardpack.Label;

namespace WebService.Controllers.Apis
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class View_master_s_labelController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new View_master_s_labelSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchID")]
        public dynamic SearchID([FromBody] dynamic data)
        {
            var res = new View_master_s_labelSearchID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchLabelCd")]
        public dynamic SearchLabelCd([FromBody] dynamic data)
        {
            var res = new Master_s_lableSearchLabelCd();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchActive")]
        public dynamic SearchActive([FromBody] dynamic data)
        {
            var res = new View_master_s_labelSearchActive();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchLastID")]
        public dynamic SearchLastID([FromBody] dynamic data)
        {
            var res = new View_master_s_labelSearchID();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
