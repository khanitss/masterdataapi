﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.FoldSpec;

namespace WebService.Controllers.Apis.Standardpack.FoldSpec
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_FoldSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("SearchbyCus")]
        public dynamic SearchbyCus([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecSearchbyCus();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("SearchByID")]
        public dynamic SearchByID([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecSearchbyId();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("SearchCancelbyID")]
        public dynamic SearchCancelbyID([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecSearchCancel();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_FoldSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
