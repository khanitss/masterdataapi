﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.LabelSpec;

namespace WebService.Controllers.Apis.Standardpack
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_labelspecController : Base
    {
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_labelspecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_labelspecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        
        [HttpPost("Update")]
        public dynamic Update([FromBody] dynamic data)
        {
            var res = new Master_s_labelspecUpdate();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}