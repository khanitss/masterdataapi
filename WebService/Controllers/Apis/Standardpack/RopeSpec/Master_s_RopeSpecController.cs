﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Standardpack.RopeSpec;

namespace WebService.Controllers.Apis.Standardpack.RopeSpec
{
    [Route("v1/Standardpack/[controller]")]
    [ApiController]
    public class Master_s_RopeSpecController : Base
    {
        [HttpPost("Search")]
        public dynamic Search([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSpecSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("SearchCus")]
        public dynamic SearchCus([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSpecSearchCus();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSpecSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
        [HttpPost("Move")]
        public dynamic Move([FromBody] dynamic data)
        {
            var res = new Master_s_RopeSpecMove();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
