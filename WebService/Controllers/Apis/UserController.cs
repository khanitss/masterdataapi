﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.User;

namespace WebService.Controllers.User
{
    [Route("v1/[controller]")]
    [ApiController]
    public class UserController : Base
    {
        [HttpPost("SearchRegis")]
        public dynamic SearchRegis([FromBody] dynamic data)
        {
            var res = new RegisSearch();
            return ResponeValid(res.Execute(HttpContext, data));
        }

        [HttpPost("Save")]
        public dynamic Save([FromBody] dynamic data)
        {
            var res = new UserSave();
            return ResponeValid(res.Execute(HttpContext, data));
        }
    }
}
