﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebService.Engine.Apis.Oauth;

namespace WebService.Controllers.Apis
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class OauthController : Base
    {
        [HttpGet("Access")]
        public async Task<dynamic> Access()
        {
            var res = new OauthAccessTokenGet();
            return await Task.Run(() => ResponeValid(res.Execute(HttpContext)));
        }

        [HttpPost("Login")]
        public async Task<dynamic> Login([FromBody] dynamic data)
        {
            var res = new OauthLogin();
            return await Task.Run(() => ResponeValid(res.Execute(HttpContext, data)));
        }

        [HttpPost("Logout")]
        public async Task<dynamic> Logout()
        {
            var res = new OauthLogout();
            return await Task.Run(() => ResponeValid(res.Execute(HttpContext)));
        }
    }
}
