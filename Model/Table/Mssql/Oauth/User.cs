﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql
{
    public class User : SetupModel
    {
        public string USERNAME;
        public string PASSWORD;
        public string EMPID;
        public string EMPNAME;
        public string CODPOS;
        public string CODPOSNAME;
        public string CODCOMP;
        public string CODCOMPNAME;
        public string STAEMP;
        public string USER_ID;
        public DateTime USER_DATE;
        public DateTime Expired_date;
        public decimal Day_Expired;
    }
}
