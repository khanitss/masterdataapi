﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql
{
    public class mastertToken
    {
        public string Code;
        public string User_ID;
        public string AccessToken_Code;
        public DateTime ExpiryTime;
        public string Type;
        public string Status;
        public int? UpdateBy;
        public DateTime? Timpstamp;
    }
}
