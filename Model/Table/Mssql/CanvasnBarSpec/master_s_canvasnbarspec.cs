﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.CanvasnBarSpec
{
    public class master_s_canvasnbarspec
    {
        public int canvasnbarspec_id;
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string canvasnbar_name;
        public int cus_id;
        public string cus_cod;
        public string cus_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string tsale;

        public string producttype_code;
        public string producttype_desc;
        public string productcate_grp;
        public string twinesize_min;
        public string twinesize_max;
        public string min_product_type;
        public string min_productcate_grp;
        public decimal min_twine_no;
        public decimal min_line_no;
        public string min_word;
        public string min_product_grp;
        public string max_product_type;
        public string max_productcate_grp;
        public decimal max_twine_no;
        public decimal max_line_no;
        public string max_word;
        public string max_product_grp;
        public int label_id;
        public string label_cd;
        public string label_name;
    }
}
