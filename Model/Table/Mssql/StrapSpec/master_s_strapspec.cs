﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.StrapSpec
{
    public class master_s_strapspec
    {
        public int strapspec_id;
        public int strap_id;
        public string strap_cd;
        public string strap_name;
        public int label_id;
        public string label_cd;
        public string label_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
