﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.RumFoldSpec
{
    public class master_s_rumfoldspec
    {
        public int rumfoldspec_id;
        public string rumtypeid;
        public string rumtypename;
        public int fold_id;
        public string fold_code;
        public string fold_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;

    }
}
