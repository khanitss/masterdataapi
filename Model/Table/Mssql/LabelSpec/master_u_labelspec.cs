﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql
{
    public class master_u_labelspec
    {
        public int labelspec_id;
        public String tsale;
        public String cus_cod;
        public String producttype_code;
        public String pqgradepd;
        public decimal stdwei_min;
        public decimal stdwei_max;
        public String twinesize_min;
        public String twinesize_max;
        public decimal depsize_min;
        public decimal depsize_max;
        public decimal depamt_min;
        public decimal depamt_max;
        public decimal len_min;
        public decimal len_max;
        public String knottype_code;
        public String linetype_code;
        public String stretchingtype_code;
        public String old_color_code;
        public String new_color_code;
        public String color_code;
        public int label_id;
        public String label_cd;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public String status;
        public String min_product_type;
        public String min_productcate_grp;
        public decimal min_twine_no;
        public decimal min_line_no;
        public String min_word;
        public String min_product_grp;
        public String max_product_type;
        public String max_productcate_grp;
        public decimal max_twine_no;
        public decimal max_line_no;
        public String max_word;
        public String user_id;
        public String user_name;
        public DateTime user_date;
        public String cancel_id;
        public String cancel_name;
        public DateTime cancel_date;
    }
}
