﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.GetLabelCode
{
    public class master_s_labelpapertype
    {
        public int labelpapertype_id;
        public string labelpapertype_cd;
        public string labelpapertype_name;
    }
}
