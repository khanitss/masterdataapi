﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.GetLabelCode
{
    public class master_acczonelabelcode
    {
        public int acczonelabelcode_id;
        public string acczone;
        public string acczonelabelcode_cd;
        public string acczonelabelcode_name;
    }
}
