﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.LineLabel
{
    public class master_s_linelabel_d
    {
        public int? linelabel_id;
        public int lineitemno_id;
        public decimal lineitemno;
        public int linesubject_id;
        public int show_flag;
        public string user_id;
        public string user_name;
        public string linelabel_desc;
        public string effdate_st;
        public string effdate_en;
        public string path_pic;
        public DateTime user_date;

    }
}
