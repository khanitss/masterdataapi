﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.LineLabel
{
    public class master_s_linesubject
    {
        public int linesubject_id;
        public string linesubject_desc;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
