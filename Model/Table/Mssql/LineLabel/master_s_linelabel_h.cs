﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.LineLabel
{
    public class master_s_linelabel_h
    {
        public int linelabel_id;
        public int cus_id;
        public string cus_name;
        public string cus_cod;
        public int label_id;
        public string label_cd;
        public string label_name_th;
        public string label_name_en;
        public string label_cd_old;
        public string linelabel_desc;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;

    }
}
