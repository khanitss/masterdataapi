﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.LineLabel
{
    public class master_u_linelabel_h
    {
        public int linelabel_id;
        public int cus_id;
        public string cus_cod;
        public int label_id;
        public string label_cd;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string path_pic;
        public string user_id;    
        public string user_name;  
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
