﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.LineLabel
{
    public class master_u_linelabel_d
    {
        public int linelabel_id;
        public int lineitemno_id;
        public decimal lineitemno;
        public int linesubject_id;
        public int show_flag;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
