﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.RopeSpec
{
    public class master_s_ropespec
    {
        public int ropespec_id;
        public string tsale;
        public string cus_cod;
        public string cus_name;
        public string produccttype_code;
        public string producttype_desc;
        public int rope_id;
        public string rope_cd;
        public string rope_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
