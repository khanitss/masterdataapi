﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql
{
    public class master_u_label : SetupModel
    {
        public int label_id;
        public string label_cd;
        public string label_name_th;
        public string label_name_en;
        public string label_cd_old;
        public decimal width;
        public decimal length;
        public string label_type;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public string cancel_date;
        public decimal version;
        public string remark;
    }
}
