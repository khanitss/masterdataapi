﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.Rope
{
    public class master_u_rope
    {
        public int rope_id;
        public string rope_cd;
        public string rope_name;
        public string rope_cd_old;
        public string path_pic;
        public string itemcode;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
