﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.Strap
{
    public class master_u_strap
    {
        public int strap_id;
        public string strap_cd;
        public string strap_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string itemcode;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
