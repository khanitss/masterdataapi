﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.FoldSpec
{
    public class master_u_foldspec
    {
        public int foldspec_id;
        public string foldspec_information;
        public string tsale;
        public string cus_cod;
        public string cus_name;
        public string producttype_code;
        public string producttype_desc;
        public string pqgradepd;
        public string pqgrade_desc;
        public decimal stdwei_min;
        public decimal stdwei_max;
        public string twinesize_min;
        public string twinesize_max;
        public decimal depsize_min;
        public decimal depsize_max;
        public decimal depamt_min;
        public decimal depamt_max;
        public decimal len_min;
        public decimal len_max;
        public string knottype_code;
        public string linetype_code;
        public string stretchingtype_code;
        public string old_color_code;
        public string new_color_code;
        public string color_code;
        public int fold_id;
        public string fold_cd;
        public string fold_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string min_product_type;
        public string min_productcate_grp;
        public decimal min_twine_no;
        public decimal min_line_no;
        public string min_word;
        public string min_product_grp;
        public string max_product_type;
        public string max_productcate_grp;
        public decimal max_twine_no;
        public decimal max_line_no;
        public string max_word;
        public string max_product_grp;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public String cancel_id;
        public String cancel_name;
        public DateTime cancel_date;
    }
}
