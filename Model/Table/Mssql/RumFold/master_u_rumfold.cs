﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.Mssql.RumFold
{
    public class master_u_rumfold
    {
        public int fold_id;
        public string fold_code;
        public string fold_name;
        public string fold_information;
        public string path_pic;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
