﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Customer
{
    public class master_v_customer
    {
        public string CUSCOD;
        public string TNAME;
        public string FNAME;
        public string LNAME;
        public string SHNAME;
        public string ACCZONE;
        public string COUNTYYCODE;
        public string SALEOFFICER;
        public string AREAMNGID;
        public string ASSISID;
        public string TYPECUS;
        public string TSALE;
        public string DSTK_FLAG;
        public string ACTIVE;
    }
}
