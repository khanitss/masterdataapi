﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_productsize
    {
        public string PRODUCT_SIZE;
        public string PRODUCT_SIZE_DESC;
        public string PRODUCT_TYPE;
        public string PRODUCT_GRP;
        public string PRODUCTCATE;
        public string TWINE_NO;
        public string LINE_NO;
        public string WORD;
    }
}
