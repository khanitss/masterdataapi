﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_pqgrade
    {
        public string PQGradeCD;
        public string PQGradeDesc;
        public string PQGradeCD_STD;
        public string PQGradeCD_STD_Desc;
        public string OUTS_ID;
        public string OUTS_NAME;
        public string OutSource_Flag;
        public string ShortDesc;
        public string plan_Flag;
        public string Table_wei;
        public string PQGRADESALE;
        public string PQGradeSale_Desc;
        public string NewProcess_Flag;
        public string EditUserID;
        public string EditDate;
    }
}
