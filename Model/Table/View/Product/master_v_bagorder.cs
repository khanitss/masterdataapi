﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_bagorder
    {
        public int bag_id;
        public string bag_cd;
        public string bag_name_th;
        public string bag_name_en;
        public decimal width;
        public decimal length;
        public string bag_cd_old;
        public DateTime last_date_use;
        public string last_order_use;
        public string last_user_id;
        public string last_user_name;
        public DateTime last_user_datetime;
    }
}
