﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_color
    {
        public string color_code;
        public string old_color_code;
        public string new_color_code;
        public string color_name;
        public string old_color_name;
        public string new_color_name;

    }
}
