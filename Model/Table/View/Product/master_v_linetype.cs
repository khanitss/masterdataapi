﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_linetype
    {
        public string PRODUCTTYPECODE;
        public string LINETYPECODE;
        public string DESC1;
        public string QTY1;
        public string QTY1UNIT;
        public string QTY2;
        public string QTY2UNIT;
    }
}
