﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Table.View.Product
{
    public class master_v_producttype
    {
        public string PRODUCTTYPECODE;
        public string DESC1;
        public string TSALE;
        public string DIGIT6;
        public string GROUPNAME;
        public string STOCKUNIT;
        public string PRODUNIT;
        public string PRODUCT_TYPE;
        public string RUM_FLAG;
        public string INPID;
        public string INPDT;
        public string PRODUCTCATE_GRP;
        public string PRODUCTCATE_GRP_NAME;
    }
}
