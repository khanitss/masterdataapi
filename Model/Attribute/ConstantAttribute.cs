﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Attribute
{
    public class ConstantAttribute : System.Attribute
    {
        public string Description { get; set; }
        public int CoderRequire { get; set; }
        public int Order { get; set; }
        public bool PercentAllow { get; set; }
    }
}
