﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Common
{
    public class ResultModel
    {
        public string _status { get; set; }
        public string _message { get; set; }
    }
}
