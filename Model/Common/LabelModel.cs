﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Common
{
    public class LabelModel
    {
        public int label_id { get; set; }
        public string label_cd { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public DateTime user_date { get; set; }
    }
}
