﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response
{
    public class ResponseStatus
    {
        public string status { get; set; }
        public string message { get; set; }
        public string statusdesc { get; set; }
    }
}
