﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request
{
    public class OauthLoginReq
    {
        public string user_id { get; set; }
        public string user_password { get; set; }
    }
}