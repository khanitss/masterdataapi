﻿using Model.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Product

{
    public class View_master_s_linetypeSearchReq
    {
        public string producttypecode;
        public string linetypecode;
    }
}
