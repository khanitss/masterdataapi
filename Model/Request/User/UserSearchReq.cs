﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.User
{
    public class UserSearchReq
    {
        public string txtSearch { get; set; }
        public List<string> Username { get; set; }
        public List<string> Password { get; set; }
        public List<string> Empid { get; set; }
        public List<string> Empname { get; set; }
    }
}
