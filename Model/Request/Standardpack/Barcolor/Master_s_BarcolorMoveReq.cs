﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Barcolor
{
    public class barcolor_movemodel
    {
        public List<Master_s_BarcolorMoveReq> barcolor_move = new List<Master_s_BarcolorMoveReq>();
    }
    public class Master_s_BarcolorMoveReq
    {
        public int barcolor_id;
        public string barcolor_cd;
        public string user_id;
        public string user_name;
    }
}
