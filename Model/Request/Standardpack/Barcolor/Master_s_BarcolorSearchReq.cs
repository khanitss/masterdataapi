﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Barcolor
{
    public class Master_s_BarcolorSearchReq : SearchReq
    {
        public int barcolor_id;
        public string barcolor_cd;
    }
}