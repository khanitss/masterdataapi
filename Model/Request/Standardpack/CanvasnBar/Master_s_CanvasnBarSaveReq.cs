﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSaveReq
    {
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string canvasnbar_name;
        public int canvas_id;
        public string canvas_cd;
        public string canvas_name;
        public int barcolor_id;
        public string barcolor_cd;
        public string barcolor_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string itemcode;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
