﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSearchReq : SearchReq
    {
        public string canvasnbar_cd;
        public string canvasnbar_id;

    }
}
