﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.CanvasnBar
{
    public class canvasnbar_movemodel
    {
        public List<Master_s_CanvasnBarMoveReq> canvasnbar_move = new List<Master_s_CanvasnBarMoveReq>();
    }
    public class Master_s_CanvasnBarMoveReq
    {
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string user_id;
        public string user_name;
    }
}
