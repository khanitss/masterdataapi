﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.CanvasnBarSpec
{
    public class Master_s_CanvasnBarSpecSearchReq : SearchReq
    {
        public string cus_cod;
        public string canvasnbarspec_id;
        public string canvasnbar_cd;
    }
}
