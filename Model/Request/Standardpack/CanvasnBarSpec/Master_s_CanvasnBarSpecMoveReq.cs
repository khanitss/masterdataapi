﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.CanvasnBarSpec
{
    public class canvasnbarspec_movemodel
    {
        public List<Master_s_CanvasnBarSpecMoveReq> canvasnbarspec_move = new List<Master_s_CanvasnBarSpecMoveReq>();
    }
    public class Master_s_CanvasnBarSpecMoveReq
    {
        public int canvasnbarspec_id;
        public string user_id;
        public string user_name;
    }

}
