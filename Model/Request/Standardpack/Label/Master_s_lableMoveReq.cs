﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack
{
    public class label_movemodel
    {
        public List<Master_s_lableMoveReq> label_move = new List<Master_s_lableMoveReq>();
    }
    public class Master_s_lableMoveReq
    {
        public int label_id;
        public string label_cd;
        public string user_id;
        public string user_name;
    }
}
