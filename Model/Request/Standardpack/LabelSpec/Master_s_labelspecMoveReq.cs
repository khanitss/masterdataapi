﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.LabelSpec
{
    public class label_movemodel
    {
        public List<Master_s_labelspecMoveReq> label_move = new List<Master_s_labelspecMoveReq>();
    }
    public class Master_s_labelspecMoveReq
    {
        public int labelspec_id;
        public string? cancel_id;
        public string? cancel_name;
    }
}
