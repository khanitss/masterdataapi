﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Bag
{
    public class Master_s_BagSearchReq : SearchReq
    {
        public string bag_id;
        public string bag_cd;
    }
}
