﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Bag
{
    public class bag_movemodel
    {
        public List<Master_s_BagMoveReq> bag_move = new List<Master_s_BagMoveReq>();
    }
    public class Master_s_BagMoveReq
    {
        public int bag_id;
        public string user_id;
        public string user_name;
    }
}
