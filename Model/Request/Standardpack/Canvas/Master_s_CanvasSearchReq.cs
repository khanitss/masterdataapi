﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Canvas
{
    public class Master_s_CanvasSearchReq : SearchReq
    {
        public int canvas_id;
        public string canvas_cd;
    }
}
