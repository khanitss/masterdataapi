﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Canvas
{
    public class canvas_movemodel
    {
        public List<Master_s_CanvasMoveReq> canvas_move = new List<Master_s_CanvasMoveReq>();
    }
    public class Master_s_CanvasMoveReq
    {
        public int canvas_id;
        public string canvas_cd;
        public string user_id;
        public string user_name;
    }
}
