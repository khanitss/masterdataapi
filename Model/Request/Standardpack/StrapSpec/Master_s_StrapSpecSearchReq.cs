﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.StrapSpec
{
    public class Master_s_StrapSpecSearchReq : SearchReq
    {
        public string label_id;
        public string label_cd;
        public string strap_id;
        public string strap_cd;
        public string strapspec_id;
    }
}
