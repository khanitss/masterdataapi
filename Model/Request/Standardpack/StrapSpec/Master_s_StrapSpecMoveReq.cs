﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.StrapSpec
{
    public class strapspec_movemodel
    {
        public List<Master_s_StrapSpecMoveReq> strapspec_move = new List<Master_s_StrapSpecMoveReq>();
    }
    public class Master_s_StrapSpecMoveReq
    {
        public int strapspec_id;
        public string user_id;
        public string user_name;
    }
}
