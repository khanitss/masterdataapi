﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Rope
{
    public class Master_s_RopeSearchReq : SearchReq
    {
        public string rope_id;
        public string rope_cd;
    }
}
