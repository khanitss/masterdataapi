﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Rope
{
    public class Master_s_RopeSaveReq
    {
        public int rope_id;
        public string rope_cd;
        public string rope_name;
        public string rope_cd_old;
        public string path_pic;
        public string itemcode;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
    }
}
