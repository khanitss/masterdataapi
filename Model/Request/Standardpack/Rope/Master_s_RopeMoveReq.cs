﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Rope
{
    public class rope_movemodel
    {
        public List<Master_s_RopeMoveReq> rope_move = new List<Master_s_RopeMoveReq>();
    }
    public class Master_s_RopeMoveReq
    {
        public int rope_id;
        public string user_id;
        public string user_name;
    }
}
