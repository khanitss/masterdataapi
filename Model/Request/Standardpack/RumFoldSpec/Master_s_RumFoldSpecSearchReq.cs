﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecSearchReq : SearchReq
    {
        public string rumfoldspec_id;
        public string rumtypeid;
        public string fold_code;
    }
}
