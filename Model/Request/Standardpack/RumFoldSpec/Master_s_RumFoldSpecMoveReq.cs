﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RumFoldSpec
{
    public class rumfoldspec_movemodel
    {
        public List<Master_s_RumFoldSpecMoveReq> rumfoldspec_move = new List<Master_s_RumFoldSpecMoveReq>();
    }
    public class Master_s_RumFoldSpecMoveReq
    {
        public int rumfoldspec_id;
        public string user_id;
        public string user_name;
    }
}
