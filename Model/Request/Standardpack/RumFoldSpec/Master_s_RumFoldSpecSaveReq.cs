﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecSaveReq
    {
        public int rumfoldspec_id;
        public string rumtypeid;
        public string rumtypename;
        public int fold_id;
        public string fold_code;
        public string fold_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
    }
}
