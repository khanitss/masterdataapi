﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.BagSpec
{
    public class Master_s_BagSpecSearchReq
    {
        public string bagspec_id;
        public string cuscod;
        public string txtSearch;
        public string codeSearch;
    }
}
