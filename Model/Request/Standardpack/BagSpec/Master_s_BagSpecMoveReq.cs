﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.BagSpec
{
    public class bagspec_movemodel
    {
        public List<Master_s_BagSpecMoveReq> bagspec_move = new List<Master_s_BagSpecMoveReq>();
    }
    public class Master_s_BagSpecMoveReq
    {
        public int bagspec_id;
        public string user_id;
        public string user_name;
    }
}
