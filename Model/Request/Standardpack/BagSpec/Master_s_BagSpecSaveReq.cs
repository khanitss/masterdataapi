﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.BagSpec
{
    public class Master_s_BagSpecSaveReq
    {
        public int bagspec_id;
        public string tsale;
        public string cus_cod;
        public string cus_name;
        public string produccttype_code;
        public string producttype_desc;
        public string stretchingtype_code;
        public string stretchingtype_desc;
        public int label_id;
        public string label_cd;
        public string label_name;
        public int bag_id;
        public string bag_cd;
        public string bag_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
    }
}
