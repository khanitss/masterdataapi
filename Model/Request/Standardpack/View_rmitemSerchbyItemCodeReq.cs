﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack
{
    public class View_rmitemSerchbyItemCodeReq : SearchReq
    {
        public string ItemCode;
        public string ItemGrp;
    }
}