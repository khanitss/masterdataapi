﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.StickerSpec
{
    public class Master_s_StickerSpecSearchReq : SearchReq
    {
        public string stickerspec_id;
        public string stretchingtype_code;
        public string label_cd;
    }
}
