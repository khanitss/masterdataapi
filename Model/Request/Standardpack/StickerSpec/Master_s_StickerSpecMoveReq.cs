﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.StickerSpec
{
    public class stickerspec_mavemodel
    {
        public List<Master_s_StickerSpecMoveReq> stickerspec_move = new List<Master_s_StickerSpecMoveReq>();
    }
    public class Master_s_StickerSpecMoveReq
    {
        public int stickerspec_id;
        public string user_id;
        public string user_name;
    }
}
