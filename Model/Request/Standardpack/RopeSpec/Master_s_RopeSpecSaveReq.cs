﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSaveReq
    {
        public int ropespec_id;
        public string tsale;
        public string cus_cod;
        public string cus_name;
        public string produccttype_code;
        public string producttype_desc;
        public int rope_id;
        public string rope_cd;
        public string rope_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
    }
}
