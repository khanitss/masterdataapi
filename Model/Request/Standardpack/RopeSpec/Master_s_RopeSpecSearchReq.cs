﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSearchReq
    {
        public string ropespec_id;
        public string cus_cod;
        public string txtSearch;
        public string codeSearch;
    }
}
