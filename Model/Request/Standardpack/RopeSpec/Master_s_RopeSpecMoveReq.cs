﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RopeSpec
{
    public class ropespec_movemodel
    {
        public List<Master_s_RopeSpecMoveReq> ropespec_move = new List<Master_s_RopeSpecMoveReq>();
    }
    public class Master_s_RopeSpecMoveReq
    {
        public int ropespec_id;
        public string user_id;
        public string user_name;
    }
}
