﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RumFold
{
    public class rumfold_movemodel
    {
        public List<Master_s_RumFoldMoveReq> rumfold_move = new List<Master_s_RumFoldMoveReq>();
    }
    public class Master_s_RumFoldMoveReq
    {
        public int fold_id;
        public string user_id;
        public string user_name;
    }
}
