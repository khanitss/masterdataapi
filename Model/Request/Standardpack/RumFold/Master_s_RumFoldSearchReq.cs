﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.RumFold
{
    public class Master_s_RumFoldSearchReq : SearchReq
    {
        public string fold_id;
        public string fold_code;
    }
}
