﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.LineLabel
{
    public class Master_s_linelabelSaveDReq
    {
        public List<lable_dModel> label_d = new List<lable_dModel>();

        public class lable_dModel
        {
            public int linelabel_id;
            public int lineitemno_id;
            public decimal lineitemno;
            public int linesubject_id;
            public int show_flag;
            public string user_id;
            public string user_name;
            public string linelabel_desc;
            public string path_pic;
            public string effdate_st;
            public string effdate_en;
        }
    }
}
