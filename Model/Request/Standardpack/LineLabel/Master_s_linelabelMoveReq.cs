﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.LineLabel
{
    public class linelabel_movemodel
    {
        public List<Master_s_linelabelMoveReq> linelabel_move = new List<Master_s_linelabelMoveReq>();
    }
    public class Master_s_linelabelMoveReq
    {
        public int linelabel_id;
        public string user_id;
        public string user_name;
    }
}
