﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.LineLabel
{
    public class Master_s_linelabelSaveReq
    {
        public List<LineLabelSaveModel> linelabel_save = new List<LineLabelSaveModel>();

        public class LineLabelSaveModel
        {
            public int linelabel_id;
            public string? linelabel_desc;
            public int cus_id;
            public string cus_cod;
            public string cus_name;
            public int label_id;
            public string label_cd;
            public string path_pic;
            public string status;
            public DateTime effdate_st;
            public DateTime effdate_en;
            public string user_id;
            public string user_name;
            public string label_name_th;
        }
        //    public List<lable_dModel> label_d = new List<lable_dModel>();

        //    public class lable_dModel
        //    {
        //        public int linelabel_id;
        //        public decimal lineitemno;
        //        public int linesubject_id;
        //        public int show_flag;
        //        public string user_id;
        //        public string user_name;
        //    }
    }
}