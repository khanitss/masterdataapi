﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.FoldSpec
{
    public class foldspec_movemodel
    {
        public List<Master_s_FoldSpecMoveReq> foldspec_move = new List<Master_s_FoldSpecMoveReq>();
    }
    public class Master_s_FoldSpecMoveReq
    {
        public int foldspec_id;
        public string cancel_id;
        public string cancel_name;
    }
}
