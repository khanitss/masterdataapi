﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecSearchReq : SearchReq
    {
        public string foldspec_id;
        public string fold_cd;
        public string cuscod;
    }
}
