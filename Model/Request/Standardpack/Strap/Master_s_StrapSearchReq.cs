﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Strap
{
    public class Master_s_StrapSearchReq : SearchReq
    {
        public string strap_id;
        public string strap_cd;
    }
}
