﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Strap
{
    public class strap_movemodel
    {
        public List<Master_s_StrapMoveReq> strap_move = new List<Master_s_StrapMoveReq>();
    }
    public class Master_s_StrapMoveReq
    {
        public int strap_id;
        public string strap_name;
        public string user_id;
        public string user_name;
    }
}
