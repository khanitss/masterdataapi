﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Sticker
{
    public class Master_s_StickerSearchReq : SearchReq
    {
        public string sticker_id;
        public string sticker_cd;
    }
}
