﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Sticker
{
    public class sticker_movemodel
    {
        public List<Master_s_StickerMoveReq> sticker_move = new List<Master_s_StickerMoveReq>();
    }
   public class Master_s_StickerMoveReq
    {
        public int sticker_id;
        public string sticker_cd;
        public string user_id;
        public string user_name;
    }
}
