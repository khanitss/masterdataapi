﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Sticker
{
    public class Master_s_StickerSaveReq
    {
        public int sticker_id;
        public string sticker_cd;
        public string sticker_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string itemcode;
        public string sticker_type;
        public decimal version;
        public string remark;
        public string path_pic;
        public string user_id;
        public string user_name;
    }
}
