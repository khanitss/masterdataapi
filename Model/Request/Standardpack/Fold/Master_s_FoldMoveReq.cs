﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Fold
{
    public class fold_movemodel
    {
        public List<Master_s_FoldMoveReq> fold_move = new List<Master_s_FoldMoveReq>();
    }
    public class Master_s_FoldMoveReq
    {
        public int fold_id;
        public string user_id;
        public string user_name;
    }
}
