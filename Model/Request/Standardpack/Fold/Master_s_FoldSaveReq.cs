﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Fold
{
    public class Master_s_FoldSaveReq
    {
        public int fold_id;
        public string fold_code;
        public string fold_name;
        public string fold_information;
        public string path_pic;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
    }
}
