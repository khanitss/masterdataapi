﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Request.Standardpack.Fold
{
    public class Master_s_FoldSearchReq : SearchReq
    {
        public string fold_id;
        public string fold_code;
    }
}
