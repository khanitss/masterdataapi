﻿using Model.Attribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Enum
{
    public enum TokenType
    {
        [ConstantAttribute(Description = "Login")]
        Login = 'L',

        [ConstantAttribute(Description = "Password Reset")]
        PasswordReset = 'P',
    }
}
