﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response
{
    public class OauthLoginRes
    {
        public string user_fullname { get; set; }
        public string user_id { get; set; }
        public string token { get; set; }
    }
}
