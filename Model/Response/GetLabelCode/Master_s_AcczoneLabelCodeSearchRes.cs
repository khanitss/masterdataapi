﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.GetLabelCode
{
    public class Master_s_AcczoneLabelCodeSearchRes
    {
        public int acczonelabelcode_id;
        public string acczone;
        public string acczonelabelcode_cd;
        public string acczonelabelcode_name;
    }
}
