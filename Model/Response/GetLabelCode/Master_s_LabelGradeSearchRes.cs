﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.GetLabelCode
{
    public class Master_s_LabelGradeSearchRes
    {
        public int labelgrade_id;
        public string labelgrade_cd;
        public string labelgrade_name;
    }
}
