﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.GetLabelCode
{
    public class Master_s_LabelPaperTypeSearchRes
    {
        public int labelpapertype_id;
        public string labelpapertype_cd;
        public string labelpapertype_name;
    }
}
