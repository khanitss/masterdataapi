﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.GetLabelCode
{
    public class Master_v_AcczoneSearchRes
    {
        public string ZONECODE;
        public string DESC1;
        public string MAINZONE;
        public string REGZONECODE;
    }
}
