﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Product
{
    public class View_master_s_linetypeSearchRes : ResponseStatus
    {
        public string producttypecode;
        public string linetypecode;
        public string desc1;
        public string qty1;
        public string qty1unit;
        public string qty2;
        public string qty2unit;
    }
}
