﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Product
{
    public class View_master_s_producttypeSearchRes : ResponseStatus
    {
        public string producttypecode;
        public string desc1;
        public string tsale;
        public string digit6;
        public string rum_flag;
        public string product_type;
        public string productcate_grp;
        public string productcate_grp_name;
    }
}
