﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Product
{
    public class View_master_s_productsizeSearchRes : ResponseStatus
    {
        public string product_size;
        public string product_size_desc;
        public string product_type;
        public string product_grp;
        public string productcate;
        public string twine_no;
        public string line_no;
        public string word;

    }
}
