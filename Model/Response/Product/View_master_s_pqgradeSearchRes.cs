﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Product
{
    public class View_master_s_pqgradeSearchRes : ResponseStatus
    {
        public string pqgradecd;
        public string pqgradedesc;
        public string pqgradecd_std;
        public string pqgradecd_std_desc;
        public string pqgradesale;
        public string pqgradesale_desc;
        public string outs_id;
        public string outs_name;
        public string outsource_flag;
        public string shortdesc;
        public string plan_flag;
    }
}
