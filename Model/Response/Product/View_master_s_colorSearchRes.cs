﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Product
{
    public class View_master_s_colorSearchRes
    {
        public string color_code;
        public string old_color_code;
        public string new_color_code;
        public string color_name;
        public string old_color_name;
        public string new_color_name;
    }
}
