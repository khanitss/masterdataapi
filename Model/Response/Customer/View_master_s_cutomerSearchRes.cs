﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Customer
{
    public class View_master_s_cutomerSearchRes : ResponseStatus
    {
        public string cuscod;
        public string tname;
        public string fname;
        public string lname;
        public string shname;
        public string acczone;
        public string countrycode;
        public string saleofficer;
        public string areamngid;
        public string assisid;
        public string typecus;
        public string tsale;
        public string dstk_flag;
        public string active;
    }
}
