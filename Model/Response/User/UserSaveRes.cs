﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.User
{
    public class UserSaveRes
    {
        public string _status { get; set; }
        public string _message { get; set; }
    }
}
