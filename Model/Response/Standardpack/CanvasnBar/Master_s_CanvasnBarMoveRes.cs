﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarMoveRes : ResponseStatus
    {
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
