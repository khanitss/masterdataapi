﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.CanvasnBar
{
    public class Master_s_CanvasnBarSaveRes : ResponseStatus
    {
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string user_id;
        public string user_name;
    }
}
