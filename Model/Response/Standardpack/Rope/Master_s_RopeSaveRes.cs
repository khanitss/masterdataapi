﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Rope
{
    public class Master_s_RopeSaveRes : ResponseStatus
    {
        public int rope_id;
        public string rope_cd;
        public string user_id;
        public string user_name;
    }
}
