﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Rope
{
    public class Master_s_RopeMoveRes : ResponseStatus
    {
        public int rope_id;
        public string cancel_id;
        public string cancel_name;
    }
}
