﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecMoveRes : ResponseStatus
    {
        public int rumfoldspec_id;
        public string cancel_id;
        public string cancel_name;
    }
}
