﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.RumFoldSpec
{
    public class Master_s_RumFoldSpecSaveRes : ResponseStatus
    {
        public int rumfoldspec_id;
        public string rumtypeid;
        public int fold_id;
        public string fold_code;
        public string user_id;
        public string user_name;
    }
}
