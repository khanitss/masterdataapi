﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.StickerSpec
{
    public class Master_s_StickerSpecMoveRes : ResultModel
    {
        public int stickerspec_id;
        public string tsale;
        public int cus_id;
        public string cus_cod;
        public string cus_name;
        public string stretchingtype_code;
        public int label_id;
        public string label_cd;
        public string label_name;
        public int sticker_id;
        public string sticker_cd;
        public string sticker_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
