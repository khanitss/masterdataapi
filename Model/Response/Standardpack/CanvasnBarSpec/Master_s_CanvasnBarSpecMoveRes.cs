﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.CanvasnBarSpec
{
    public class Master_s_CanvasnBarSpecMoveRes : ResultModel
    {
        public int canvasnbarspec_id;
        public int canvasnbar_id;
        public string canvasnbar_cd;
        public string canvasnbar_name;
        public int cus_id;
        public string cus_cod;
        public string cus_name;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
        public string tsale;
    }
}
