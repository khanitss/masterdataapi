﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecMoveRes : ResponseStatus
    {
        public int ropespec_id;
        public string cancel_id;
        public string cancel_name;
    }
}
