﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.RopeSpec
{
    public class Master_s_RopeSpecSaveRes : ResponseStatus
    {
        public int ropespec_id;
        public string user_id;
        public string user_name;
    }
}
