﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.StrapSpec
{
    public class Master_s_StrapSpecSaveRes : ResultModel
    {
        public int strapspec_id;
        public int strap_id;
        public int label_id;
        public string strap_cd;
        public string label_cd;
        public string strap_name;
        public string label_name;
        public string user_id;
    }
}
