﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecMoveRes : ResponseStatus
    {
        public int foldspec_id;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
