﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.FoldSpec
{
    public class Master_s_FoldSpecSaveRes : ResponseStatus
    {
        public int foldspec_id;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
