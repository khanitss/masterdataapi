﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack.LabelSpec
{
    public class Master_s_labelspecMoveRes : ResponseStatus
    {
        public int labelspec_id;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;

    }
}
