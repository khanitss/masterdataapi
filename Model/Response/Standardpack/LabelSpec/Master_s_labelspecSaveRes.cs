﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack.LabelSpec
{
    public class Master_s_labelspecSaveRes : ResponseStatus
    {
        public int labelspec_id { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public DateTime user_date { get; set; }

    }
}
