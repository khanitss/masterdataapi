﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Canvas
{
    public class Master_s_CanvasSearchURes
    {
        public int canvas_id;
        public string canvas_cd;
        public string canvas_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string itemcode;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
