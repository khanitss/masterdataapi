﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Canvas
{
    public class Master_s_CanvasMoveRes : ResponseStatus
    {
        public int canvas_id;
        public string canvas_cd;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}