﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Canvas
{
    public class Master_s_CanvasSaveRes : ResponseStatus
    {
        public int canvas_id;
        public string canvas_cd;
        public string user_id;
        public string user_name;
    }
}
