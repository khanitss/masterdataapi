﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.BagSpec
{
    public class Master_s_BagSpecSaveRes : ResponseStatus
    {
        public int bagspec_id;
        public string user_id;
        public string user_name;
    }
}
