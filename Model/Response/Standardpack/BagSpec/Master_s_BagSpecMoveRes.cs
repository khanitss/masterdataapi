﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.BagSpec
{
    public class Master_s_BagSpecMoveRes : ResponseStatus
    {
        public int bagspec_id;
        public string cancel_id;
        public string cancel_name;
    }
}
