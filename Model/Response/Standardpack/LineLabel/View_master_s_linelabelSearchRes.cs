﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearchRes : ResponseStatus
    {
        public int cus_id;
        public string cus_cod;
        public string cus_name;
        public int label_id;
        public string label_cd;
        public string label_name_th;
        public string label_name_en;
        public string label_cd_old;
        public string linelabel_desc;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string _status;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public int linelabel_id;
    }
}
