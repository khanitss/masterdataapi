﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class Master_s_linelabelMoveRes : ResponseStatus
    {
        public int labelline_id;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
