﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class View_master_s_linesubjectSearchRes : ResponseStatus
    {
        public int linesubject_id;
        public string linesubject_desc;
        public string user_id;
        public string user_name;
        public DateTime user_date;

    }
}
