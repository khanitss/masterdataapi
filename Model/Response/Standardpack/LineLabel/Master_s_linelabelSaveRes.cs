﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class Master_s_linelabelSaveRes : ResponseStatus
    {
        public int? linelabel_id;
        public int lineitemno_id;
        public decimal lineitemno;
        public int linesubject_id;
        public int show_flag;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
