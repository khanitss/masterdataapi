﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearchByIDRes
    {
        public string cus_cod;
        public string cus_name;
        public int label_id;
        public string label_cd;
        public string label_name_th;
        public string path_pic;
    }
}
