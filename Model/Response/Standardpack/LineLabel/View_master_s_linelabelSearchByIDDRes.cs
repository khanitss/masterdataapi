﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.LineLabel
{
    public class View_master_s_linelabelSearchByIDDRes
    {
        public int? linelabel_id;
        public int lineitemno_id;
        public decimal lineitemno;
        public int linesubject_id;
        public int show_flag;
    }
}
