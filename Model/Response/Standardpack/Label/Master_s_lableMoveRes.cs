﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack
{
    public class Master_s_lableMoveRes : ResultModel
    {

        public string statusdesc;
        public int label_id { get; set; }
        public string label_cd { get; set; }
        public string cancel_id { get; set; }
        public string cancel_name { get; set; }
        public DateTime cancel_date { get; set; }
    }
}
