﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack
{
    public class Master_s_lableSaveRes : ResultModel
    {
        public string statusdesc;
        public int label_id { get; set; }
        public string label_cd { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public DateTime user_date { get; set; }

    }
}
