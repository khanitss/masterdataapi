﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack
{
    public class View_master_s_labelSearchRes
    {
        public int label_id;
        public string label_cd;
        public string label_name_th;
        public string label_name_en;
        public string label_cd_old;
        public decimal width;
        public decimal length;
        public string label_type;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string itemcode;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public decimal version;
        public string remark;
        public int labelowner_id;
        public int labelgrade_id;
        public string labelgrade_cd;
        public string cus_cod;
        public string zone_code;
        public int acczonelabelcode_id;
        public string acczonelabelcode_cd;
        public int labelpapertype_id;
        public string labelpapertype_cd;
        public string digit1;
        public string digit23;
        public string digit4;
    }
}
