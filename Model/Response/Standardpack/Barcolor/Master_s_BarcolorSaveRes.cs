﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Barcolor
{
    public class Master_s_BarcolorSaveRes : ResponseStatus
    {
        public int barcolor_id;
        public string barcolor_cd;
        public string user_id;
        public string user_name;
    }
}
