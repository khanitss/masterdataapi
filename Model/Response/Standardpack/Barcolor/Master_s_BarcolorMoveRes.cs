﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Barcolor
{
    public class Master_s_BarcolorMoveRes : ResponseStatus
    {
        public int barcolor_id;
        public string barcolor_cd;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
