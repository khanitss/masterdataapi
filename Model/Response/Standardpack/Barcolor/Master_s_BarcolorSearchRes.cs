﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Barcolor
{
    public class Master_s_BarcolorSearchRes : ResultModel
    {
        public int barcolor_id;
        public string barcolor_cd;
        public string barcolor_name;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string status;
        public string path_pic;
        public string user_id;
        public string user_name;
        public DateTime user_date;
    }
}
