﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Stadardpack
{
    public class View_itemSearchRes : ResultModel
    {
        public string statusdesc;
        public string ItemCode;
        public string ItemName;
    }
}
