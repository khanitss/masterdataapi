﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Bag
{
    public class Master_s_BagSaveRes : ResponseStatus
    {
        public int bag_id;
        public string user_id;
        public string user_name;
    }
}
