﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Bag
{
    public class Master_s_BagSearchCancelRes
    {
        public int bag_id;
        public string bag_cd;
        public string bag_name_th;
        public string bag_name_en;
        public decimal width;
        public decimal length;
        public string bag_cd_old;
        public string path_pic;
        public string status;
        public DateTime effdate_st;
        public DateTime effdate_en;
        public string user_id;
        public string user_name;
        public DateTime user_date;
        public string cancel_id;
        public string cancel_name;
        public DateTime cancel_date;
    }
}
