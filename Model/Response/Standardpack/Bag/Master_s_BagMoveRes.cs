﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Response.Standardpack.Bag
{
    public class Master_s_BagMoveRes : ResponseStatus
    {
        public int bag_id;
        public string cancel_id;
        public string cancel_name;
    }
}
