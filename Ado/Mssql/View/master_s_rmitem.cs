﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View
{
    public class master_s_rmitem : Base
    {
        private static master_s_rmitem instant;

        public static master_s_rmitem GetInstant()
        {
            if (instant == null) instant = new master_s_rmitem();
            return instant;
        }

         public List<Model.Table.View.master_v_rmitem> SearchAllItemCode(Model.Request.Standardpack.View_rmitemSerchbyItemCodeReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ItemCode", $"{d.ItemCode.GetValue()}%");
            param.Add("@ItemGrp", $"{d.ItemGrp.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");


            string cmd = "select * from master_v_rmitem " +
                 $"WHERE (@ItemCode IS NULL OR ItemCode like @ItemCode) " +
                 $"OR (@ItemGrp IS NULL OR ItemCode like @ItemGrp)";

            return Query<Model.Table.View.master_v_rmitem>(cmd, param).ToList();
        }
        public List<Model.Table.View.master_v_rmitem> SearchItemCode(Model.Request.Standardpack.View_rmitemSerchbyItemCodeReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ItemCode", d.ItemCode.GetValue());

            string cmd = "select * from master_v_rmitem " +
                 $"WHERE ItemCode = @ItemCode  order by ItemCode";

            return Query<Model.Table.View.master_v_rmitem>(cmd, param).ToList();
        }
    }
}
