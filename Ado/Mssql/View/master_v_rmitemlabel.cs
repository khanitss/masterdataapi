﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View
{
    public class master_v_rmitemlabel : Base
    {
        private static master_v_rmitemlabel instant;

        public static master_v_rmitemlabel GetInstant()
        {
            if (instant == null) instant = new master_v_rmitemlabel();
            return instant;
        }

        public List<Model.Table.View.master_v_rmitemlabel> SearchItem(Model.Request.Standardpack.View_itemSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ItemCode", $"%{d.ItemCode.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "select * from master_v_rmitemlabel " +
                 $"WHERE (@ItemCode IS NULL OR ItemCode like @ItemCode) " +
                 $"AND (ItemCode Like @txtSearch) order by ItemCode";

            return Query<Model.Table.View.master_v_rmitemlabel>(cmd, param).ToList();
        }

    }
}