﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Customer
{
    public class master_v_customer : Base
    {
        private static master_v_customer instant;

        public static master_v_customer GetInstant()
        {
            if (instant == null) instant = new master_v_customer();
            return instant;
        }

        public List<Model.Table.View.Customer.master_v_customer> Search(Model.Request.Customer.View_master_s_cutomerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", $"%{d.cuscod.GetValue()}%");
            param.Add("@tsale", d.tsale);

            string cmd = "SELECT * FROM master_v_customer " +
                "WHERE (@cuscod IS NULL OR CUSCOD LIKE @cuscod)" +
                "AND (@tsale IS NULL OR tsale LIKE @tsale) order by CUSCOD";

            return Query<Model.Table.View.Customer.master_v_customer>(cmd, param).ToList();
        }
        
        public List<Model.Table.View.Customer.master_v_customer> SearchCusname(Model.Request.Customer.View_master_s_cutomerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", d.cuscod.GetValue());

            string cmd = "SELECT * FROM master_v_customer " +
                "WHERE @cuscod IS NULL OR CUSCOD = @cuscod order by CUSCOD";

            return Query<Model.Table.View.Customer.master_v_customer>(cmd, param).ToList();
        }
    }
}