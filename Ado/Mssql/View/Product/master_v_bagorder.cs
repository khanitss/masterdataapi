﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_bagorder : Base
    {
        private static master_v_bagorder instant;

        public static master_v_bagorder GetInstant()
        {
            if (instant == null) instant = new master_v_bagorder();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_bagorder> Search(Model.Request.Product.View_master_v_bagorderSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", d.bag_id);
            param.Add("@bag_cd", d.bag_cd);
            param.Add("@bag_cd_old", d.bag_cd_old);

            string cmd = "SELECT * FROM master_v_bagorder " +
                "WHERE (@bag_id IS NULL OR bag_id LIKE @bag_id) AND " +
                "(@bag_cd IS NULL OR bag_cd LIKE @bag_cd) AND " +
                "(@bag_cd_old IS NULL OR bag_cd_old LIKE @bag_cd_old ) order by bag_cd" +
                "";

            return Query<Model.Table.View.Product.master_v_bagorder>(cmd, param).ToList();
        }
    }
}
