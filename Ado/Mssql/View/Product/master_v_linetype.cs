﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_linetype : Base
    {
        private static master_v_linetype instant;

        public static master_v_linetype GetInstant()
        {
            if (instant == null) instant = new master_v_linetype();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_linetype> Search(Model.Request.Product.View_master_s_linetypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@producttypecode", $"%{d.producttypecode.GetValue()}%");
            param.Add("@linetypecode", d.linetypecode);

            string cmd = "SELECT * FROM master_v_linetype " +
                "WHERE (@producttypecode IS NULL OR PRODUCTTYPECODE like @producttypecode) " +
                "AND (@linetypecode IS NULL OR LINETYPECODE like @linetypecode) order by LINETYPECODE";

            return Query<Model.Table.View.Product.master_v_linetype>(cmd, param).ToList();
        }
    }
}