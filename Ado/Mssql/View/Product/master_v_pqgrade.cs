﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_pqgrade : Base
    {
        private static master_v_pqgrade instant;

        public static master_v_pqgrade GetInstant()
        {
            if (instant == null) instant = new master_v_pqgrade();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_pqgrade> Search(Model.Request.Product.View_master_s_pqgradeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@pqgradecd", d.pqgradecd);
            param.Add("@pqgradecd_std", d.pqgradecd_std);

            string cmd = "SELECT * FROM master_v_pqgrade " +
                "WHERE (@pqgradecd IS NULL OR PQGradeCD like @pqgradecd) " +
                "AND (@pqgradecd_std IS NULL OR PQGradeCD_STD like @pqgradecd_std ) order by PQGradeCD";

            return Query<Model.Table.View.Product.master_v_pqgrade>(cmd, param).ToList();
        }
    }
}