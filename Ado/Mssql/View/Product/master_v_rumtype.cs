﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_rumtype : Base
    {
        private static master_v_rumtype instant;

        public static master_v_rumtype GetInstant()
        {
            if (instant == null) instant = new master_v_rumtype();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_rumtype> Search(Model.Request.Product.View_master_v_rumtypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumtypeid", $"%{d.rumtypeid.GetValue()}%");

            string cmd = "SELECT * FROM master_v_rumtype WHERE " +
                "(@rumtypeid IS NULL OR rumtypeid LIKE @rumtypeid) order by rumtypeid ";

            return Query<Model.Table.View.Product.master_v_rumtype>(cmd, param).ToList();

        }
    }
}
