﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_knottype : Base
    {
        private static master_v_knottype instant;

        public static master_v_knottype GetInstant()
        {
            if (instant == null) instant = new master_v_knottype();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_knottype> Search(Model.Request.Product.View_master_s_knottypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@knottype_code", d.knottype_code);

            string cmd = "SELECT * FROM master_v_knottype " +
                "WHERE @knottype_code IS NULL OR KNOT_TYPE like @knottype_code order by KNOT_TYPE";

            return Query<Model.Table.View.Product.master_v_knottype>(cmd, param).ToList();
        }
    }
}