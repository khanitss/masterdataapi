﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_stretchingtype : Base
    {
        private static master_v_stretchingtype instant;

        public static master_v_stretchingtype GetInstant()
        {
            if (instant == null) instant = new master_v_stretchingtype();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_stretchingtype> Search(Model.Request.Product.View_master_s_stretchingtypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stretchingtype_code",  $"%{d.stretchingtype_code.GetValue()}%");

            string cmd = "SELECT * FROM master_v_stretchingtype" +
                " WHERE @stretchingtype_code IS NULL OR STRETCHING_TYPE like @stretchingtype_code";

            return Query<Model.Table.View.Product.master_v_stretchingtype>(cmd, param).ToList();
        }
    }
}