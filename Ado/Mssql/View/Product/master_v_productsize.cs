﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_productsize : Base
    {
        private static master_v_productsize instant;

        public static master_v_productsize GetInstant()
        {
            if (instant == null) instant = new master_v_productsize();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_productsize> Search(Model.Request.Product.View_master_s_productsizeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@product_size", $"%{d.product_size.GetValue()}%");
            param.Add("@productcate_grp", $"%{d.productcate_grp.GetValue()}%");

            string cmd = "SELECT * FROM master_v_productsize " +
                "WHERE (@productcate_grp IS NULL OR PRODUCTCATE like @productcate_grp) " +
                "AND (@product_size IS NULL OR Product_size like @product_size) order by Product_size";

            return Query<Model.Table.View.Product.master_v_productsize>(cmd, param).ToList();
        }
    }
}