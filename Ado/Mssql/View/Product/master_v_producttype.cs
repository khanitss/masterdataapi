﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_producttype : Base
    {
        private static master_v_producttype instant;

        public static master_v_producttype GetInstant()
        {
            if (instant == null) instant = new master_v_producttype();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_producttype> Search(Model.Request.Product.View_master_s_producttypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@producttype_code", $"%{ d.producttype_code.GetValue()}%");

            string cmd = "SELECT * FROM master_v_producttype " +
                "WHERE @producttype_code IS NULL OR PRODUCTTYPECODE LIKE @producttype_code order by PRODUCTTYPECODE";

            return Query<Model.Table.View.Product.master_v_producttype>(cmd, param).ToList();
        }
    }
}