﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View.Product
{
    public class master_v_color : Base
    {
        private static master_v_color instant;

        public static master_v_color GetInstant()
        {
            if (instant == null) instant = new master_v_color();
            return instant;
        }

        public List<Model.Table.View.Product.master_v_color> Search(Model.Request.Product.View_master_s_colorSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@color_code", d.color_code);

            string cmd = "SELECT * FROM master_v_color " +
                "WHERE @color_code IS NULL OR COLOR_CODE like @color_code order by COLOR_CODE";

            return Query<Model.Table.View.Product.master_v_color>(cmd, param).ToList();
        }
    }
}
