﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.View
{
    public class master_v_labelorder : Base
    {
        private static master_v_labelorder instant;

        public static master_v_labelorder GetInstant()
        {
            if (instant == null) instant = new master_v_labelorder();
            return instant;
        }

        public List<Model.Table.View.master_v_labelorder> SearchLabelCd(Model.Request.Standardpack.Master_s_lableSearchLabelCdReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", d.txtSearch);

            string cmd = "select * from master_v_labelorder where labelcd like @txtSearch";

            return Query<Model.Table.View.master_v_labelorder>(cmd, param).ToList();
        }
    }
}
