﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.LineLabel
{
    public class master_s_linesubject : Base
    {
        private static master_s_linesubject instant;
        public static master_s_linesubject GetInstant()
        {
            if (instant == null) instant = new master_s_linesubject();
            return instant;
        }
        public List<Model.Table.Mssql.LineLabel.master_s_linesubject> Search(Model.Request.Standardpack.LineLabel.View_master_s_linesubjectSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", d.txtSearch);

            string cmd = "SELECT * FROM master_s_linesubject where @txtSearch IS NULL OR linesubject_id like @txtSearch OR linesubject_desc like @txtSearch";

            return Query<Model.Table.Mssql.LineLabel.master_s_linesubject>(cmd, param).ToList();
        }
    }
}
