﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.LineLabel
{
    public class master_s_linelabel_d : Base
    {
        private static master_s_linelabel_d instant;

        public static master_s_linelabel_d GetInstant()
        {
            if (instant == null) instant = new master_s_linelabel_d();
            return instant;
        }

        public Model.Table.Mssql.LineLabel.master_s_linelabel_d Get(Model.Table.Mssql.LineLabel.master_s_linelabel_d d)
        {
            DynamicParameters param = new Dapper.DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);
            param.Add("@linesubject_id", d.linesubject_id);

            string cmd = "SELECT * From master_s_linelabel_d " +
                "WHERE linelabel_id = @linelabel_id and linesubject_id = @linesubject_id " +
                "ORDER BY linelabel_id;";

            var res = Query<Model.Table.Mssql.LineLabel.master_s_linelabel_d>(cmd, param).FirstOrDefault();
            return res;
        }
        public int Save(Model.Table.Mssql.LineLabel.master_s_linelabel_d d)
        {
            var item = Get(new Model.Table.Mssql.LineLabel.master_s_linelabel_d
            {
                linelabel_id = d.linelabel_id,
                linesubject_id = d.linesubject_id
            });
            if ( item!= null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        private int Insert(Model.Table.Mssql.LineLabel.master_s_linelabel_d d)
        {
            DynamicParameters param = new Dapper.DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);
            //param.Add("@lineitemno_id", d.lineitemno_id);
            param.Add("@lineitemno", d.lineitemno);
            param.Add("@linesubject_id", d.linesubject_id);
            param.Add("@show_flag", d.show_flag);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_linelabel_d " +
                "([linelabel_id],[lineitemno],[linesubject_id],[show_flag],[user_id],[user_name],[user_date]) " +
                " VALUES " +
                "(@linelabel_id,@lineitemno,@linesubject_id,@show_flag,@user_id,@user_name,GetDate()) " +
                "SELECT SCOPE_IDENTITY();";

            var res = ExecuteScalar<int>(cmd, param);
            return res;
        }

        public List<Model.Table.Mssql.LineLabel.master_s_linelabel_d> SearchByID(Model.Request.Standardpack.LineLabel.View_master_s_linelabelSearchByIDReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);

            string cmd = "SELECT * FROM master_s_linelabel_d WHERE linelabel_id = @linelabel_id order by lineitemno ";

            return Query<Model.Table.Mssql.LineLabel.master_s_linelabel_d>(cmd, param).ToList();
        }

        public int Update(Model.Table.Mssql.LineLabel.master_s_linelabel_d d)
        {
            DynamicParameters param = new Dapper.DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);
            param.Add("@lineitemno_id", d.lineitemno_id);
            param.Add("@lineitemno", d.lineitemno);
            param.Add("@linesubject_id", d.linesubject_id);
            param.Add("@show_flag", d.show_flag);
            param.Add("@linelabel_desc", d.linelabel_desc);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);


            string cmd = "UPDATE master_s_linelabel_d SET " +
                "show_flag = @show_flag, user_id = @user_id, user_name =@user_name, user_date = GetDate() " +
                "WHERE linelabel_id = @linelabel_id AND linesubject_id = @linesubject_id AND lineitemno_id = @lineitemno_id " +
                " UPDATE master_s_linelabel_h SET linelabel_desc = @linelabel_desc , path_pic =@path_pic , effdate_st = @effdate_st , effdate_en = @effdate_en  Where linelabel_id = @linelabel_id ";

            var res = ExecuteScalar<int>(cmd, param);

            return res;
        }
    }
}
