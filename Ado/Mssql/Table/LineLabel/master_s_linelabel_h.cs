﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.LineLabel
{
    public class master_s_linelabel_h : Base
    {
        private static master_s_linelabel_h instant;
        public static master_s_linelabel_h GetInstant()
        {
            if (instant == null) instant = new master_s_linelabel_h();
            return instant;
        }
        public List<Model.Table.Mssql.LineLabel.master_s_linelabel_h> Search(Model.Request.Standardpack.LineLabel.View_master_s_linelabelSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", $"%{d.cuscod.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_linelabel_h " +
                "WHERE (@cuscod IS NULL OR cus_cod like @cuscod)" +
                "AND (@label_cd IS NULL OR label_cd like @label_cd)" +
                "AND (cus_cod like @txtSearch OR label_cd like @txtSearch)";

            return Query<Model.Table.Mssql.LineLabel.master_s_linelabel_h>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.LineLabel.master_s_linelabel_h> SearchByID(Model.Request.Standardpack.LineLabel.View_master_s_linelabelSearchByIDReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);

            string cmd = "SELECT * FROM master_s_linelabel_h WHERE linelabel_id = @linelabel_id ";

            return Query<Model.Table.Mssql.LineLabel.master_s_linelabel_h>(cmd, param).ToList();
        }

        //public  FindDesc

        //{
        //    DynamicParameters param = new DynamicParameters();
        //    param.Add("@linelabel_id", d.linelabel_id);
        //    param.Add("@linesubject_id", d.linesubject_id);

        //    string cmd = "WITH q(linelabel_id,linesubject_desc) AS (SELECT A.linelabel_id, B.linesubject_desc FROM master_s_linelabel_d A "+
        //                 "LEFT JOIN master_s_linesubject B on A.linesubject_id = B.linesubject_id WHERE A.show_flag = 1 AND A.linelabel_id = @linelabel_id )  SELECT* FROM( "+
        //                 "SELECT DISTINCT linelabel_id from q) qo "+
        //                 "CROSS APPLY "+
        //                 "(SELECT CASE ROW_NUMBER() OVER(ORDER by linesubject_desc) WHEN 1 THEN '' ELSE ', ' END + qi.linesubject_desc "+
        //                 "FROM q qi WHERE qi.linelabel_id = qo.linelabel_id ORDER BY linelabel_id FOR XML PATH('')) qi(linesubject_desc) ";
        //    var res = Query<Model.Table.Mssql.LineLabel.master_s_linelabel_h>(cmd, param).ToList();
        //    return res;
        //}

        public int Save(Model.Table.Mssql.LineLabel.master_s_linelabel_h d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_cod", d.cus_cod.GetValue());
            param.Add("@cus_name", d.cus_name.GetValue());
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd.GetValue());
            param.Add("@label_name_th", d.label_name_th.GetValue());
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status.GetValue());
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@linelabel_desc", d.linelabel_desc.GetValue());

            string cmd = "INSERT INTO master_s_linelabel_h " +
                "(cus_id, cus_cod,cus_name, label_id, label_cd, effdate_st, effdate_en, status, path_pic, user_id, user_name, user_date , label_name_th, linelabel_desc) " +
                "VALUES" +
                "(@cus_id, @cus_cod, @cus_name, @label_id, @label_cd, @effdate_st, @effdate_en, @status,@path_pic, @user_id,@user_name , GetDate(), @label_name_th , @linelabel_desc) " +
                "SELECT SCOPE_IDENTITY(); ";
            
            var linelabel_id = ExecuteScalar<int>(cmd, param);
            return linelabel_id;
        }
        public int Move(Model.Table.Mssql.LineLabel.master_s_linelabel_h d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_linelabel_h " +
                "( linelabel_id,cus_id,cus_cod,label_id,label_cd,effdate_st,effdate_en,path_pic,user_id,user_name,user_date) " +
                "SELECT " +
                " linelabel_id,cus_id,cus_cod,label_id,label_cd,effdate_st,effdate_en,path_pic,user_id,user_name,user_date FROM master_s_linelabel_h " +
                "WHERE linelabel_id = @linelabel_id " +
                "UPDATE master_u_linelabel_h SET cancel_id = @user_id , cancel_name = @user_name ,cancel_date = GetDate(), status = 'C' WHERE linelabel_id = @linelabel_id " +
                "UPDATE master_s_linelabel_h SET status = 'C' WHERE linelabel_id = @linelabel_id ";

            var res = ExecuteNonQuery(cmd, param);

            string cmd_linelabel_d = "INSERT INTO master_u_linelabel_d " +
                "(linelabel_id,lineitemno_id,lineitemno,linesubject_id,show_flag,user_id,user_name,user_date) " +
                "SELECT " +
                "linelabel_id,lineitemno_id,lineitemno,linesubject_id,show_flag,user_id,user_name,user_date FROM master_s_linelabel_d " +
                "WHERE linelabel_id = @linelabel_id " +
                "UPDATE master_u_linelabel_d SET cancel_id = @user_id , cancel_name = @user_name, cancel_date = GetDate() WHERE linelabel_id = @linelabel_id " +
                "DELETE FROM master_s_linelabel_d WHERE linelabel_id = @linelabel_id ";
            
            var resp = ExecuteNonQuery(cmd_linelabel_d, param);

            return res;
        }

        public int Update(Model.Table.Mssql.LineLabel.master_s_linelabel_h d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@linelabel_id", d.linelabel_id);
            param.Add("@linelabel_desc", d.linelabel_desc);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_linelabel_h " +
                "SET effdate_st = @effdate_st,effdate_en = @effdate_en, path_pic =@path_pic, user_id =@user_id, user_name = @user_name , user_date = GetDate() , linelabel_desc = @linelabel_desc " +
                "Where linelabel_id= @linelabel_id " +
                "SELECT SCOPE_IDENTITY(); ";

            var linelabel_id = ExecuteScalar<int>(cmd, param);
            return linelabel_id;
        }
    }
}
