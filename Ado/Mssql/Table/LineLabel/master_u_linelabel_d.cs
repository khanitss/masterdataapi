﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.LineLabel
{
   public class master_u_linelabel_d : Base
    {
        private static master_u_linelabel_d instant;

        public static master_u_linelabel_d GetInstant()
        {
            if (instant == null) instant = new master_u_linelabel_d();
            return instant;
        }
        public List<Model.Table.Mssql.LineLabel.master_u_linelabel_d> SearchByID(Model.Request.Standardpack.LineLabel.View_master_s_linelabelSearchByIDReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@linelabel_id", d.linelabel_id);

            string cmd = "SELECT * FROM master_u_linelabel_d WHERE linelabel_id = @linelabel_id order by lineitemno ";

            return Query<Model.Table.Mssql.LineLabel.master_u_linelabel_d>(cmd, param).ToList();
        }
    }
}
