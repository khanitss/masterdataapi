﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Strap
{
    public class master_u_strap : Base
    {
        private static master_u_strap instant;

        public static master_u_strap GetInstant()
        {
            if (instant == null) instant = new master_u_strap();
            return instant;
        }

        public Model.Table.Mssql.Strap.master_u_strap Get(Model.Table.Mssql.Strap.master_u_strap d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", d.strap_id);

            string cmd = "SELECT * FROM master_u_strap WHERE strap_id = @strap_id";

            var res = Query<Model.Table.Mssql.Strap.master_u_strap>(cmd, param).FirstOrDefault();

            return res;
        }
        public List<Model.Table.Mssql.Strap.master_u_strap> SearchView(Model.Request.Standardpack.Strap.Master_s_StrapSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", $"%{d.strap_id.GetValue()}%");
            param.Add("@strap_cd", $"%{d.strap_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = " SELECT * FROM master_u_strap WHERE " +
                "(@strap_id IS NULL OR strap_id like @strap_id) AND " +
                "(@strap_cd IS NULL OR strap_cd like @strap_cd) AND " +
                "(@txtSearch IS NULL OR strap_cd like @txtSearch OR strap_name like @txtSearch ) order by strap_id";

            return Query<Model.Table.Mssql.Strap.master_u_strap>(cmd, param).ToList();
        }
    }
}