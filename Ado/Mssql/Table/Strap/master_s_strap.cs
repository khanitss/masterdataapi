﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Strap
{
    public class master_s_strap : Base
    {
        private static master_s_strap instant;
        
        public static master_s_strap GetInstant()
        {
            if (instant == null) instant = new master_s_strap();
            return instant;
        }

        public Model.Table.Mssql.Strap.master_s_strap Get(Model.Table.Mssql.Strap.master_s_strap d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", d.strap_id);

            string cmd = "SELECT * FROM master_s_strap WHERE strap_id = @strap_id";

            var res = Query<Model.Table.Mssql.Strap.master_s_strap>(cmd, param).FirstOrDefault();
           
            return res;
        }
        public List<Model.Table.Mssql.Strap.master_s_strap> SearchView(Model.Request.Standardpack.Strap.Master_s_StrapSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", $"%{d.strap_id.GetValue()}%");
            param.Add("@strap_cd", $"%{d.strap_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = " SELECT * FROM master_s_strap WHERE " +
                "(@strap_id IS NULL OR strap_id like @strap_id) AND " +
                "(@strap_cd IS NULL OR strap_cd like @strap_cd) AND " +
                "(@txtSearch IS NULL OR strap_cd like @txtSearch OR strap_name like @txtSearch ) order by strap_id";

            return Query<Model.Table.Mssql.Strap.master_s_strap>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.Strap.master_s_strap> Search(Model.Request.Standardpack.Strap.Master_s_StrapSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", $"%{d.strap_id.GetValue()}%");
            param.Add("@strap_cd", $"%{d.strap_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = " SELECT * FROM master_s_strap WHERE " +
                "(@strap_id IS NULL OR strap_id like @strap_id) AND " +
                "(@strap_cd IS NULL OR strap_cd like @strap_cd) AND " +
                "(@txtSearch IS NULL OR strap_cd like @txtSearch OR strap_name like @txtSearch ) AND status = 'A' order by strap_cd";

            return Query<Model.Table.Mssql.Strap.master_s_strap>(cmd, param).ToList();
        }
        public int Save(Model.Table.Mssql.Strap.master_s_strap d)
        {
            var item = Get(new Model.Table.Mssql.Strap.master_s_strap
            {
                strap_id = d.strap_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        private int Insert(Model.Table.Mssql.Strap.master_s_strap d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_cd", d.strap_cd);
            param.Add("@strap_name", d.strap_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@itemcode", d.itemcode);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_strap " +
                "(strap_cd,strap_name,effdate_st,effdate_en,status,itemcode,user_id,user_name,user_date) " +
                "VALUES " +
                "(@strap_cd,@strap_name,@effdate_st,@effdate_en,@status,@itemcode,@user_id,@user_name, GetDate()) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.Strap.master_s_strap d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", d.strap_id);
            param.Add("@strap_cd", d.strap_cd);
            param.Add("@strap_name", d.strap_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@itemcode", d.itemcode);
            param.Add("@user_id", d.user_id);   
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_strap " +
                "SET strap_cd = @strap_cd ,strap_name =@strap_name,effdate_st =@effdate_st ,effdate_en =@effdate_en " +
                ",status =@status,itemcode =@itemcode,user_id = @user_id,user_name = @user_name ,user_date = GetDate() " +
                "WHERE strap_id = @strap_id SELECT @strap_id AS strap_id";

            var strap_id = ExecuteScalar<int>(cmd, param);

            return strap_id;
        }
        public int Move(Model.Table.Mssql.Strap.master_s_strap d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", d.strap_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_strap (strap_id,strap_cd,strap_name,effdate_st,effdate_en,itemcode,user_id,user_name,user_date) " +
                "SELECT strap_id,strap_cd,strap_name,effdate_st,effdate_en,itemcode,user_id,user_name,user_date FROM master_s_strap WHERE strap_id = @strap_id " +
                "UPDATE master_u_strap SET status = 'C', cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE strap_id = @strap_id " +
                "UPDATE master_s_strap SET status = 'C' WHERE strap_id = @strap_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
