﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table
{
    public class User : Base
    {
        private static User instant;

        public static User GetInstant()
        {
            if (instant == null) instant = new User();
            return instant;
        }

        public List<Model.Table.Mssql.User> SearchRegis(Model.Request.User.RegisSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@USERNAME", d.USERNAME);

            string cmd = "SELECT * FROM master_s_user WHERE USERNAME = @USERNAME";

            var res = Query<Model.Table.Mssql.User>(cmd, param).ToList();
            return res;
        }

        public List<Model.Table.Mssql.User> Search(Model.Request.User.UserSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Username", d.Username.ListNull());
            param.Add("@Empid", d.Empid.ListNull());
            param.Add("@Empname", d.Empname.ListNull());
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_user " +
                $"WHERE (@Username IS NULL OR Username IN ('{ d.Username.Join("','") }') OR Username LIKE @Username) " +
                $"AND (@Empid IS NULL OR Empid IN ('{ d.Empid.Join("','") }')) " +
                $"AND (@Empname IS NULL OR Empname IN ('{ d.Empname.Join("','") }')) " +
                "ORDER BY Username;";
            var res = this.Query<Model.Table.Mssql.User>(cmd, param).ToList();
            return res;
        }

        public List<Model.Table.Mssql.User> Save(Model.Request.UserSaveReq d)
        {
            var pass = Core.Util.EncryptUtil.Hash(d.PASSWORD.Trim());

            DynamicParameters param = new DynamicParameters();
            param.Add("@USERNAME", d.USERNAME);
            param.Add("@PASSWORD", Core.Util.EncryptUtil.Hash(d.PASSWORD.Trim()));
            param.Add("@EMPNAME", d.EMPNAME);
            param.Add("@CODPOS", d.CODPOS);
            param.Add("@CODPOSNAME", d.CODPOSNAME);
            param.Add("@CODCOMP", d.CODCOMP);
            param.Add("@CODCOMPNAME", d.CODCOMPNAME);
            param.Add("@STAEMP", d.STAEMP);
            param.Add("@USER_ID", d.USER_ID);
            param.Add("@USER_DATE", d.USER_DATE);
            param.Add("@Expired_date", d.Expired_date);
            param.Add("@Day_Expired", d.Day_Expired);


            string cmd = "INSERT INTO master_s_user (USERNAME,PASSWORD,EMPID,EMPNAME,CODPOS,CODPOSNAME,CODCOMP,CODCOMPNAME,STAEMP,USER_ID,USER_DATE,Expired_date,Day_Expired) " +
                "VALUES (@USERNAME,@PASSWORD,@USERNAME,@EMPNAME,@CODPOS,@CODPOSNAME,@CODCOMP,@CODCOMPNAME,@STAEMP,@USERNAME,GETDATE(),(GETDATE() + 180),180);";

            var res = Query<Model.Table.Mssql.User>(cmd, param).ToList();
            return res;
        }
    }
}
