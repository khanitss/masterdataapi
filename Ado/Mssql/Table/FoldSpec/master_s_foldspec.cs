﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.FoldSpec
{
    public class master_s_foldspec : Base
    {
        private static master_s_foldspec instant;

        public static master_s_foldspec GetInstant()
        {
            if (instant == null) instant = new master_s_foldspec();
            return instant;
        }

        public List<Model.Table.Mssql.FoldSpec.master_s_foldspec> Search(Model.Request.Standardpack.FoldSpec.Master_s_FoldSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", d.cuscod);
            param.Add("@foldspec_id", $"%{d.foldspec_id.GetValue()}%");

            string cmd = "SELECT * FROM master_s_foldspec " +
                "WHERE (cus_cod = @cuscod) AND" +
                "(@foldspec_id IS NULL OR foldspec_id like @foldspec_id)";

            return Query<Model.Table.Mssql.FoldSpec.master_s_foldspec>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.FoldSpec.master_s_foldspec> SearchbyCus(Model.Request.Standardpack.FoldSpec.Master_s_FoldSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", $"%{d.cuscod.GetValue()}%");
            param.Add("@foldspec_id", $"%{d.foldspec_id.GetValue()}%");
            param.Add("@fold_cd", $"%{d.fold_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT DISTINCT(cus_cod),tsale,cus_name,status FROM master_s_foldspec " +
                "WHERE (@cuscod IS NULL OR cus_cod like @cuscod)" +
                "AND (@fold_cd IS NULL OR fold_cd like @fold_cd)" +
                "AND (fold_name like @txtSearch OR foldspec_information like @txtSearch)";

            return Query<Model.Table.Mssql.FoldSpec.master_s_foldspec>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.FoldSpec.master_s_foldspec> SearchByID(Model.Request.Standardpack.FoldSpec.Master_s_FoldSpecSearchReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_id", d.foldspec_id);

            string cmd = "SELECT * FROM master_s_foldspec WHERE foldspec_id = @foldspec_id";

            return Query<Model.Table.Mssql.FoldSpec.master_s_foldspec>(cmd, param).ToList();
        }

        public Model.Table.Mssql.FoldSpec.master_s_foldspec Get(Model.Table.Mssql.FoldSpec.master_s_foldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_id", d.foldspec_id);

            string cmd = "SELECT * FROM master_s_foldspec WHERE foldspec_id = @foldspec_id ";

            var res = Query<Model.Table.Mssql.FoldSpec.master_s_foldspec>(cmd, param).FirstOrDefault();

            return res;
        }
        public int Save(Model.Table.Mssql.FoldSpec.master_s_foldspec d)
        {
            var item = Get(new Model.Table.Mssql.FoldSpec.master_s_foldspec
            {
                foldspec_id = d.foldspec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        private int Insert(Model.Table.Mssql.FoldSpec.master_s_foldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_information", d.foldspec_information);
            param.Add("@tsale", d.tsale);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@producttype_code", d.producttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@pqgradepd", d.pqgradepd);
            param.Add("@pqgrade_desc", d.pqgrade_desc);
            param.Add("@stdwei_min", d.stdwei_min);
            param.Add("@stdwei_max", d.stdwei_max);
            param.Add("@twinesize_min", d.twinesize_min);
            param.Add("@twinesize_max", d.twinesize_max);
            param.Add("@depsize_min", d.depsize_min);
            param.Add("@depsize_max", d.depsize_max);
            param.Add("@depamt_min", d.depamt_min);
            param.Add("@depamt_max", d.depamt_max);
            param.Add("@len_min", d.len_min);
            param.Add("@len_max", d.len_max);
            param.Add("@knottype_code", d.knottype_code);
            param.Add("@linetype_code", d.linetype_code);
            param.Add("@stretchingtype_code", d.stretchingtype_code);
            param.Add("@old_color_code", d.old_color_code);
            param.Add("@new_color_code", d.new_color_code);
            param.Add("@color_code", d.color_code);
            param.Add("@fold_id", d.fold_id);
            param.Add("@fold_cd", d.fold_cd);
            param.Add("@fold_name", d.fold_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@min_product_type", d.min_product_type);
            param.Add("@min_productcate_grp", d.min_productcate_grp);
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word);
            param.Add("@min_product_grp", d.min_product_grp);
            param.Add("@max_product_type", d.max_product_type);
            param.Add("@max_productcate_grp", d.max_productcate_grp);
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word);
            param.Add("@max_product_grp", d.max_product_grp);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_foldspec" +
                "(foldspec_information,tsale,cus_cod,cus_name,producttype_code,pqgradepd,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min " +
                ",depsize_max,depamt_min,depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,fold_id " +
                ",fold_cd,fold_name,effdate_st,effdate_en,status,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type " +
                ",max_productcate_grp,max_twine_no,max_line_no,max_word,user_id,user_name,user_date,producttype_desc,pqgrade_desc )" +
                "VALUES " +
                "(@foldspec_information,@tsale,@cus_cod,@cus_name,@producttype_code,@pqgradepd,@stdwei_min,@stdwei_max,@twinesize_min,@twinesize_max,@depsize_min " +
                ",@depsize_max,@depamt_min,@depamt_max,@len_min,@len_max,@knottype_code,@linetype_code,@stretchingtype_code,@old_color_code,@new_color_code,@color_code,@fold_id " +
                ",@fold_cd,@fold_name,@effdate_st,@effdate_en,@status,@min_product_type,@min_productcate_grp,@min_twine_no,@min_line_no,@min_word,@min_product_grp,@max_product_type " +
                ",@max_productcate_grp,@max_twine_no,@max_line_no,@max_word,@user_id,@user_name, GetDate() ,@producttype_desc,@pqgrade_desc)" +
                "SELECT SCOPE_IDENTITY();";

            return ExecuteScalar<int>(cmd, param);
        }

        private int Update(Model.Table.Mssql.FoldSpec.master_s_foldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_id", d.foldspec_id);
            param.Add("@foldspec_information", d.foldspec_information);
            param.Add("@tsale", d.tsale.GetValue());
            param.Add("@cus_cod", d.cus_cod.GetValue());
            param.Add("@cus_name", d.cus_name.GetValue());
            param.Add("@producttype_code", d.producttype_code.GetValue());
            param.Add("@producttype_desc", d.producttype_desc.GetValue());
            param.Add("@pqgradepd", d.pqgradepd.GetValue());
            param.Add("@pqgrade_desc", d.pqgrade_desc.GetValue());
            param.Add("@stdwei_min", d.stdwei_min);
            param.Add("@stdwei_max", d.stdwei_max);
            param.Add("@twinesize_min", d.twinesize_min.GetValue());
            param.Add("@twinesize_max", d.twinesize_max.GetValue());
            param.Add("@depsize_min", d.depsize_min);
            param.Add("@depsize_max", d.depsize_max);
            param.Add("@depamt_min", d.depamt_min);
            param.Add("@depamt_max", d.depamt_max);
            param.Add("@len_min", d.len_min);
            param.Add("@len_max", d.len_max);
            param.Add("@knottype_code", d.knottype_code.GetValue());
            param.Add("@linetype_code", d.linetype_code.GetValue());
            param.Add("@stretchingtype_code", d.stretchingtype_code.GetValue());
            param.Add("@old_color_code", d.old_color_code.GetValue());
            param.Add("@new_color_code", d.new_color_code.GetValue());
            param.Add("@color_code", d.color_code.GetValue());
            param.Add("@fold_id", d.fold_id);
            param.Add("@fold_cd", d.fold_cd.GetValue());
            param.Add("@fold_name", d.fold_name.GetValue());
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status.GetValue());
            param.Add("@min_product_type", d.min_product_type.GetValue());
            param.Add("@min_productcate_grp", d.min_productcate_grp.GetValue());
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word.GetValue());
            param.Add("@min_product_grp", d.min_product_grp.GetValue());
            param.Add("@max_product_type", d.max_product_type.GetValue());
            param.Add("@max_productcate_grp", d.max_productcate_grp.GetValue());
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word.GetValue());
            param.Add("@max_product_grp", d.max_product_grp.GetValue());
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_foldspec " +
                "SET foldspec_information = @foldspec_information,tsale = @tsale,cus_cod = @cus_cod,cus_name = @cus_name,producttype_code = @producttype_code,pqgradepd = @pqgradepd,stdwei_min = @stdwei_min," +
                "stdwei_max = @stdwei_max,twinesize_min = @twinesize_min,twinesize_max = @twinesize_max,depsize_min = @depsize_min " +
                ",depsize_max = @depsize_max,depamt_min = @depamt_min,depamt_max = @depamt_max,len_min = @len_min,len_max = @len_max,knottype_code = @knottype_code,linetype_code = @linetype_code, " +
                "stretchingtype_code = @stretchingtype_code,old_color_code = @old_color_code,new_color_code = @new_color_code,color_code = @color_code,fold_id = @fold_id " +
                ",fold_cd = @fold_cd,fold_name = @fold_name,effdate_st = @effdate_st,effdate_en = @effdate_en,status = @status,min_product_type = @min_product_type,min_productcate_grp = @min_productcate_grp, " +
                "min_twine_no = @min_twine_no,min_line_no = @min_line_no,min_word= @min_word,min_product_grp = @min_product_grp,max_product_type = @max_product_type " +
                ",max_productcate_grp = @max_productcate_grp,max_twine_no= @max_twine_no,max_line_no = @max_line_no,max_word = @max_word,user_id = @user_id,user_name = @user_name, user_date = GetDate() ," +
                "producttype_desc = @producttype_desc,pqgrade_desc = @pqgrade_desc " +
                "WHERE foldspec_id = @foldspec_id " +
                "SELECT @foldspec_id AS foldspec_id";

            var foldspec_id = ExecuteScalar<int>(cmd, param);

            return foldspec_id;
        }

        public int Move(Model.Table.Mssql.FoldSpec.master_s_foldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_id", d.foldspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_foldspec" +
                "(foldspec_id , foldspec_information,tsale,cus_cod,cus_name,producttype_code,pqgradepd,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min " +
                ",depsize_max,depamt_min,depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,fold_id " +
                ",fold_cd,fold_name,effdate_st,effdate_en,status,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type " +
                ",max_productcate_grp,max_twine_no,max_line_no,max_word,user_id,user_name,user_date,producttype_desc,pqgrade_desc ) " +
                "SELECT foldspec_id , foldspec_information,tsale,cus_cod,cus_name,producttype_code,pqgradepd,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min " +
                ",depsize_max,depamt_min,depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,fold_id " +
                ",fold_cd,fold_name,effdate_st,effdate_en,status,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type " +
                ",max_productcate_grp,max_twine_no,max_line_no,max_word,user_id,user_name,user_date,producttype_desc,pqgrade_desc " +
                "FROM master_s_foldspec WHERE foldspec_id = @foldspec_id " +
                "UPDATE master_u_foldspec SET status = 'C' ,cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE foldspec_id = @foldspec_id " +
                "UPDATE master_s_foldspec SET status = 'C' WHERE foldspec_id = @foldspec_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
