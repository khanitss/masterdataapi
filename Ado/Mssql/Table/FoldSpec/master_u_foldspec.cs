﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.FoldSpec
{
    public class master_u_foldspec : Base
    {
        private static master_u_foldspec instant;

        public static master_u_foldspec GetInstant()
        {
            if (instant == null) instant = new master_u_foldspec();
            return instant;
        }
        public List<Model.Table.Mssql.FoldSpec.master_u_foldspec> SearchCancelByID(Model.Request.Standardpack.FoldSpec.Master_s_FoldSpecSearchReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@foldspec_id", d.foldspec_id);

            string cmd = "SELECT * FROM master_u_foldspec WHERE foldspec_id = @foldspec_id";

            return Query<Model.Table.Mssql.FoldSpec.master_u_foldspec>(cmd, param).ToList();
        }
    }
}
