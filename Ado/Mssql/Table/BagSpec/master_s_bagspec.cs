﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.BagSpec
{
    public class master_s_bagspec : Base
    {
        private static master_s_bagspec instant;

        public static master_s_bagspec GetInstant()
        {
            if (instant == null) instant = new master_s_bagspec();
            return instant;
        }

        public List<Model.Table.Mssql.BagSpec.master_s_bagspec> Search(Model.Request.Standardpack.BagSpec.Master_s_BagSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@codeSearch", $"%{d.codeSearch.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT DISTINCT(cus_cod),tsale,cus_name ,status FROM master_s_bagspec WHERE " +
                "(@codeSearch IS NULL OR cus_cod like @codeSearch OR bag_cd like @codeSearch OR stretchingtype_code like @codeSearch) AND " +
                "(@txtSearch IS NULL OR cus_name like @txtSearch OR stretchingtype_desc like @txtSearch OR bag_name like @txtSearch ) " +
                "UNION SELECT DISTINCT(cus_cod),tsale, cus_name,status FROM master_u_bagspec WHERE " +
                "(@codeSearch IS NULL OR cus_cod like @codeSearch OR bag_cd like @codeSearch OR stretchingtype_code like @codeSearch ) AND " +
                "(@txtSearch IS NULL OR cus_name like @txtSearch OR stretchingtype_desc like @txtSearch OR bag_name like @txtSearch ) " +
                "order by cus_cod ";

            return Query<Model.Table.Mssql.BagSpec.master_s_bagspec>(cmd, param).ToList();


        }
        public List<Model.Table.Mssql.BagSpec.master_s_bagspec> SearchByValue(Model.Request.Standardpack.BagSpec.Master_s_BagSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", $"%{d.cuscod.GetValue()}%");
            param.Add("@bagspec_id", $"%{d.bagspec_id.GetValue()}%");

            string cmd = "SELECT bagspec_id,tsale,cus_cod,cus_name,produccttype_code,producttype_desc,stretchingtype_code,stretchingtype_desc,label_id,label_cd,label_name, " +
                "bag_id,bag_cd,bag_name,status,effdate_st,effdate_en,user_id,user_name,user_date,'' as cancel_id,'' as cancel_name,'' as cancel_date FROM master_s_bagspec WHERE " +
                "(@cuscod IS NULL OR cus_cod like @cuscod ) AND " +
                "(@bagspec_id IS NULL OR bagspec_id like @bagspec_id ) " +
                "UNION SELECT bagspec_id,tsale,cus_cod,cus_name,produccttype_code,producttype_desc,stretchingtype_code,stretchingtype_desc,label_id,label_cd,label_name, " +
                "bag_id,bag_cd,bag_name,status,effdate_st,effdate_en,user_id,user_name,user_date,cancel_id,cancel_name,cancel_date FROM master_u_bagspec WHERE " +
                "(@cuscod IS NULL OR cus_cod like @cuscod ) AND " +
                "(@bagspec_id IS NULL OR bagspec_id like @bagspec_id ) " +
                "order by bagspec_id ";

            return Query<Model.Table.Mssql.BagSpec.master_s_bagspec>(cmd, param).ToList();
        }

        public Model.Table.Mssql.BagSpec.master_s_bagspec Get(Model.Table.Mssql.BagSpec.master_s_bagspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bagspec_id", d.bagspec_id);

            string cmd = "SELECT * FROM master_s_bagspec WHERE bagspec_id = @bagspec_id ";

            var res = Query<Model.Table.Mssql.BagSpec.master_s_bagspec>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.BagSpec.master_s_bagspec d)
        {
            var item = Get(new Model.Table.Mssql.BagSpec.master_s_bagspec
            {
                bagspec_id = d.bagspec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        private int Insert(Model.Table.Mssql.BagSpec.master_s_bagspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@tsale", d.tsale);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@produccttype_code", d.produccttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@stretchingtype_code", d.stretchingtype_code);
            param.Add("@stretchingtype_desc", d.stretchingtype_desc);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);
            param.Add("@bag_id", d.bag_id);
            param.Add("@bag_cd", d.bag_cd);
            param.Add("@bag_name", d.bag_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_bagspec " +
                "(tsale,cus_cod,cus_name,produccttype_code,producttype_desc,stretchingtype_code,stretchingtype_desc,label_id,label_cd,label_name " +
                ",bag_id,bag_cd,bag_name,status,effdate_st,effdate_en,user_id,user_name,user_date ) " +
                "VALUES " +
                "(@tsale,@cus_cod,@cus_name,@produccttype_code,@producttype_desc,@stretchingtype_code,@stretchingtype_desc,@label_id,@label_cd,@label_name " +
                ",@bag_id,@bag_cd,@bag_name,@status,@effdate_st,@effdate_en,@user_id,@user_name,GetDate() )" +
                "SELECT SCOPE_IDENTITY();";

            return ExecuteScalar<int>(cmd, param); 
        }
        private int Update(Model.Table.Mssql.BagSpec.master_s_bagspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bagspec_id", d.bagspec_id);
            param.Add("@tsale", d.tsale);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@produccttype_code", d.produccttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@stretchingtype_code", d.stretchingtype_code);
            param.Add("@stretchingtype_desc", d.stretchingtype_desc);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);
            param.Add("@bag_id", d.bag_id);
            param.Add("@bag_cd", d.bag_cd);
            param.Add("@bag_name", d.bag_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_bagspec " +
                "SET tsale = @tsale,cus_cod = @cus_cod,cus_name = @cus_name,produccttype_code = @produccttype_code,producttype_desc = @producttype_desc" +
                ",stretchingtype_code = @stretchingtype_code,stretchingtype_desc = @stretchingtype_desc,label_id = @label_id,label_cd = @label_cd,label_name = @label_name," +
                "bag_id = @bag_id,bag_cd = @bag_cd,bag_name = @bag_name,status =@status,effdate_st = @effdate_st ,effdate_en = @effdate_en ,user_id = @user_id ,user_name = @user_name,user_date = GetDate() " +
                "WHERE  bagspec_id = @bagspec_id " +
                "SELECT @bagspec_id AS bagspec_id ";
             
            var bagspec_id = ExecuteScalar<int>(cmd, param);

            return bagspec_id;
        }
        
        public int Move(Model.Table.Mssql.BagSpec.master_s_bagspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bagspec_id", d.bagspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_bagspec " +
                "(bagspec_id,tsale,cus_cod,cus_name,produccttype_code,producttype_desc,stretchingtype_code,stretchingtype_desc,label_id,label_cd,label_name " +
                ",bag_id,bag_cd,bag_name,status,effdate_st,effdate_en,user_id,user_name,user_date ) " +
                "SELECT bagspec_id,tsale,cus_cod,cus_name,produccttype_code,producttype_desc,stretchingtype_code,stretchingtype_desc,label_id,label_cd,label_name " +
                ",bag_id,bag_cd,bag_name,status,effdate_st,effdate_en,user_id,user_name,user_date FROM master_s_bagspec " +
                "WHERE bagspec_id = @bagspec_id " +
                "UPDATE master_u_bagspec SET status = 'C' , cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE bagspec_id = @bagspec_id " +
                "DELETE master_s_bagspec WHERE bagspec_id = @bagspec_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
