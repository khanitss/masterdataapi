﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.StickerSpec
{
    public class master_u_stickerspec : Base
    {
        private static master_u_stickerspec instant;
        public static master_u_stickerspec GetInstant()
        {
            if (instant == null) instant = new master_u_stickerspec();
            return instant;
        }
        public List<Model.Table.Mssql.StickerSpec.master_u_stickerspec> SearchCancel(Model.Request.Standardpack.StickerSpec.Master_s_StickerSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stickerspec_id", $"%{d.stickerspec_id.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@stretchingtype_code", $"%{d.stretchingtype_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_stickerspec WHERE " +
                "(@stickerspec_id IS NULL OR stickerspec_id LIKE @stickerspec_id) AND " +
                "(@label_cd IS NULL OR label_cd LIKE @label_cd) AND " +
                "(@stretchingtype_code IS NULL OR stretchingtype_code LIKE @stretchingtype_code OR stretchingtype_name LIKE @stretchingtype_code)  AND " +
                "(@txtSearch IS NULL OR sticker_name LIKE @txtSearch)";

            return Query<Model.Table.Mssql.StickerSpec.master_u_stickerspec>(cmd, param).ToList();
        }
    }
}
