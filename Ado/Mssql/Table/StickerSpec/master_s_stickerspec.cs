﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.StickerSpec
{
    public class master_s_stickerspec : Base
    {
        private static master_s_stickerspec instant;
        public static master_s_stickerspec GetInstant()
        {
            if (instant == null) instant = new master_s_stickerspec();
            return instant;
        }
        public Model.Table.Mssql.StickerSpec.master_s_stickerspec Get (Model.Table.Mssql.StickerSpec.master_s_stickerspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stickerspec_id", d.stickerspec_id);

            string cmd = "SELECT * FROM master_s_stickerspec WHERE stickerspec_id = @stickerspec_id ";

            var res = Query<Model.Table.Mssql.StickerSpec.master_s_stickerspec>(cmd, param).FirstOrDefault();
            return res;
        }

        public int Save(Model.Table.Mssql.StickerSpec.master_s_stickerspec d)
        {
            var item = Get(new Model.Table.Mssql.StickerSpec.master_s_stickerspec
            {
                stickerspec_id = d.stickerspec_id 
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        public List<Model.Table.Mssql.StickerSpec.master_s_stickerspec> SearchView(Model.Request.Standardpack.StickerSpec.Master_s_StickerSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stickerspec_id", $"%{d.stickerspec_id.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@stretchingtype_code", $"%{d.stretchingtype_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_stickerspec WHERE " +
                "(@stickerspec_id IS NULL OR stickerspec_id LIKE @stickerspec_id) AND " +
                "(@label_cd IS NULL OR label_cd LIKE @label_cd) AND " +
                "(@stretchingtype_code IS NULL OR stretchingtype_code LIKE @stretchingtype_code OR stretchingtype_name LIKE @stretchingtype_code) AND " +
                "(@txtSearch IS NULL OR sticker_name LIKE @txtSearch)";

            return Query<Model.Table.Mssql.StickerSpec.master_s_stickerspec>(cmd, param).ToList();
        }

        private int Insert(Model.Table.Mssql.StickerSpec.master_s_stickerspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@tsale", d.tsale);
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@stretchingtype_code", d.stretchingtype_code);
            param.Add("@stretchingtype_name", d.stretchingtype_name);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);
            param.Add("@sticker_id", d.sticker_id);
            param.Add("@sticker_cd", d.sticker_cd);
            param.Add("@sticker_name", d.sticker_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_stickerspec " +
                "(tsale,cus_id,cus_cod,cus_name,stretchingtype_code,stretchingtype_name,label_id,label_cd,label_name," +
                "sticker_id,sticker_cd,sticker_name,status,effdate_st,effdate_en,user_id,user_name,user_date)" +
                "VALUES " +
                "(@tsale,@cus_id,@cus_cod,@cus_name,@stretchingtype_code,@stretchingtype_name,@label_id,@label_cd," +
                "@label_name,@sticker_id,@sticker_cd,@sticker_name,@status,@effdate_st,@effdate_en,@user_id,@user_name, GetDate()) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }

        private int Update(Model.Table.Mssql.StickerSpec.master_s_stickerspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stickerspec_id", d.stickerspec_id);
            param.Add("@tsale", d.tsale);
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@stretchingtype_code", d.stretchingtype_code);
            param.Add("@stretchingtype_name", d.stretchingtype_name);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);
            param.Add("@sticker_id", d.sticker_id);
            param.Add("@sticker_cd", d.sticker_cd);
            param.Add("@sticker_name", d.sticker_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_stickerspec " +
                "SET tsale = @tsale ,cus_id = @cus_id,cus_cod = @cus_cod ,cus_name = @cus_name ,stretchingtype_code = @stretchingtype_code ,stretchingtype_name = @stretchingtype_name ,label_id = @label_id ,label_cd = @label_cd ,label_name = @label_name, " +
                "sticker_id = @sticker_id,sticker_cd = @sticker_cd,sticker_name =@sticker_name,status = @status,effdate_st =@effdate_st,effdate_en =@effdate_en,user_id =@user_id,user_name =@user_name ,user_date=  GetDate() " +
                "WHERE stickerspec_id = @stickerspec_id SELECT @stickerspec_id AS stickerspec_id ";

            var stickerspec_id = ExecuteScalar<int>(cmd, param);

            return stickerspec_id;
        }

        public int Move(Model.Table.Mssql.StickerSpec.master_s_stickerspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@stickerspec_id", d.stickerspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_stickerspec " +
                "( stickerspec_id,tsale,cus_id,cus_cod,cus_name,stretchingtype_code,stretchingtype_name,label_id,label_cd,label_name, " +
                "sticker_id,sticker_cd,sticker_name,status,effdate_st,effdate_en,user_id,user_name,user_date) " +
                "SELECT stickerspec_id,tsale,cus_id,cus_cod,cus_name,stretchingtype_code,stretchingtype_name,label_id,label_cd,label_name, " +
                "sticker_id,sticker_cd,sticker_name,status,effdate_st,effdate_en,user_id,user_name,user_date " +
                "FROM master_s_stickerspec WHERE stickerspec_id = @stickerspec_id " +
                "UPDATE master_u_stickerspec SET status = 'C' , cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE stickerspec_id = @stickerspec_id " +
            "UPDATE master_s_stickerspec SET status = 'C' WHERE stickerspec_id = @stickerspec_id ";


            return ExecuteNonQuery(cmd, param);
        }
    }
}
