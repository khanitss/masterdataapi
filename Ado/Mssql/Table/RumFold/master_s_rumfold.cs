﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.RumFold
{
    public class master_s_rumfold : Base
    {
        private static master_s_rumfold instant;

        public static master_s_rumfold GetInstant()
        {
            if (instant == null) instant = new master_s_rumfold();
            return instant;
        }

        public List<Model.Table.Mssql.RumFold.master_s_rumfold> Search(Model.Request.Standardpack.RumFold.Master_s_RumFoldSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", $"%{d.fold_id.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_rumfold WHERE " +
                "(@fold_id IS NULL OR fold_id like @fold_id ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch OR fold_information like @txtSearch ) " +
                "AND status = 'A' order by fold_code ";

            return Query<Model.Table.Mssql.RumFold.master_s_rumfold>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.RumFold.master_s_rumfold> SearchView(Model.Request.Standardpack.RumFold.Master_s_RumFoldSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", $"%{d.fold_id.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_rumfold WHERE " +
                "(@fold_id IS NULL OR fold_id like @fold_id ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch OR fold_information like @txtSearch ) " +
                "order by fold_code ";

            return Query<Model.Table.Mssql.RumFold.master_s_rumfold>(cmd, param).ToList();
        }
        
        public Model.Table.Mssql.RumFold.master_s_rumfold Get(Model.Table.Mssql.RumFold.master_s_rumfold d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", d.fold_id);

            string cmd = "SELECT * FROM master_s_rumfold WHERE fold_id = @fold_id";

            var res = Query<Model.Table.Mssql.RumFold.master_s_rumfold>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.RumFold.master_s_rumfold d)
        {
            var item = Get(new Model.Table.Mssql.RumFold.master_s_rumfold
            {
                fold_id = d.fold_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        public int Insert(Model.Table.Mssql.RumFold.master_s_rumfold d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_code", d.fold_code);
            param.Add("@fold_name", d.fold_name);
            param.Add("@fold_information", d.fold_information);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_rumfold " +
                "(fold_code,fold_name,fold_information ,path_pic ,status,effdate_st,effdate_en,user_id,user_name,user_date) " +
                "VALUES " +
                "(@fold_code,@fold_name,@fold_information ,@path_pic ,@status,@effdate_st,@effdate_en,@user_id,@user_name, GetDate() ) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        public int Update(Model.Table.Mssql.RumFold.master_s_rumfold d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", d.fold_id);
            param.Add("@fold_code", d.fold_code);
            param.Add("@fold_name", d.fold_name);
            param.Add("@fold_information", d.fold_information);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_rumfold " +
                "SET fold_code = @fold_code ,fold_name = @fold_name,fold_information =@fold_information ,path_pic =@path_pic, status = @status ," +
                "effdate_st = @effdate_st ,effdate_en = @effdate_en ,user_id = @user_id ,user_name = @user_name ,user_date = GetDate() " +
                "WHERE fold_id = @fold_id " +
                "SELECT @fold_id As fold_id ";

            var fold_id = ExecuteScalar<int>(cmd, param);

            return fold_id;
        }

        public int Move(Model.Table.Mssql.RumFold.master_s_rumfold d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", d.fold_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_rumfold " +
                "(fold_id,fold_code,fold_name,fold_information ,path_pic ,status,effdate_st,effdate_en,user_id,user_name,user_date ) " +
                "SELECT fold_id,fold_code,fold_name,fold_information ,path_pic ,status,effdate_st,effdate_en,user_id,user_name,user_date " +
                "FROM master_s_rumfold WHERE fold_id = @fold_id " +
                "UPDATE master_u_rumfold SET status = 'C', cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE fold_id = @fold_id " +
                "UPDATE master_s_rumfold SET status = 'C' WHERE fold_id = @fold_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
