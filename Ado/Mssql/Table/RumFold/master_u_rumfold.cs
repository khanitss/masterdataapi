﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.RumFold
{
    public class master_u_rumfold : Base
    {
        private static master_u_rumfold instant;

        public static master_u_rumfold GetInstant()
        {
            if (instant == null) instant = new master_u_rumfold();
            return instant;
        }
        public List<Model.Table.Mssql.RumFold.master_u_rumfold> SearchCancel(Model.Request.Standardpack.RumFold.Master_s_RumFoldSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", $"%{d.fold_id.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_rumfold WHERE " +
                "(@fold_id IS NULL OR fold_id like @fold_id ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch OR fold_information like @txtSearch ) " +
                "order by fold_code ";

            return Query<Model.Table.Mssql.RumFold.master_u_rumfold>(cmd, param).ToList();
        }
    }
}
