﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table
{
    public class vTemploy1 : Base
    {
        private static vTemploy1 instant;

        public static vTemploy1 GetInstant()
        {
            if (instant == null) instant = new vTemploy1();
            return instant;
        }

        public List<Model.Table.Mssql.vTemploy1> Search(Model.Request.Job.vTemploy1SearchReq d, SqlTransaction transac = null)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@CODEMPID", d.CODEMPID);
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * From vTemploy1 " +
                $"WHERE CODEMPID = @CODEMPID " +
                $"AND (CODEMPID Like @txtSearch)";

            var res = Query<Model.Table.Mssql.vTemploy1>(cmd, param).ToList();
            return res;
        }
    }
}
