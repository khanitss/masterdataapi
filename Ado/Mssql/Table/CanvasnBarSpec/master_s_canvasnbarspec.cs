﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.CanvasnBarSpec
{
    public class master_s_canvasnbarspec : Base
    {
        private static master_s_canvasnbarspec instant;

        public static master_s_canvasnbarspec GetInstant()
        {
            if (instant == null) instant = new master_s_canvasnbarspec();
            return instant;
        }

        public Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec Get(Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbarspec_id", d.canvasnbarspec_id);

            string cmd = "SELECT * FROM master_s_canvasnbarspec WHERE canvasnbarspec_id =@canvasnbarspec_id";

            var res = Query<Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec>(cmd, param).FirstOrDefault();

            return res;
        }
        public int Save(Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec d)
        {
            var item = Get(new Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec
            {
                canvasnbarspec_id = d.canvasnbarspec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        public List<Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec> SearchView(Model.Request.Standardpack.CanvasnBarSpec.Master_s_CanvasnBarSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cus_cod", $"%{d.cus_cod.GetValue()}%");
            param.Add("@canvasnbarspec_id", $"%{d.canvasnbarspec_id.GetValue()}%");
            param.Add("@canvasnbar_cd", $"%{d.canvasnbar_cd.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_canvasnbarspec WHERE " +
                "(@cus_cod IS NULL OR cus_cod like @cus_cod ) AND " +
                "(@canvasnbarspec_id IS NULL OR canvasnbarspec_id like @canvasnbarspec_id ) AND " +
                "(@canvasnbar_cd IS NULL OR canvasnbar_cd like @canvasnbar_cd ) AND " +
                "(@txtsearch IS NULL OR cus_name like @txtSearch OR canvasnbar_name like @txtSearch ) order by canvasnbarspec_id ";

            return Query<Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec>(cmd, param).ToList();
        }
        private int Insert(Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            param.Add("@canvasnbar_cd", d.canvasnbar_cd);
            param.Add("@canvasnbar_name", d.canvasnbar_name);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_name", d.cus_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@tsale", d.tsale);

            param.Add("@producttype_code", d.producttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@productcate_grp", d.productcate_grp);
            param.Add("@twinesize_min", d.twinesize_min);
            param.Add("@twinesize_max", d.twinesize_max);
            param.Add("@min_product_type", d.min_product_type);
            param.Add("@min_productcate_grp", d.min_productcate_grp);
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word);
            param.Add("@min_product_grp", d.min_product_grp);
            param.Add("@max_product_type", d.max_product_type);
            param.Add("@max_productcate_grp", d.max_productcate_grp);
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word);
            param.Add("@max_product_grp", d.max_product_grp);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);

            string cmd = "INSERT INTO master_s_canvasnbarspec " +
                "(canvasnbar_id , canvasnbar_cd , canvasnbar_name , cus_id , cus_cod , cus_name , effdate_st , effdate_en , status , user_id , user_name , user_date , tsale ," +
                "producttype_code,producttype_desc,productcate_grp,twinesize_min,twinesize_max,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word, min_product_grp," +
                "max_product_type,max_productcate_grp,max_twine_no,max_line_no, max_word,max_product_grp,label_id,label_cd,label_name) " +
                "VALUES " +
                "(@canvasnbar_id,@canvasnbar_cd,@canvasnbar_name,@cus_id,@cus_cod,@cus_name,@effdate_st,@effdate_en,@status,@user_id,@user_name,GetDate() ,@tsale ," +
                "@producttype_code,@producttype_desc,@productcate_grp,@twinesize_min,@twinesize_max,@min_product_type,@min_productcate_grp,@min_twine_no,@min_line_no,@min_word, @min_product_grp," +
                "@max_product_type,@max_productcate_grp,@max_twine_no,@max_line_no,@max_word,@max_product_grp,@label_id,@label_cd,@label_name)" +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbarspec_id", d.canvasnbarspec_id);
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            param.Add("@canvasnbar_cd", d.canvasnbar_cd);
            param.Add("@canvasnbar_name", d.canvasnbar_name);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_name", d.cus_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@tsale", d.tsale);

            param.Add("@producttype_code", d.producttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@productcate_grp", d.productcate_grp);
            param.Add("@twinesize_min", d.twinesize_min);
            param.Add("@twinesize_max", d.twinesize_max);
            param.Add("@min_product_type", d.min_product_type);
            param.Add("@min_productcate_grp", d.min_productcate_grp);
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word);
            param.Add("@min_product_grp", d.min_product_grp);
            param.Add("@max_product_type", d.max_product_type);
            param.Add("@max_productcate_grp", d.max_productcate_grp);
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word);
            param.Add("@max_product_grp", d.max_product_grp);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name);

            string cmd = "UPDATE master_s_canvasnbarspec " +
                "SET canvasnbar_id = @canvasnbar_id, canvasnbar_cd = @canvasnbar_cd,canvasnbar_name = @canvasnbar_name,cus_id = @cus_id,cus_cod = @cus_cod,cus_name = @cus_name, tsale = @tsale , " +
                "effdate_st = @effdate_st,effdate_en = @effdate_en,status = @status,user_id = @user_id, user_name = @user_name, user_date = GetDate() , " +
                "producttype_code = @producttype_code,producttype_desc = @producttype_desc,productcate_grp = @productcate_grp,twinesize_min =@twinesize_min,twinesize_max =@twinesize_max, " +
                "min_product_type = @min_product_type ,min_productcate_grp = @min_productcate_grp ,min_twine_no =@min_twine_no ,min_line_no =@min_line_no,min_word =@min_word, min_product_grp = @min_product_grp, " +
                "max_product_type = @max_product_type,max_productcate_grp = @max_productcate_grp ,max_twine_no = @max_twine_no,max_line_no = @max_line_no, max_word = @max_word,max_product_grp = @max_product_grp, " +
                "label_id = @label_id,label_cd = @label_cd,label_name = @label_name " +
                "WHERE canvasnbarspec_id = @canvasnbarspec_id SELECT @canvasnbarspec_id AS canvasnbarspec_id";

            var canvasnbarspec_id = ExecuteScalar<int>(cmd, param);
            
            return canvasnbarspec_id;
        }

        public int Move(Model.Table.Mssql.CanvasnBarSpec.master_s_canvasnbarspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbarspec_id", d.canvasnbarspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_canvasnbarspec " +
                "(canvasnbarspec_id, canvasnbar_id , canvasnbar_cd , canvasnbar_name , cus_id , cus_cod , cus_name , effdate_st , effdate_en, user_id , user_name , user_date ,tsale," +
                "producttype_code,producttype_desc,productcate_grp,twinesize_min,twinesize_max,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word, min_product_grp," +
                "max_product_type,max_productcate_grp,max_twine_no,max_line_no, max_word,max_product_grp,label_id,label_cd,label_name )" +
                "SELECT " +
                "canvasnbarspec_id, canvasnbar_id , canvasnbar_cd , canvasnbar_name , cus_id , cus_cod , cus_name , effdate_st , effdate_en , user_id , user_name , user_date , tsale , " +
                "producttype_code,producttype_desc,productcate_grp,twinesize_min,twinesize_max,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word, min_product_grp, " +
                "max_product_type,max_productcate_grp,max_twine_no,max_line_no, max_word,max_product_grp,label_id,label_cd,label_name " +
                "FROM master_s_canvasnbarspec WHERE canvasnbarspec_id = @canvasnbarspec_id " +
                "UPDATE master_u_canvasnbarspec SET cancel_id = @user_id , cancel_name = @user_name , cancel_date = Getdate() , status = 'C' WHERE canvasnbarspec_id = @canvasnbarspec_id " +
                "UPDATE master_s_canvasnbarspec SET status = 'C' WHERE canvasnbarspec_id = @canvasnbarspec_id";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
