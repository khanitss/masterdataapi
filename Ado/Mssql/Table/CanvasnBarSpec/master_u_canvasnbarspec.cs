﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.CanvasnBarSpec
{
    public class master_u_canvasnbarspec : Base
    {
        private static master_u_canvasnbarspec instant;

        public static master_u_canvasnbarspec GetInstat()
        {
            if (instant == null) instant = new master_u_canvasnbarspec();
            return instant;
        }
        public List<Model.Table.Mssql.CanvasnBarSpec.master_u_canvasnbarspec> SearchCancel(Model.Request.Standardpack.CanvasnBarSpec.Master_s_CanvasnBarSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cus_cod", $"%{d.cus_cod.GetValue()}%");
            param.Add("@canvasnbarspec_id", $"%{d.canvasnbarspec_id.GetValue()}%");
            param.Add("@canvasnbar_cd", $"%{d.canvasnbar_cd.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_canvasnbarspec WHERE " +
                "(@cus_cod IS NULL OR cus_cod like @cus_cod ) AND " +
                "(@canvasnbarspec_id IS NULL OR canvasnbarspec_id like @canvasnbarspec_id ) AND " +
                "(@canvasnbar_cd IS NULL OR canvasnbar_cd like @canvasnbar_cd ) AND " +
                "(@txtsearch IS NULL OR cus_name like @txtSearch OR canvasnbar_name like @txtSearch ) order by canvasnbarspec_id ";

            return Query<Model.Table.Mssql.CanvasnBarSpec.master_u_canvasnbarspec>(cmd, param).ToList();
        }
    } 
}
