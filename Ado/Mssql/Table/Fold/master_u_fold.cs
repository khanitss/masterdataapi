﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Fold
{
    public class master_u_fold : Base
    {
        private static master_u_fold instant;

        public  static master_u_fold GetInstant()
        {
            if (instant == null) instant = new master_u_fold();
            return instant;
        }

        public List<Model.Table.Mssql.Fold.master_u_fold> SearchCancel(Model.Request.Standardpack.Fold.Master_s_FoldSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@fold_id", $"%{d.fold_id.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_fold WHERE " +
                "(@fold_id IS NULL OR fold_id like @fold_id ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch OR fold_information like @txtSearch ) " +
                "order by fold_code ";

            return Query<Model.Table.Mssql.Fold.master_u_fold>(cmd, param).ToList();
        }
    }
}
