﻿using Dapper;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table
{
    public class AccessToken : Base
    {
        private static AccessToken instant;

        public static AccessToken GetInstant()
        {
            if (instant == null) instant = new AccessToken();
            return instant;
        }
        public List<Model.Table.Mssql.mastertAccessToken> ListActive()
        {
            string cmd = "SELECT * FROM master_t_accesstoken " +
                "WHERE Status='A';";
            var res = Query<Model.Table.Mssql.mastertAccessToken>(cmd, null).ToList();
            return res;
        }
        public List<Model.Table.Mssql.mastertAccessToken> Search(string Code, SqlTransaction transac = null)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Code", Code);

            string cmd = "SELECT * FROM master_t_accesstoken " +
                "WHERE Code=@Code;";
            var res = Query<Model.Table.Mssql.mastertAccessToken>(cmd, param).ToList();
            return res;
        }

        public int Update(string Code, SqlTransaction transac = null)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Code", Code);

            string cmd = $"UPDATE master_t_accesstoken SET " +
                "CountUse=CountUse+1" +
                "WHERE Code=@Code;";
            var res = ExecuteNonQuery(transac, cmd, param);
            return res;
        }

        public int Insert(string Code, string IPAddress, string Agent, SqlTransaction transac = null)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Code", Code);
            param.Add("@IPAddress", IPAddress);
            param.Add("@Agent", Agent);

            string cmd = "INSERT INTO master_t_accesstoken (Code, IPAddress, Agent, CountUse, Status, UpdateBy, Timestamp) " +
                "VALUES (@Code, @IPAddress, @Agent, 1, 'A', 0, GETDATE());";
            var res = ExecuteNonQuery(transac, cmd, param);
            return res;
        }
    }
}
