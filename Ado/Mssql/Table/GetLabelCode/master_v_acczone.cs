﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.GetLabelCode
{
    public class master_v_acczone : Base
    {
        private static master_v_acczone instant;

        public static master_v_acczone GetInstant()
        {
            if (instant == null) instant = new master_v_acczone();
            return instant;
        }
        public List<Model.Table.Mssql.GetLabelCode.master_v_acczone> Search(Model.Request.GetLabelCode.Master_v_AcczoneSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@zone_code", d.zone_code);

            string cmd = "Select * FROM master_v_acczone " +
                "WHERE @zone_code IS NULL OR zonecode like @zone_code " +
                " ORDER BY zonecode ";

            return Query<Model.Table.Mssql.GetLabelCode.master_v_acczone>(cmd, param).ToList();
        }
    }
}
