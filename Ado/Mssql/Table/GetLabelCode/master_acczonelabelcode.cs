﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.GetLabelCode
{
    public class master_acczonelabelcode : Base
    {
        private static master_acczonelabelcode instant;

        public static master_acczonelabelcode GetInstant()
        {
            if (instant == null) instant = new master_acczonelabelcode();
            return instant;
        }
        public List<Model.Table.Mssql.GetLabelCode.master_acczonelabelcode> Search(Model.Request.GetLabelCode.Master_s_AcczoneLabelCodeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@zone_code", d.zone_code);

            string cmd = "Select * FROM master_s_acczonelabelcode " +
                "WHERE @zone_code IS NULL OR acczone like @zone_code ORDER BY acczone ";

            return Query<Model.Table.Mssql.GetLabelCode.master_acczonelabelcode>(cmd,param).ToList();
        }
    }
}
