﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.GetLabelCode
{
    public class master_s_labelowner : Base
    {
        private static master_s_labelowner instant;

        public static master_s_labelowner GetInstant()
        {
            if (instant == null) instant = new master_s_labelowner();
            return instant;
        }
        public List<Model.Table.Mssql.GetLabelCode.master_s_labelowner> Search(Model.Request.GetLabelCode.Master_s_LabelOwnerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@labelowner_id", d.labelowner_id.GetValue());
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "Select labelowner_id,labelowner_name FROM master_s_labelowner " +
                " ORDER BY labelowner_id ";

            return Query<Model.Table.Mssql.GetLabelCode.master_s_labelowner>(cmd, param).ToList();
        }
    }
}
