﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.GetLabelCode
{
    public class master_s_labelpapertype : Base
    {
        private static master_s_labelpapertype instant;

        public static master_s_labelpapertype GetInstant()
        {
            if (instant == null) instant = new master_s_labelpapertype();
            return instant;
        }
        public List<Model.Table.Mssql.GetLabelCode.master_s_labelpapertype> Search(Model.Request.GetLabelCode.Master_s_LabelPaperTypeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@labelowner_id", d.labelpapertype_id);
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "Select * FROM master_s_labelpapertype " +
                "WHERE (@labelpapertype_id IS NULL OR labelpapertype_id like @labelpapertype_id) " +
                "AND (@txtSearch IS NULL OR labelpapertype_cd like @txtSearch) " +
                " ORDER BY labelpapertype_cd ";

            return Query<Model.Table.Mssql.GetLabelCode.master_s_labelpapertype>(cmd, param).ToList();
        }
    }
}
