﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.GetLabelCode
{
    public class master_s_labelgrade : Base
    {
        private static master_s_labelgrade instant;

        public static master_s_labelgrade GetInstant()
        {
            if (instant == null) instant = new master_s_labelgrade();
            return instant;
        }
        public List<Model.Table.Mssql.GetLabelCode.master_s_labelgrade> Search(Model.Request.GetLabelCode.Master_s_LabelGradeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@labelgrade_id", d.labelgrade_id.GetValue());

            string cmd = "SELECT labelgrade_id,labelgrade_cd,labelgrade_name FROM master_s_labelgrade";

            return Query<Model.Table.Mssql.GetLabelCode.master_s_labelgrade>(cmd, param).ToList();
        }
    }
}
