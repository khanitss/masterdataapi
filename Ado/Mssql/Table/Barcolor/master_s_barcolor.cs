﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.BarColor
{
    public class master_s_barcolor : Base
    {
        private static master_s_barcolor instant;

        public static master_s_barcolor GetInstant()
        {
            if (instant == null) instant = new master_s_barcolor();
            return instant;
        }

        public Model.Table.Mssql.BarColor.master_s_barcolor Get(Model.Table.Mssql.BarColor.master_s_barcolor d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id" , d.barcolor_id);

            string cmd = "SELECT * FROM master_s_barcolor WHERE barcolor_id = @barcolor_id ORDER BY barcolor_id ";

            var res = Query<Model.Table.Mssql.BarColor.master_s_barcolor>(cmd, param).FirstOrDefault();
            return res;
        }
        public int Save(Model.Table.Mssql.BarColor.master_s_barcolor d)
        {
            var item = Get(new Model.Table.Mssql.BarColor.master_s_barcolor
            {
                barcolor_id = d.barcolor_id 
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        public List<Model.Table.Mssql.BarColor.master_s_barcolor> Search(Model.Request.Standardpack.Barcolor.Master_s_BarcolorSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_cd", $"%{d.barcolor_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_barcolor WHERE " +
                "(@barcolor_cd IS NULL OR barcolor_cd like @barcolor_cd) AND" +
                "(@txtSearch IS NULL OR barcolor_id like @txtSearch OR barcolor_name like @txtSearch) order by barcolor_cd";

            return Query<Model.Table.Mssql.BarColor.master_s_barcolor>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.BarColor.master_s_barcolor> SearchView(Model.Request.Standardpack.Barcolor.Master_s_BarcolorSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_cd", $"%{d.barcolor_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_barcolor WHERE " +
                "(@barcolor_cd IS NULL OR barcolor_cd like @barcolor_cd) AND" +
                "(@txtSearch IS NULL OR barcolor_id like @txtSearch OR barcolor_name like @txtSearch) order by barcolor_id";

            return Query<Model.Table.Mssql.BarColor.master_s_barcolor>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.BarColor.master_s_barcolor> SearchID(Model.Request.Standardpack.Barcolor.Master_s_BarcolorSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);

            string cmd = "SELECT * FROM master_s_barcolor WHERE barcolor_id = @barcolor_id";

            return Query<Model.Table.Mssql.BarColor.master_s_barcolor>(cmd, param).ToList();
        }

        private int Insert(Model.Table.Mssql.BarColor.master_s_barcolor d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_cd", d.barcolor_cd);
            param.Add("@barcolor_name", d.barcolor_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@status", d.status);

            string cmd = "INSERT INTO master_s_barcolor " +
                "(barcolor_cd,barcolor_name,effdate_st,effdate_en,path_pic,status , user_id , user_name , user_date) " +
                "VALUES (@barcolor_cd,@barcolor_name,@effdate_st,@effdate_en,@path_pic,@status, @user_id , @user_name ,GetDate())" +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.BarColor.master_s_barcolor d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);
            param.Add("@barcolor_cd", d.barcolor_cd);
            param.Add("@barcolor_name", d.barcolor_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_barcolor " +
                "SET barcolor_cd = @barcolor_cd,barcolor_name = @barcolor_name,effdate_st = @effdate_st ,effdate_en = @effdate_en , " +
                "path_pic = @path_pic,status = @status, user_id = @user_id , user_name = @user_name , user_date = GetDate() " +
                "WHERE barcolor_id = @barcolor_id SELECT @barcolor_id As barcolor_id";

            var barcolor_id = ExecuteScalar<int>(cmd, param);

            return barcolor_id;
        }

        public int Move(Model.Table.Mssql.BarColor.master_s_barcolor d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);
            param.Add("@barcolor_cd", d.barcolor_cd);
            param.Add("@cancel_id", d.user_id);
            param.Add("@cancel_name", d.user_name);

            string cmd = "INSERT INTO master_u_barcolor " +
                "(barcolor_id , barcolor_cd,barcolor_name,effdate_st,effdate_en,path_pic,user_id , user_name , user_date) " +
                "SELECT barcolor_id, barcolor_cd,barcolor_name,effdate_st,effdate_en,path_pic,user_id , user_name , user_date " +
                "FROM master_s_barcolor WHERE barcolor_id = @barcolor_id AND barcolor_cd = @barcolor_cd AND status = 'A' " +
                "UPDATE master_u_barcolor SET cancel_id = @cancel_id , cancel_name = @cancel_name , cancel_date = GetDate() , status = 'C' " +
                "WHERE barcolor_id = @barcolor_id AND barcolor_cd = @barcolor_cd " +
                "UPDATE master_s_barcolor SET status = 'C' WHERE barcolor_id = @barcolor_id AND barcolor_cd = @barcolor_cd";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
