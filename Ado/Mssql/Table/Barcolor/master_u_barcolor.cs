﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.BarColor
{
    public class master_u_barcolor : Base
    {
        private static master_u_barcolor instant;

        public static master_u_barcolor GetInstant()
        {
            if (instant == null) instant = new master_u_barcolor();
            return instant;
        }
        public List<Model.Table.Mssql.BarColor.master_u_barcolor> Search(Model.Request.Standardpack.Barcolor.Master_s_BarcolorSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            //param.Add("@barcolor_id", $"%{d.barcolor_id.GetValue()}%");
            param.Add("@barcolor_cd", $"%{d.barcolor_cd.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_barcolor WHERE " +
                //"(@barcolor_id IS NULL OR barcolor_id like @barcolor_id) AND" +
                "(@barcolor_cd IS NULL OR barcolor_cd like @barcolor_cd) AND" +
                "(@txtSearch IS NULL OR barcolor_id like @txtSearch OR barcolor_name like @txtSearch) order by barcolor_id";

            return Query<Model.Table.Mssql.BarColor.master_u_barcolor>(cmd, param).ToList();
        }
    }
}
