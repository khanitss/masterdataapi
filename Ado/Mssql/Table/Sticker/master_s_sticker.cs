﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Sticker
{
    public class master_s_sticker : Base
    {
        private static master_s_sticker instant;

        public static master_s_sticker GetInstant()
        {
            if (instant == null) instant = new master_s_sticker();
            return instant;
        }

        public Model.Table.Mssql.Sticker.master_s_sticker Get(Model.Table.Mssql.Sticker.master_s_sticker d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", d.sticker_id);

            string cmd = "SELECT * FROM master_s_sticker WHERE sticker_id = @sticker_id";

            var res = Query<Model.Table.Mssql.Sticker.master_s_sticker>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.Sticker.master_s_sticker d)
        {
            var item = Get(new Model.Table.Mssql.Sticker.master_s_sticker
            {
                sticker_id = d.sticker_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        
        public List<Model.Table.Mssql.Sticker.master_s_sticker> SearchView(Model.Request.Standardpack.Sticker.Master_s_StickerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", $"%{d.sticker_id.GetValue()}%");
            param.Add("@sticker_cd", $"%{d.sticker_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_sticker WHERE " +
                "(@sticker_id IS NULL OR sticker_id like @sticker_id) AND " +
                "(@sticker_cd IS NULL OR sticker_cd like @sticker_cd) AND " +
                "(@txtSearch IS NULL OR sticker_cd like @txtSearch OR sticker_name like @txtSearch ) order by sticker_cd";

            return Query<Model.Table.Mssql.Sticker.master_s_sticker>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.Sticker.master_s_sticker> Search(Model.Request.Standardpack.Sticker.Master_s_StickerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", $"%{d.sticker_id.GetValue()}%");
            param.Add("@sticker_cd", $"%{d.sticker_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_sticker WHERE " +
                "(@sticker_id IS NULL OR sticker_id like @sticker_id) AND " +
                "(@sticker_cd IS NULL OR sticker_cd like @sticker_cd) AND " +
                "(@txtSearch IS NULL OR sticker_cd like @txtSearch OR sticker_name like @txtSearch) " +
                "AND status = 'A' order by sticker_cd ";

            return Query<Model.Table.Mssql.Sticker.master_s_sticker>(cmd, param).ToList();
        }
        private int Insert(Model.Table.Mssql.Sticker.master_s_sticker d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_cd", d.sticker_cd);
            param.Add("@sticker_name", d.sticker_name);
            param.Add("@status", d.status);
            param.Add("@itemcode", d.itemcode);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@sticker_type", d.sticker_type);
            param.Add("@version", d.version);
            param.Add("@remark", d.remark);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));

            string cmd = "INSERT INTO master_s_sticker " +
                "(sticker_cd,sticker_name,effdate_st,effdate_en,status,itemcode,user_id,user_name,user_date,sticker_type,version,remark,path_pic) " +
                "VALUES " +
                "(@sticker_cd,@sticker_name,@effdate_st,@effdate_en,@status,@itemcode,@user_id,@user_name, GetDate() ,@sticker_type,@version,@remark,@path_pic) " +
                "SELECT SCOPE_IDENTITY(); ";


            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.Sticker.master_s_sticker d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", d.sticker_id);
            param.Add("@sticker_cd", d.sticker_cd);
            param.Add("@sticker_name", d.sticker_name);
            param.Add("@status", d.status);
            param.Add("@itemcode", d.itemcode);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@sticker_type", d.sticker_type);
            param.Add("@version", d.version);
            param.Add("@remark", d.remark);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));


            string cmd = "UPDATE master_s_sticker " +
                "SET sticker_cd = @sticker_cd, sticker_name = @sticker_name,effdate_st = @effdate_st,effdate_en = @effdate_en, " +
                "status = @status,itemcode =  @itemcode,user_id = @user_id,user_name = @user_name, user_date = GetDate()," +
                "sticker_type = @sticker_type,version = @version,remark = @remark,path_pic = @path_pic " +
                "WHERE sticker_id = @sticker_id SELECT @sticker_id AS sticker_id";

            var sticker_id = ExecuteScalar<int>(cmd, param);

            return sticker_id;
        }

        public int Move(Model.Table.Mssql.Sticker.master_s_sticker d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", d.sticker_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            
             

            string cmd = "INSERT INTO master_u_sticker " +
                "(sticker_id,sticker_cd,sticker_name,effdate_st,effdate_en,status,itemcode,user_id,user_name,user_date,sticker_type,version,remark,path_pic) " +
                "SELECT sticker_id,sticker_cd,sticker_name,effdate_st,effdate_en,status,itemcode,user_id,user_name,user_date,sticker_type,version,remark,path_pic " +
                "FROM master_s_sticker WHERE sticker_id = @sticker_id " +
                "UPDATE master_u_sticker SET status = 'C', cancel_id = @user_id , cancel_name = @user_name, cancel_date = GetDate() WHERE sticker_id = @sticker_id " +
                "UPDATE master_s_sticker SET status = 'C' WHERE sticker_id = @sticker_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
