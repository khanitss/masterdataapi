﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Sticker
{
    public class master_u_sticker : Base
    {
        private static master_u_sticker instant;

        public static master_u_sticker GetInstant()
        {
            if (instant == null) instant = new master_u_sticker();
            return instant;
        }
        public List<Model.Table.Mssql.Sticker.master_u_sticker> Search(Model.Request.Standardpack.Sticker.Master_s_StickerSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@sticker_id", $"%{d.sticker_id.GetValue()}%");
            param.Add("@sticker_cd", $"%{d.sticker_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_sticker WHERE " +
                "(@sticker_id IS NULL OR sticker_id like @sticker_id) AND " +
                "(@sticker_cd IS NULL OR sticker_cd like @sticker_cd) AND " +
                "(@txtSearch IS NULL OR sticker_cd like @txtSearch OR sticker_name like @txtSearch) " +
                "order by sticker_cd ";

            return Query<Model.Table.Mssql.Sticker.master_u_sticker>(cmd, param).ToList();
        }
    }
}

