﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.StrapSpec
{
    public class master_s_strapspec : Base
    {
        private static master_s_strapspec instant;

        public static master_s_strapspec GetInstant()
        {
            if (instant == null) instant = new master_s_strapspec();
            return instant;
        }

        public Model.Table.Mssql.StrapSpec.master_s_strapspec Get(Model.Table.Mssql.StrapSpec.master_s_strapspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strapspec_id", d.strapspec_id);

            string cmd = "SELECT * FROM master_s_strapspec WHERE strapspec_id = @strapspec_id";

            return Query<Model.Table.Mssql.StrapSpec.master_s_strapspec>(cmd, param).FirstOrDefault();
        }

        public List<Model.Table.Mssql.StrapSpec.master_s_strapspec> Search(Model.Request.Standardpack.StrapSpec.Master_s_StrapSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strapspec_id", $"%{d.strapspec_id.GetValue()}%");
            param.Add("@strap_cd", $"%{d.strap_cd.GetValue()}%");
            param.Add("@strap_id", $"%{d.strap_id.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@label_id", $"%{d.label_id.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_strapspec WHERE " +
                "(@strapspec_id IS NULL OR strapspec_id like @strapspec_id) AND " +
                "(@strap_cd IS NULL OR strap_cd like @strap_cd ) AND " +
                "(@strap_id IS NULL OR strap_id like @strap_id ) AND " +
                "(@label_cd IS NULL OR label_cd like @label_cd ) AND " +
                "(@label_id IS NULL OR label_id like @label_id ) AND " +
                "(@txtSearch IS NULL OR label_CD Like @txtSearch OR strap_cd like @txtSearch ) ORDER BY strap_cd";

            return Query<Model.Table.Mssql.StrapSpec.master_s_strapspec>(cmd, param).ToList();
        }

        public int Save(Model.Table.Mssql.StrapSpec.master_s_strapspec d)
        {
            var item = Get(new Model.Table.Mssql.StrapSpec.master_s_strapspec
            {
                strapspec_id = d.strapspec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        private int Insert(Model.Table.Mssql.StrapSpec.master_s_strapspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strap_id", d.strap_id);
            param.Add("@strap_cd", d.strap_cd);
            param.Add("@strap_name", d.strap_name);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name) ;
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_strapspec " +
                "(strap_id,label_id,strap_cd,label_cd,effdate_st,effdate_en,status,user_id,user_name,user_date,strap_name,label_name ) " +
                "VALUES " +
                "(@strap_id ,@label_id ,@strap_cd ,@label_cd ,@effdate_st ,@effdate_en ,@status ,@user_id ,@user_name ,GetDate() ,@strap_name ,@label_name ) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);

        }
        private int Update(Model.Table.Mssql.StrapSpec.master_s_strapspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strapspec_id", d.strapspec_id);
            param.Add("@strap_id", d.strap_id);
            param.Add("@strap_cd", d.strap_cd);
            param.Add("@strap_name", d.strap_name);
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name", d.label_name) ;
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_strapspec SET " +
                "strap_id = @strap_id ,label_id = @label_id ,strap_cd = @strap_cd ,label_cd = @label_cd ,effdate_st = @effdate_st ,effdate_en = @effdate_en ,status = @status ,user_id = @user_id ," +
                "user_name = @user_name ,user_date = GetDate() ,strap_name = @strap_name ,label_name = @label_name " +
                "WHERE strapspec_id = @strapspec_id SELECT @strapspec_id AS strapspec_id";

            var strapspec_id = ExecuteScalar<int>(cmd, param);

            return strapspec_id;
        }
        public int Move(Model.Table.Mssql.StrapSpec.master_s_strapspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strapspec_id", d.strapspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_strapspec " +
                "(strapspec_id, strap_id,label_id,strap_cd,label_cd,effdate_st,effdate_en,status,user_id,user_name,user_date,strap_name,label_name) " +
                "SELECT strapspec_id, strap_id,label_id,strap_cd,label_cd,effdate_st,effdate_en,status,user_id,user_name,user_date,strap_name,label_name " +
                "FROM master_s_strapspec  WHERE strapspec_id = @strapspec_id " +
                "UPDATE master_u_strapspec SET status = 'C', cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE strapspec_id = @strapspec_id " +
                "UPDATE master_s_strapspec SET status ='C' WHERE strapspec_id = @strapspec_id";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
