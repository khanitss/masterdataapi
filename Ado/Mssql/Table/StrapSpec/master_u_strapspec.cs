﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.StrapSpec
{
    public class master_u_strapspec : Base
    {
        private static master_u_strapspec instant;

        public static master_u_strapspec GetInstant()
        {
            if (instant == null) instant = new master_u_strapspec();
            return instant;
        }

        public List<Model.Table.Mssql.StrapSpec.master_u_strapspec> Search(Model.Request.Standardpack.StrapSpec.Master_s_StrapSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@strapspec_id", $"%{d.strapspec_id.GetValue()}%");
            param.Add("@strap_cd", $"%{d.strap_cd.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_strapspec WHERE " +
                "(@strapspec_id IS NULL OR strapspec_id like @strapspec_id) AND " +
                "(@strap_cd IS NULL OR strap_cd like @strap_cd ) AND " +
                "(@label_cd IS NULL OR label_cd like @label_cd ) AND " +
                "(@txtSearch IS NULL OR label_CD Like @txtSearch OR strap_cd like @txtSearch ) ORDER BY strap_cd";

            return Query<Model.Table.Mssql.StrapSpec.master_u_strapspec>(cmd, param).ToList();
        }
    }
}
