﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Bag
{
    public class master_u_bag : Base
    {
        private static master_u_bag instant;

        public static master_u_bag GetInstant()
        {
            if (instant == null) instant = new master_u_bag();
            return instant;
        }

        public List<Model.Table.Mssql.Bag.master_u_bag> Search(Model.Request.Standardpack.Bag.Master_s_BagSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", $"%{d.bag_id.GetValue()}%");
            param.Add("@bag_cd", $"%{d.bag_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_bag WHERE " +
                "(@bag_id IS NULL OR bag_id like @bag_id ) AND " +
                "(@bag_cd IS NULL OR bag_cd like @bag_cd ) AND " +
                "(@txtSearch IS NULL OR bag_name_th like @txtSearch OR bag_name_en like @txtSearch ) " +
                "order by bag_cd ";

            return Query<Model.Table.Mssql.Bag.master_u_bag>(cmd, param).ToList();

        }
    }
}
