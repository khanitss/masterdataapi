﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Bag
{
    public class master_s_bag : Base
    {

        private static master_s_bag instant;

        public static master_s_bag GetInstant()
        {
            if (instant == null) instant = new master_s_bag();
            return instant;
        }

        public List<Model.Table.Mssql.Bag.master_s_bag> Search(Model.Request.Standardpack.Bag.Master_s_BagSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", $"%{d.bag_id.GetValue()}%");
            param.Add("@bag_cd", $"%{d.bag_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_bag WHERE " +
                "(@bag_id IS NULL OR bag_id like @bag_id ) AND " +
                "(@bag_cd IS NULL OR bag_cd like @bag_cd ) AND " +
                "(@txtSearch IS NULL OR bag_name_th like @txtSearch OR bag_name_en like @txtSearch ) " +
                "order by bag_cd ";

            return Query<Model.Table.Mssql.Bag.master_s_bag>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.Bag.master_s_bag> SearchActive(Model.Request.Standardpack.Bag.Master_s_BagSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", $"%{d.bag_id.GetValue()}%");
            param.Add("@bag_cd", $"%{d.bag_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_bag WHERE " +
                "(@bag_id IS NULL OR bag_id like @bag_id ) AND " +
                "(@bag_cd IS NULL OR bag_cd like @bag_cd ) AND " +
                "(@txtSearch IS NULL OR bag_name_th like @txtSearch OR bag_name_en like @txtSearch ) " +
                "AND status = 'A' " +
                "order by bag_cd ";

            return Query<Model.Table.Mssql.Bag.master_s_bag>(cmd, param).ToList();
        }
        public Model.Table.Mssql.Bag.master_s_bag Get(Model.Table.Mssql.Bag.master_s_bag d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", d.bag_id);

            string cmd = "SELECT * FROM master_s_bag WHERE bag_id = @bag_id";

            var res = Query<Model.Table.Mssql.Bag.master_s_bag>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.Bag.master_s_bag d)
        {
            var item = Get(new Model.Table.Mssql.Bag.master_s_bag
            {
                bag_id = d.bag_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        public int Insert(Model.Table.Mssql.Bag.master_s_bag d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_cd", d.bag_cd);
            param.Add("@bag_name_th", d.bag_name_th);
            param.Add("@bag_name_en", d.bag_name_en);
            param.Add("@bag_cd_old", d.bag_cd_old);
            param.Add("@width", d.width);
            param.Add("@length", d.length);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_bag " +
                "(bag_cd,bag_name_th,bag_name_en ,bag_cd_old ,width,length,path_pic ,status,effdate_st,effdate_en,user_id,user_name,user_date) " +
                "VALUES " +
                "(@bag_cd,@bag_name_th,@bag_name_en ,@bag_cd_old ,@width,@length ,@path_pic ,@status,@effdate_st,@effdate_en,@user_id,@user_name, GetDate() ) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        public int Update(Model.Table.Mssql.Bag.master_s_bag d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", d.bag_id);
            param.Add("@bag_cd", d.bag_cd);
            param.Add("@bag_name_th", d.bag_name_th);
            param.Add("@bag_name_en", d.bag_name_en);
            param.Add("@bag_cd_old", d.bag_cd_old);
            param.Add("@width", d.width);
            param.Add("@length", d.length);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_bag SET " +
                "bag_cd = @bag_cd,bag_name_th = @bag_name_th,bag_name_en = @bag_name_en ,bag_cd_old =@bag_cd_old ,width = @width ,length =@length ,path_pic =@path_pic, status = @status ," +
                "effdate_st = @effdate_st ,effdate_en = @effdate_en ,user_id = @user_id ,user_name = @user_name ,user_date = GetDate() " +
                "WHERE bag_id = @bag_id " +
                "SELECT @bag_id As bag_id ";

            var bag_id = ExecuteScalar<int>(cmd, param);

            return bag_id;
        }

        public int Move(Model.Table.Mssql.Bag.master_s_bag d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@bag_id", d.bag_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_bag " +
                "(bag_id,bag_cd,bag_name_th,bag_name_en,width,length,bag_cd_old,path_pic,status,effdate_st,effdate_en,user_id,user_name,user_date ) " +
                "SELECT bag_id,bag_cd,bag_name_th,bag_name_en,width,length,bag_cd_old,path_pic,status,effdate_st,effdate_en,user_id,user_name,user_date " +
                "FROM master_s_bag WHERE bag_id = @bag_id " +
                "UPDATE master_u_bag SET status = 'C', cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE bag_id = @bag_id " +
                "DELETE master_s_bag WHERE bag_id = @bag_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
