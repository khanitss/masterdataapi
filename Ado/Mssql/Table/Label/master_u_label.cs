﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table
{
    public class master_u_label : Base
    {
        private static master_u_label instant;

        public static master_u_label GetInstant()
        {
            if (instant == null) instant = new master_u_label();
            return instant;
        }

        public List<Model.Table.Mssql.master_u_label> SearchUlabel(Model.Request.Standardpack.View_master_s_labelSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_label WHERE label_cd Like @txtSearch";

            return Query<Model.Table.Mssql.master_u_label>(cmd, param).ToList();
        }
    }
}
