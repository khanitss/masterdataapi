﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table
{
    public class master_s_label : Base
    {
        private static master_s_label instant;

        public static master_s_label GetInstant()
        {
            if (instant == null) instant = new master_s_label();
            return instant;
        }

        public List<Model.Table.Mssql.master_s_label> Search(Model.Request.Standardpack.View_master_s_labelSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");

            string cmd = "SELECT * FROM master_s_label WHERE" +
                " ( @label_cd IS NULL OR label_cd Like @label_cd) AND " +
                "( @txtSearch IS NULL OR label_name_th Like @txtSearch OR label_name_en Like @txtSearch) " +
                " order by label_cd";

            return Query<Model.Table.Mssql.master_s_label>(cmd, param).ToList();
        }

        public int SearchLastID(Model.Table.Mssql.master_s_label d)
        {
            DynamicParameters param = new DynamicParameters();

            string cmd = "select max(label_id) AS max_id from master_s_label";

            var max_id = ExecuteScalar<int>(cmd, param);

            return max_id;
        }

        public List<Model.Table.Mssql.master_s_label> SearchActive(Model.Request.Standardpack.View_master_s_labelSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_label WHERE label_cd Like @txtSearch AND status = 'A' order by label_cd";


            return Query<Model.Table.Mssql.master_s_label>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.master_s_label> SearchID(Model.Request.Standardpack.View_master_s_labelSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_label WHERE label_id Like @txtSearch";

            return Query<Model.Table.Mssql.master_s_label>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.master_s_label> Save(Model.Request.Standardpack.Master_s_lableSaveReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@label_cd", d.label_cd);
            param.Add("@label_name_th", d.label_name_th);
            param.Add("@label_name_en", d.label_name_en);
            param.Add("@label_cd_old", d.label_cd_old);
            param.Add("@width", d.width);
            param.Add("@length", d.length);
            param.Add("@label_type", d.label_type);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status); 
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);           
            param.Add("@itemcode", d.itemcode);
            param.Add("@version", d.version);
            param.Add("@remark", d.remark);
            
            param.Add("@acczonelabelcode_id", d.acczonelabelcode_id);
            param.Add("@acczonelabelcode_cd", d.acczonelabelcode_cd);
            param.Add("@labelowner_id", d.labelowner_id);
            param.Add("@labelgrade_id", d.labelgrade_id);
            param.Add("@labelgrade_cd", d.labelgrade_cd);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@zone_code", d.zone_code);
            param.Add("@labelpapertype_id", d.labelpapertype_id);
            param.Add("@labelpapertype_cd", d.labelpapertype_cd);
            param.Add("@digit1", d.digit1);
            param.Add("@digit23", d.digit23);
            param.Add("@digit4", d.digit4);


        string cmd = "INSERT INTO master_s_label " +
                "(label_cd,label_name_th,label_name_en,width,length,label_type,effdate_st,effdate_en,status,path_pic,user_id, user_name,user_date,itemcode,version,remark ," +
                "acczonelabelcode_id,acczonelabelcode_cd,labelowner_id,labelgrade_id,labelrade_cd,cus_cod,zone_code,labelpapertype_id,labelpapertype_cd,digit1,digit23,digit4) " +
                "VALUES " +
                "(@label_cd,@label_name_th,@label_name_en,@width,@length,@label_type,@effdate_st,@effdate_en,@status,@path_pic,@user_id,@user_name,Getdate(),@itemcode,@version,@remark," +
                "@acczonelabelcode_id,@acczonelabelcode_cd,@labelowner_id,@labelgrade_id,@labelgrade_cd,@cus_cod,@zone_code,@labelpapertype_id,@labelpapertype_cd,@digit1,@digit23,@digit4)";

            var res = Query<Model.Table.Mssql.master_s_label>(cmd, param).ToList();
            return res;
        }
        //public int Save(Model.Table.Mssql.master_s_label d)
        //{
        //    DynamicParameters param = new DynamicParameters();
        //    param.Add("@label_id", d.label_id);
        //    param.Add("@label_cd", d.label_cd);
        //    param.Add("@label_name_th", d.label_name_th);
        //    param.Add("@label_name_en", d.label_name_en);
        //    param.Add("@label_cd_old", d.label_cd_old);
        //    param.Add("@width", d.width);
        //    param.Add("@length", d.length);
        //    param.Add("@label_type", d.label_type);
        //    param.Add("@effdate_st", d.effdate_st);
        //    param.Add("@effdate_en", d.effdate_en);
        //    param.Add("@status", d.status);
        //    param.Add("@path_pic", d.path_pic);

        //    string cmd = "INSERT INTO master_s_label " +
        //        "(label_cd,label_name_th,label_name_en,label_cd_old,width,length,label_type,effdate_s,effdate_en,status,path_pic) " +
        //        "VALUES " +
        //        "(@label_cd,@label_name_th,@label_name_en,@label_cd_old,@width,@length,@label_type,@effdate_s,@effdate_en,@status,@path_pic)";

        //    return ExecuteScalar<int>(cmd, param);

        //}
        public List<Model.Table.Mssql.master_s_label> Update(Model.Request.Standardpack.Master_s_lableSaveReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@label_name_th", d.label_name_th);
            param.Add("@label_name_en", d.label_name_en);
            param.Add("@width", d.width);
            param.Add("@length", d.length);
            param.Add("@label_type", d.label_type);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@label_id", d.label_id);
            param.Add("@itemcode", d.itemcode);
            param.Add("@version", d.version);
            param.Add("@remark", d.remark);


            string cmd = "UPDATE master_s_label SET " +
                "label_name_th = @label_name_th ,label_name_en = @label_name_en ," +
                "width = @width ,length = @length ,label_type = @label_type ,effdate_st = @effdate_st ,effdate_en = @effdate_en," +
                "status = @status , path_pic = @path_pic , itemcode = @itemcode, version = @version , remark = @remark " +
                "Where label_id = @label_id";

            var res = Query<Model.Table.Mssql.master_s_label>(cmd, param).ToList();
            return res;
        }

        public int Move(Model.Table.Mssql.master_u_label d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);


            string cmd = "INSERT INTO master_u_label ( label_id,label_cd,label_name_th ,label_name_en,label_cd_old ,width,length ,label_type,effdate_st ,effdate_en,status,path_pic ,user_id,user_name,user_date,version,remark," +
                "acczonelabelcode_id,acczonelabelcode_cd,labelowner_id,labelowner_id,labelgrade_id,cus_cod,zone_code,labelpapertype_id,labelpapertype_cd,digit1,digit23,digit4 )" +
                " SELECT label_id, label_cd, label_name_th, label_name_en, label_cd_old, width, length, label_type, effdate_st, effdate_en, status, path_pic, user_id, user_name, user_date , version,remark," +
                "acczonelabelcode_id,acczonelabelcode_cd,labelowner_id,labelowner_id,labelgrade_id,cus_cod,zone_code,labelpapertype_id,labelpapertype_cd,digit1,digit23,digit4 FROM master_s_label " +
                " WHERE( label_id = @label_id AND label_cd = @label_cd)" +

                " UPDATE master_u_label SET cancel_id = @user_id,cancel_name = @user_name,cancel_date = GetDATE(), status = 'C' WHERE (label_id = @label_id AND label_cd = @label_cd)" +

                " UPDATE master_s_label SET status = 'C' WHERE (label_id = @label_id AND label_cd = @label_cd )";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
