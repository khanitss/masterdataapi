﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.RopeSpec
{
    public class master_s_ropespec : Base
    {
        private static master_s_ropespec instant;

        public static master_s_ropespec GetInstant()
        {
            if (instant == null) instant = new master_s_ropespec();
            return instant;
        }

        public List<Model.Table.Mssql.RopeSpec.master_s_ropespec> Search(Model.Request.Standardpack.RopeSpec.Master_s_RopeSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@codeSearch", $"%{d.codeSearch.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT DISTINCT(cus_cod),tsale,cus_name ,status FROM master_s_ropespec WHERE " +
                "(@codeSearch IS NULL OR rope_cd LIKE @codeSearch OR cus_cod LIKE @codeSearch OR produccttype_code LIKE @codeSearch ) AND " +
                "(@txtSearch IS NULL OR rope_name LIKE @txtSearch OR cus_name LIKE @txtSearch OR producttype_desc LIKE @txtSearch ) " +
                "UNION SELECT DISTINCT(cus_cod),tsale, cus_name,status FROM master_u_ropespec WHERE " +
                "(@codeSearch IS NULL OR rope_cd LIKE @codeSearch OR cus_cod LIKE @codeSearch OR produccttype_code LIKE @codeSearch ) AND " +
                "(@txtSearch IS NULL OR rope_name LIKE @txtSearch OR cus_name LIKE @txtSearch OR producttype_desc LIKE @txtSearch ) " +
                "order by cus_cod ";

            return Query<Model.Table.Mssql.RopeSpec.master_s_ropespec>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.RopeSpec.master_s_ropespec> SearchByValue(Model.Request.Standardpack.RopeSpec.Master_s_RopeSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cus_cod", $"%{d.cus_cod.GetValue()}%");
            param.Add("@ropespec_id", $"%{d.ropespec_id.GetValue()}%");

            string cmd = "SELECT ropespec_id,tsale,cus_cod,produccttype_code,rope_id,rope_cd,rope_name,status,effdate_st,effdate_en,user_id,user_name," +
                "user_date,cus_name,producttype_desc,'' as cancel_id,'' as cancel_name,'' as cancel_date FROM master_s_ropespec WHERE " +
                "(@cus_cod IS NULL OR cus_cod LIKE @cus_cod ) AND " +
                "(@ropespec_id IS NULL OR ropespec_id LIKE @ropespec_id ) " +
                "UNION SELECT ropespec_id,tsale,cus_cod,produccttype_code,rope_id,rope_cd,rope_name,status,effdate_st,effdate_en,user_id,user_name," +
                "user_date,cus_name,producttype_desc,cancel_id, cancel_name , cancel_date FROM master_u_ropespec WHERE " +
                "(@cus_cod IS NULL OR cus_cod LIKE @cus_cod ) AND " +
                "(@ropespec_id IS NULL OR ropespec_id LIKE @ropespec_id ) " +
                "order by cus_cod ";

            return Query<Model.Table.Mssql.RopeSpec.master_s_ropespec>(cmd, param).ToList();
        }

        public Model.Table.Mssql.RopeSpec.master_s_ropespec Get(Model.Table.Mssql.RopeSpec.master_s_ropespec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ropespec_id", d.ropespec_id);

            string cmd = "SELECT * FROM master_s_ropespec WHERE ropespec_id = @ropespec_id ";

            var res = Query<Model.Table.Mssql.RopeSpec.master_s_ropespec>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.RopeSpec.master_s_ropespec d)
        {
            var item = Get(new Model.Table.Mssql.RopeSpec.master_s_ropespec
            {
                ropespec_id = d.ropespec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        private int Insert(Model.Table.Mssql.RopeSpec.master_s_ropespec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@tsale", d.tsale);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@produccttype_code", d.produccttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@rope_id", d.rope_id);
            param.Add("@rope_cd", d.rope_cd);
            param.Add("@rope_name", d.rope_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_ropespec " +
                "(tsale,cus_cod,produccttype_code,rope_id,rope_cd,rope_name,status,effdate_st,effdate_en," +
                "user_id,user_name,user_date,cus_name,producttype_desc) " +
                "VALUES " +
                "(@tsale,@cus_cod,@produccttype_code,@rope_id,@rope_cd,@rope_name,@status,@effdate_st,@effdate_en," +
                "@user_id,@user_name,GetDate(),@cus_name,@producttype_desc) " +
                "SELECT SCOPE_IDENTITY();";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.RopeSpec.master_s_ropespec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ropespec_id", d.ropespec_id);
            param.Add("@tsale", d.tsale);
            param.Add("@cus_cod", d.cus_cod);
            param.Add("@cus_name", d.cus_name);
            param.Add("@produccttype_code", d.produccttype_code);
            param.Add("@producttype_desc", d.producttype_desc);
            param.Add("@rope_id", d.rope_id);
            param.Add("@rope_cd", d.rope_cd);
            param.Add("@rope_name", d.rope_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_ropespec SET " +
                "tsale = @tsale ,cus_cod = @cus_cod,produccttype_code = @produccttype_code,rope_id = @rope_id ,rope_cd = @rope_cd ," +
                "rope_name = @rope_name ,status = @status ,effdate_st = @effdate_st ,effdate_en = @effdate_en ,user_id = @user_id ,user_name = @user_name," +
                "user_date = GetDate(),cus_name = @cus_name, producttype_desc = @producttype_desc " +
                "WHERE ropespec_id = @ropespec_id " +
                "SELECT @ropespec_id AS ropespec_id ";

            var ropespec_id = ExecuteScalar<int>(cmd, param);

            return ropespec_id;
        }

        public int Move(Model.Table.Mssql.RopeSpec.master_s_ropespec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@ropespec_id", d.ropespec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_ropespec " +
                "(ropespec_id,tsale,cus_cod,produccttype_code,rope_id,rope_cd,rope_name,status,effdate_st,effdate_en,user_id,user_name,user_date,cus_name,producttype_desc) " +
                "SELECT " +
                "ropespec_id,tsale,cus_cod,produccttype_code,rope_id,rope_cd,rope_name,status,effdate_st,effdate_en,user_id,user_name,user_date,cus_name,producttype_desc " +
                "FROM master_s_ropespec WHERE ropespec_id = @ropespec_id " +
                "UPDATE master_u_ropespec SET status = 'C' , cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE ropespec_id = @ropespec_id " +
                "DELETE master_s_ropespec WHERE ropespec_id = @ropespec_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
