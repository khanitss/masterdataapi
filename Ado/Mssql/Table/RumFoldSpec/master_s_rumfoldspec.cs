﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.RumFoldSpec
{
    public class master_s_rumfoldspec : Base
    {
        private static master_s_rumfoldspec instant;

        public static master_s_rumfoldspec GetInstant()
        {
            if (instant == null) instant = new master_s_rumfoldspec();
            return instant;
        }

        public List<Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec> Search(Model.Request.Standardpack.RumFoldSpec.Master_s_RumFoldSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumfoldspec_id", $"%{d.rumfoldspec_id.GetValue()}%");
            param.Add("@rumtypeid", $"%{d.rumtypeid.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_rumfoldspec WHERE " +
                "(@rumtypeid IS NULL OR rumtypeid like @rumtypeid ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@rumfoldspec_id IS NULL OR rumfoldspec_id like @rumfoldspec_id ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch ) " +
                "order by rumfoldspec_id";

            return Query<Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec>(cmd, param).ToList();
        }
        public int Save(Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec d)
        {
            var item = Get(new Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec
            {
                rumfoldspec_id = d.rumfoldspec_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        public Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec Get(Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumfoldspec_id", d.rumfoldspec_id);

            string cmd = "SELECT * FROM master_s_rumfoldspec WHERE rumfoldspec_id = @rumfoldspec_id";

            var res = Query<Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec>(cmd, param).FirstOrDefault();

            return res;
        }
        public int Insert(Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumtypeid", d.rumtypeid);
            param.Add("@rumtypename", d.rumtypename);
            param.Add("@fold_id", d.fold_id);
            param.Add("@fold_code", d.fold_code);
            param.Add("@fold_name", d.fold_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_rumfoldspec " +
                "(rumtypeid,rumtypename,fold_id,fold_code,fold_name,status,effdate_st,effdate_en,user_id,user_name,user_date) " +
                "VALUES " +
                "(@rumtypeid,@rumtypename,@fold_id,@fold_code,@fold_name,@status,@effdate_st,@effdate_en,@user_id,@user_name,GetDate()) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);

        }
        public int Update(Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumfoldspec_id", d.rumfoldspec_id);
            param.Add("@rumtypeid", d.rumtypeid);
            param.Add("@rumtypename", d.rumtypename);
            param.Add("@fold_id", d.fold_id);
            param.Add("@fold_code", d.fold_code);
            param.Add("@fold_name", d.fold_name);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_rumfoldspec SET " +
                "rumtypeid = @rumtypeid,rumtypename = @rumtypename,fold_id = @fold_id,fold_code = @fold_code,fold_name = @fold_name, " +
                "status = @status,effdate_st = @effdate_st,effdate_en = @effdate_en,user_id = @user_id,user_name = @user_name,user_date = GetDate() " +
                "WHERE rumfoldspec_id = @rumfoldspec_id " +
                "SELECT @rumfoldspec_id As rumfoldspec_id";

            var rumfoldspec_id = ExecuteScalar<int>(cmd, param);
            
            return rumfoldspec_id;
        }
        public int Move(Model.Table.Mssql.RumFoldSpec.master_s_rumfoldspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumfoldspec_id", d.rumfoldspec_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_rumfoldspec " +
                "(rumfoldspec_id , rumtypeid,rumtypename,fold_id,fold_code,fold_name,status,effdate_st,effdate_en,user_id,user_name,user_date ) " +
                "SELECT rumfoldspec_id , rumtypeid,rumtypename,fold_id,fold_code,fold_name,status,effdate_st,effdate_en,user_id,user_name,user_date " +
                "FROM master_s_rumfoldspec " +
                "WHERE rumfoldspec_id = @rumfoldspec_id " +
                "UPDATE master_u_rumfoldspec SET status = 'C' ,cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE rumfoldspec_id = @rumfoldspec_id " +
                "DELETE master_s_rumfoldspec WHERE rumfoldspec_id = @rumfoldspec_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}