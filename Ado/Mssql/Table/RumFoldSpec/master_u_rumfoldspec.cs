﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.RumFoldSpec
{
    public class master_u_rumfoldspec : Base
    {
        private static master_u_rumfoldspec instant;

        public static master_u_rumfoldspec GetInstant()
        {
            if (instant == null) instant = new master_u_rumfoldspec();
            return instant;
        }

        public List<Model.Table.Mssql.RumFoldSpec.master_u_rumfoldspec> Search(Model.Request.Standardpack.RumFoldSpec.Master_s_RumFoldSpecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rumfoldspec_id", $"%{d.rumfoldspec_id.GetValue()}%");
            param.Add("@rumtypeid", $"%{d.rumtypeid.GetValue()}%");
            param.Add("@fold_code", $"%{d.fold_code.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_rumfoldspec WHERE " +
                "(@rumtypeid IS NULL OR rumtypeid like @rumtypeid ) AND " +
                "(@fold_code IS NULL OR fold_code like @fold_code ) AND " +
                "(@rumfoldspec_id IS NULL OR rumfoldspec_id like @rumfoldspec_id ) AND " +
                "(@txtSearch IS NULL OR fold_name like @txtSearch ) " +
                "order by rumfoldspec_id";

            return Query<Model.Table.Mssql.RumFoldSpec.master_u_rumfoldspec>(cmd, param).ToList();
        }
    }
}
