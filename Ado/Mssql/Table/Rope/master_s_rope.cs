﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Rope
{
    public class master_s_rope : Base
    {
        private static master_s_rope instant;

        public static master_s_rope GetInsant()
        {
            if (instant == null) instant = new master_s_rope();
            return instant;
        }
        public List<Model.Table.Mssql.Rope.master_s_rope > Search(Model.Request.Standardpack.Rope.Master_s_RopeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_id", $"%{d.rope_id.GetValue()}%");
            param.Add("@rope_cd", $"%{d.rope_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT rope_id ,rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name , user_date,'' as cancel_id,'' as cancel_name,'' as cancel_date FROM master_s_rope WHERE " +
                "(@rope_id IS NULL OR rope_id like @rope_id ) AND " +
                "(@rope_cd IS NULL OR rope_cd like @rope_cd ) AND " +
                "(@txtSearch IS NULL OR rope_name like @txtSearch ) " +
                "UNION SELECT rope_id ,rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name, user_date, cancel_id, cancel_name , cancel_date FROM master_u_rope WHERE " +
                "(@rope_id IS NULL OR rope_id like @rope_id ) AND " +
                "(@rope_cd IS NULL OR rope_cd like @rope_cd ) AND " +
                "(@txtSearch IS NULL OR rope_name like @txtSearch ) " +
                "order by rope_cd ";

            return Query<Model.Table.Mssql.Rope.master_s_rope>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.Rope.master_s_rope> SearchActive(Model.Request.Standardpack.Rope.Master_s_RopeSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_id", $"%{d.rope_id.GetValue()}%");
            param.Add("@rope_cd", $"%{d.rope_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT rope_id ,rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name , user_date FROM master_s_rope WHERE " +
                "(@rope_id IS NULL OR rope_id like @rope_id ) AND " +
                "(@rope_cd IS NULL OR rope_cd like @rope_cd ) AND " +
                "(@txtSearch IS NULL OR rope_name like @txtSearch ) AND " +
                "status = 'A' " +
                "order by rope_cd ";

            return Query<Model.Table.Mssql.Rope.master_s_rope>(cmd, param).ToList();
        }
        public Model.Table.Mssql.Rope.master_s_rope Get(Model.Table.Mssql.Rope.master_s_rope d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_id", d.rope_id);

            string cmd = "SELECT * FROM master_s_rope WHERE rope_id = @rope_id";

            var res = Query<Model.Table.Mssql.Rope.master_s_rope>(cmd, param).FirstOrDefault();

            return res;
        }

        public int Save(Model.Table.Mssql.Rope.master_s_rope d)
        {
            var item = Get(new Model.Table.Mssql.Rope.master_s_rope
            {
                rope_id = d.rope_id
            });
            if (item != null)
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }

        public int Insert(Model.Table.Mssql.Rope.master_s_rope d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_cd", d.rope_cd);
            param.Add("@rope_name", d.rope_name);
            param.Add("@rope_cd_old", d.rope_cd_old);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@itemcode", d.itemcode);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_rope " +
                "(rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name , user_date) " +
                "VALUES " +
                "(@rope_cd, @rope_name, @rope_cd_old, @path_pic, @itemcode, @status , @effdate_st, @effdate_en, @user_id , @user_name , GetDate() ) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }

        public int Update(Model.Table.Mssql.Rope.master_s_rope d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_id", d.rope_id);
            param.Add("@rope_cd", d.rope_cd);
            param.Add("@rope_name", d.rope_name);
            param.Add("@rope_cd_old", d.rope_cd_old);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@itemcode", d.itemcode);
            param.Add("@status", d.status);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_rope SET " +
                "rope_cd = @rope_cd , rope_name = @rope_name, rope_cd_old = @rope_cd_old, path_pic = @path_pic , itemcode = @itemcode" +
                ", status = @status , effdate_st = @effdate_st ,effdate_en = @effdate_en , user_id  = @user_id , user_name =  @user_name , user_date = GetDate()" +
                "WHERE rope_id = @rope_id " +
                "SELECT @rope_id AS rope_id";

            var rope_id = ExecuteScalar<int>(cmd, param);

            return rope_id;
        }

        public int Move(Model.Table.Mssql.Rope.master_s_rope d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@rope_id", d.rope_id);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_rope " +
                "(rope_id ,rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name , user_date ) " +
                "SELECT rope_id ,rope_cd, rope_name, rope_cd_old, path_pic, itemcode, status , effdate_st, effdate_en, user_id , user_name , user_date " +
                "FROM master_s_rope WHERE rope_id = @rope_id " +
                "UPDATE master_u_rope SET status = 'C' , cancel_id = @user_id , cancel_name = @user_name , cancel_date = GetDate() WHERE rope_id = @rope_id " +
                "DELETE master_s_rope WHERE rope_id = @rope_id ";

            return ExecuteNonQuery(cmd, param);
        }
    }
}
