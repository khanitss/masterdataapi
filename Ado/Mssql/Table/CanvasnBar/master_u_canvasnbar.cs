﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.CanvasnBar
{
    public class master_u_canvasnbar : Base
    {
        private static master_u_canvasnbar instant;

        public static master_u_canvasnbar GetInstant()
        {
            if (instant == null) instant = new master_u_canvasnbar();
            return instant;
        }
        public List<Model.Table.Mssql.CanvasnBar.master_u_canvasnbar> Search(Model.Request.Standardpack.CanvasnBar.Master_s_CanvasnBarSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_cd", $"%{d.canvasnbar_cd.GetValue()}%");
            param.Add("@canvasnbar_id", $"%{d.canvasnbar_id.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_canvasnbar WHERE " +
                 "(@canvasnbar_cd IS NULL OR canvasnbar_cd like @canvasnbar_cd ) AND " +
                "(@canvasnbar_id IS NULL OR canvasnbar_id like @canvasnbar_id ) AND " +
                "(@txtsearch IS NULL OR canvasnbar_cd like @txtsearch OR canvasnbar_name like @txtsearch ) order by canvasnbar_id";

            return Query<Model.Table.Mssql.CanvasnBar.master_u_canvasnbar>(cmd, param).ToList();
        }
    }
}
