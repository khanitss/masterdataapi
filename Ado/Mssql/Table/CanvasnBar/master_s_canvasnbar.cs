﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.CanvasnBar
{
    public class master_s_canvasnbar : Base
    {
        private static master_s_canvasnbar instant;

        public static master_s_canvasnbar GetInstant()
        {
            if (instant == null) instant = new master_s_canvasnbar();
            return instant;
        }

        public Model.Table.Mssql.CanvasnBar.master_s_canvasnbar Get(Model.Table.Mssql.CanvasnBar.master_s_canvasnbar d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            //param.Add("@canvasnbar_cd", d.canvasnbar_cd);

            string cmd = "SELECT * FROM master_s_canvasnbar WHERE canvasnbar_id = @canvasnbar_id";
            //string cmd = "SELECT * FROM master_s_canvasnbar WHERE canvasnbar_id = @canvasnbar_id AND canvasnbar_cd = @canvasnbar_cd";

            var res = Query<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar>(cmd, param).FirstOrDefault();

            return res;
        }
        public int Save(Model.Table.Mssql.CanvasnBar.master_s_canvasnbar d)
        {
            var item = Get(new Model.Table.Mssql.CanvasnBar.master_s_canvasnbar
            {
                canvasnbar_id = d.canvasnbar_id,
                canvasnbar_cd = d.canvasnbar_cd
            });
            if (item != null )
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        public List<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar> Search(Model.Request.Standardpack.CanvasnBar.Master_s_CanvasnBarSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_cd", $"%{d.canvasnbar_cd.GetValue()}%");
            param.Add("@canvasnbar_id", $"%{d.canvasnbar_id.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_canvasnbar WHERE " +
                "(@canvasnbar_cd IS NULL OR canvasnbar_cd like @canvasnbar_cd ) AND " +
                "(@canvasnbar_id IS NULL OR canvasnbar_id like @canvasnbar_id ) AND " +
                "(@txtsearch IS NULL OR canvasnbar_cd like @txtsearch OR canvasnbar_name like @txtsearch ) order by canvasnbar_id";

            return Query<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar> SearchActive(Model.Request.Standardpack.CanvasnBar.Master_s_CanvasnBarSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_cd", $"%{d.canvasnbar_cd.GetValue()}%");
            param.Add("@canvasnbar_id", $"%{d.canvasnbar_id.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_canvasnbar WHERE " +
                "(@canvasnbar_cd IS NULL OR canvasnbar_cd like @canvasnbar_cd ) AND " +
                "(@canvasnbar_id IS NULL OR canvasnbar_id like @canvasnbar_id ) AND " +
                "(@txtsearch IS NULL OR canvasnbar_cd like @txtsearch OR canvasnbar_name like @txtsearch ) AND status = 'A' order by canvasnbar_cd";

            return Query<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar>(cmd, param).ToList();
        }

        private int Insert(Model.Table.Mssql.CanvasnBar.master_s_canvasnbar d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);
            param.Add("@barcolor_cd", d.barcolor_cd);
            param.Add("@barcolor_name", d.barcolor_name);
            param.Add("@canvas_id", d.canvas_id);
            param.Add("@canvas_cd", d.canvas_cd);
            param.Add("@canvas_name", d.canvas_name);
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            param.Add("@canvasnbar_cd", d.canvasnbar_cd);
            param.Add("@canvasnbar_name", d.canvasnbar_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_canvasnbar " +
                "(canvasnbar_cd,canvasnbar_name,canvas_id,canvas_cd,canvas_name,barcolor_id, " +
                "barcolor_cd,barcolor_name,effdate_st,effdate_en,status,user_id,user_name,user_date) " +
                "VALUES " +
                "(@canvasnbar_cd,@canvasnbar_name,@canvas_id,@canvas_cd,@canvas_name,@barcolor_id" +
                ",@barcolor_cd,@barcolor_name,@effdate_st,@effdate_en,@status,@user_id,@user_name, GetDate()) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.CanvasnBar.master_s_canvasnbar d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);
            param.Add("@barcolor_cd", d.barcolor_cd);
            param.Add("@barcolor_name", d.barcolor_name);
            param.Add("@canvas_id", d.canvas_id);
            param.Add("@canvas_cd", d.canvas_cd);
            param.Add("@canvas_name", d.canvas_name);
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            param.Add("@canvasnbar_cd", d.canvasnbar_cd);
            param.Add("@canvasnbar_name", d.canvasnbar_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_canvasnbar " +
                "SET canvasnbar_cd= @canvasnbar_cd, canvasnbar_name =@canvasnbar_name, canvas_id = @canvas_id, canvas_cd =@canvas_cd, canvas_name = @canvas_name," +
                "barcolor_id = @barcolor_id,barcolor_cd = @barcolor_cd,barcolor_name = @barcolor_name,effdate_st = @effdate_st,effdate_en = @effdate_en,status = @status," +
                "user_id = @user_id,user_name = @user_name,user_date = Getdate() " +
                "WHERE canvasnbar_id = @canvasnbar_id SELECT @canvasnbar_id As canvasnbar_id";

            var canvasnbar_id = ExecuteScalar<int>(cmd, param);

            return canvasnbar_id;
        }
        public int Move(Model.Table.Mssql.CanvasnBar.master_s_canvasnbar d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvasnbar_id", d.canvasnbar_id);
            param.Add("@canvasnbar_cd", d.canvasnbar_cd);
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_u_canvasnbar " +
                "(canvasnbar_id,canvasnbar_cd,canvasnbar_name,canvas_id,canvas_cd,canvas_name,barcolor_id, " +
                "barcolor_cd,barcolor_name,effdate_st,effdate_en,status,user_id,user_name,user_date) " +
                "SELECT canvasnbar_id,canvasnbar_cd,canvasnbar_name,canvas_id,canvas_cd,canvas_name,barcolor_id, " +
                "barcolor_cd,barcolor_name,effdate_st,effdate_en,status,user_id,user_name,user_date FROM master_s_canvasnbar WHERE canvasnbar_id = @canvasnbar_id AND canvasnbar_cd = @canvasnbar_cd " +
                "UPDATE master_u_canvasnbar SET cancel_id = @user_id , cancel_name = @user_name , cancel_date = Getdate() ,status = 'C' " +
                "WHERE canvasnbar_id = @canvasnbar_id AND canvasnbar_cd = @canvasnbar_cd " +
                "UPDATE master_s_canvasnbar SET status = 'C' WHERE canvasnbar_id =@canvasnbar_id AND canvasnbar_cd = @canvasnbar_cd ";

            return ExecuteNonQuery(cmd, param);
        }

        public List<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar> SearchUsed(Model.Request.Standardpack.CanvasnBar.Master_s_CanvasnBarSearchUsedReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@barcolor_id", d.barcolor_id);
            param.Add("@canvas_id", d.canvas_id);

            string cmd = "SELECT * FROM master_s_canvasnbar WHERE " +
                "(@barcolor_id IS NULL OR barcolor_id = @barcolor_id AND status = 'A') OR " +
                "(@canvas_id IS NULL OR canvas_id = @canvas_id AND status = 'A') ";

            return Query<Model.Table.Mssql.CanvasnBar.master_s_canvasnbar>(cmd, param).ToList();
        }

    }
}
