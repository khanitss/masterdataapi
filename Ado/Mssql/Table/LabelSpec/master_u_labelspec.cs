﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ado.Mssql.Table.LabelSpec
{
    public class master_u_labelspec : Base
    {
        private static master_u_labelspec instant;

        public static master_u_labelspec GetInstant()
        {
            if (instant == null) instant = new master_u_labelspec();
            return instant;
        }
    }
}
