﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.LabelSpec
{
    public class master_s_labelspec : Base
    {
        private static master_s_labelspec instant;

        public static master_s_labelspec GetInstant()
        {
            if (instant == null) instant = new master_s_labelspec();
            return instant;
        }
        
        public List<Model.Table.Mssql.master_s_labelspec> Search(Model.Request.Standardpack.View_master_s_labelspecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", d.cuscod);

            string cmd = "SELECT * FROM master_s_labelspec " +
                "WHERE cus_cod = @cuscod";

            return Query<Model.Table.Mssql.master_s_labelspec>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.master_s_labelspec> SearchbyCus(Model.Request.Standardpack.View_master_s_labelspecSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@cuscod", $"%{d.cuscod.GetValue()}%");
            param.Add("@label_cd", $"%{d.label_cd.GetValue()}%");
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT DISTINCT(cus_cod),tsale,cus_name,status FROM master_s_labelspec " +
                "WHERE (@cuscod IS NULL OR cus_cod like @cuscod)" +
                "AND (@label_cd IS NULL OR label_cd like @label_cd)" +
                "AND (cus_cod like @txtSearch OR label_cd like @txtSearch)";

            return Query<Model.Table.Mssql.master_s_labelspec>(cmd, param).ToList();
        }


        public List<Model.Table.Mssql.master_s_labelspec> SearchByID(Model.Request.Standardpack.View_master_s_labelspecSearchReq d)

        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@txtSearch", d.txtSearch);

            string cmd = "SELECT * FROM master_s_labelspec WHERE labelspec_id = @txtSearch";

            return Query<Model.Table.Mssql.master_s_labelspec>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.master_s_labelspec> Save(Model.Request.Standardpack.LabelSpec.Master_s_labelspecSaveReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@tsale",d.tsale.GetValue());
            param.Add("@cus_id",d.cus_id);
            param.Add("@cus_cod", d.cus_cod.GetValue());
            param.Add("@cus_name", d.cus_name.GetValue());
            param.Add("@producttype_code", d.producttype_code.GetValue() );
            param.Add("@producttype_desc", d.producttype_desc.GetValue() );
            param.Add("@pqgrade_cd", d.pqgrade_cd.GetValue() );
            param.Add("@pqgrade_desc", d.pqgrade_desc.GetValue() );
            param.Add("@stdwei_min", d.stdwei_min);
            param.Add("@stdwei_max", d.stdwei_max);
            param.Add("@twinesize_min", d.twinesize_min.GetValue() );
            param.Add("@twinesize_max", d.twinesize_max.GetValue() );
            param.Add("@depsize_min", d.depsize_min);
            param.Add("@depsize_max", d.depsize_max);
            param.Add("@depamt_min", d.depamt_min);
            param.Add("@depamt_max", d.depamt_max);
            param.Add("@len_min", d.len_min);
            param.Add("@len_max", d.len_max);
            param.Add("@knottype_code", d.knottype_code.GetValue() );
            param.Add("@linetype_code", d.linetype_code.GetValue() );
            param.Add("@stretchingtype_code", d.stretchingtype_code.GetValue() );
            param.Add("@old_color_code", d.old_color_code.GetValue() );
            param.Add("@new_color_code", d.new_color_code.GetValue() );
            param.Add("@color_code", d.color_code.GetValue() );
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd.GetValue() );
            param.Add("@label_name", d.label_name.GetValue() );
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status.GetValue() );
            param.Add("@min_product_type", d.min_product_type.GetValue() );
            param.Add("@min_productcate_grp", d.min_productcate_grp.GetValue() );
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word.GetValue() );
            param.Add("@min_product_grp", d.min_product_grp.GetValue() );
            param.Add("@max_product_type", d.max_product_type.GetValue() );
            param.Add("@max_productcate_grp", d.max_productcate_grp.GetValue() );
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word.GetValue() );
            param.Add("@max_product_grp", d.max_product_grp.GetValue() );
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            // Table_1
            string cmd = "INSERT INTO master_s_labelspec " +
                "(tsale,cus_cod,producttype_code,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min,depsize_max,depamt_min" +
                ",depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,label_id,label_cd" +
                ",effdate_st,effdate_en,status,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type,max_productcate_grp" +
                ",max_twine_no,max_line_no,max_word,pqgradepd,user_id,user_name,user_date,cus_name , pqgrade_desc,producttype_desc,label_name)" +
                "VALUES" +
                "(@tsale,@cus_cod,@producttype_code,@stdwei_min,@stdwei_max,@twinesize_min,@twinesize_max,@depsize_min,@depsize_max,@depamt_min" +
                ",@depamt_max,@len_min,@len_max,@knottype_code,@linetype_code,@stretchingtype_code,@old_color_code,@new_color_code,@color_code,@label_id,@label_cd" +
                ",@effdate_st,@effdate_en,@status,@min_product_type,@min_productcate_grp,@min_twine_no,@min_line_no,@min_word,@min_product_grp,@max_product_type,@max_productcate_grp" +
                ",@max_twine_no,@max_line_no,@max_word,@pqgrade_cd,@user_id,@user_name,GetDate(),@cus_name, @pqgrade_desc,@producttype_desc, @label_name)";

            return Query<Model.Table.Mssql.master_s_labelspec>(cmd, param).ToList();
        }

        public List<Model.Table.Mssql.master_s_labelspec> Update(Model.Request.Standardpack.LabelSpec.Master_s_labelspecSaveReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@tsale", d.tsale.GetValue());
            param.Add("@cus_id", d.cus_id);
            param.Add("@cus_cod", d.cus_cod.GetValue());
            param.Add("@cus_name", d.cus_name.GetValue());
            param.Add("@producttype_code", d.producttype_code.GetValue());
            param.Add("@producttype_desc", d.producttype_desc.GetValue());
            param.Add("@pqgrade_cd", d.pqgrade_cd.GetValue());
            param.Add("@pqgrade_desc", d.pqgrade_desc.GetValue());
            param.Add("@stdwei_min", d.stdwei_min);
            param.Add("@stdwei_max", d.stdwei_max);
            param.Add("@twinesize_min", d.twinesize_min.GetValue());
            param.Add("@twinesize_max", d.twinesize_max.GetValue());
            param.Add("@depsize_min", d.depsize_min);
            param.Add("@depsize_max", d.depsize_max);
            param.Add("@depamt_min", d.depamt_min);
            param.Add("@depamt_max", d.depamt_max);
            param.Add("@len_min", d.len_min);
            param.Add("@len_max", d.len_max);
            param.Add("@knottype_code", d.knottype_code.GetValue());
            param.Add("@linetype_code", d.linetype_code.GetValue());
            param.Add("@stretchingtype_code", d.stretchingtype_code.GetValue());
            param.Add("@old_color_code", d.old_color_code.GetValue());
            param.Add("@new_color_code", d.new_color_code.GetValue());
            param.Add("@color_code", d.color_code.GetValue());
            param.Add("@label_id", d.label_id);
            param.Add("@label_cd", d.label_cd.GetValue());
            param.Add("@label_name", d.label_name.GetValue());
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status.GetValue());
            param.Add("@min_product_type", d.min_product_type.GetValue());
            param.Add("@min_productcate_grp", d.min_productcate_grp.GetValue());
            param.Add("@min_twine_no", d.min_twine_no);
            param.Add("@min_line_no", d.min_line_no);
            param.Add("@min_word", d.min_word.GetValue());
            param.Add("@min_product_grp", d.min_product_grp.GetValue());
            param.Add("@max_product_type", d.max_product_type.GetValue());
            param.Add("@max_productcate_grp", d.max_productcate_grp.GetValue());
            param.Add("@max_twine_no", d.max_twine_no);
            param.Add("@max_line_no", d.max_line_no);
            param.Add("@max_word", d.max_word.GetValue());
            param.Add("@max_product_grp", d.max_product_grp.GetValue());
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);
            param.Add("@labelspec_id ", d.labelspec_id);

            string cmd = "UPDATE master_s_labelspec SET " +
                 "tsale = @tsale," +
                "cus_cod = @cus_cod," +
                "cus_name = @cus_name," +
                "producttype_code =@producttype_code," +
                "producttype_desc = @producttype_desc," +
                "pqgradepd = @pqgrade_cd, " +
                "pqgrade_desc = @pqgrade_desc, " +
                "stdwei_min = @stdwei_min," +
                "stdwei_max = @stdwei_max," +
                "twinesize_min = @twinesize_min," +
                "twinesize_max = @twinesize_max," +
                "depsize_min = @depsize_min" +
                ",depsize_max = @depsize_max" +
                ",depamt_min =@depamt_min" +
                ",depamt_max =@depamt_max," +
                "len_min = @len_min," +
                "len_max = @len_max" +
                ",knottype_code = @knottype_code" +
                ",linetype_code = @linetype_code," +
                "stretchingtype_code = @stretchingtype_code," +
                "old_color_code = @old_color_code," +
                "new_color_code = @new_color_code," +
                "color_code =@color_code," +
                "label_id = @label_id," +
                "label_cd = @label_cd," +
                "label_name = @label_name," +
                "effdate_st =@effdate_st" +
                ",effdate_en = @effdate_en" +
                ",status =@status" +
                ",min_product_type = @min_product_type" +
                ",min_productcate_grp = @min_productcate_grp" +
                ",min_twine_no = @min_twine_no," +
                "min_line_no = @min_line_no," +
                "min_word = @min_word," +
                "min_product_grp =@min_product_grp," +
                "max_product_type = @max_product_type," +
                "max_productcate_grp =@max_productcate_grp" +
                ",max_twine_no = @max_twine_no," +
                "max_line_no = @max_line_no," +
                "max_word = @max_word," +
                "user_id = @user_id," +
                "user_name = @user_name," +
                "user_date = GetDate() " +
                "WHERE labelspec_id = @labelspec_id ";

            return Query<Model.Table.Mssql.master_s_labelspec>(cmd,param).ToList();
        }

        public int Move(Model.Table.Mssql.master_s_labelspec d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@labelspec_id", d.labelspec_id);
            param.Add("@cancel_id", d.user_id);
            param.Add("@cancel_name", d.user_name);

            string cmd = "INSERT INTO master_u_labelspec" +
                "(tsale,cus_cod,producttype_code,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min,depsize_max,depamt_min" +
                ",depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,label_id,label_cd" +
                ",effdate_st,effdate_en,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type,max_productcate_grp" +
                ",max_twine_no,max_line_no,max_word,cancel_date,labelspec_id,user_id,user_name,user_date) " +
                "SELECT " +
                "tsale,cus_cod,producttype_code,stdwei_min,stdwei_max,twinesize_min,twinesize_max,depsize_min,depsize_max,depamt_min" +
                ",depamt_max,len_min,len_max,knottype_code,linetype_code,stretchingtype_code,old_color_code,new_color_code,color_code,label_id,label_cd" +
                ",effdate_st,effdate_en,min_product_type,min_productcate_grp,min_twine_no,min_line_no,min_word,min_product_grp,max_product_type,max_productcate_grp" +
                ",max_twine_no,max_line_no,max_word,GetDate(),@labelspec_id,user_id,user_name,user_date " +
                "FROM master_s_labelspec " +
                "WHERE labelspec_id = @labelspec_id " +
                "UPDATE master_u_labelspec SET cancel_id = @cancel_id , cancel_name =@cancel_name, status ='C' WHERE labelspec_id = @labelspec_id " +
                "UPDATE master_s_labelspec SET status = 'C' WHERE labelspec_id = @labelspec_id ";

            return ExecuteNonQuery(cmd, param);
        }

    }
}
