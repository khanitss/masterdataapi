﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Canvas
{
    public class master_u_canvas : Base
    {
        private static master_u_canvas instant;

        public static master_u_canvas GetInstant()
        {
            if (instant == null) instant = new master_u_canvas();
            return instant;
        }
        public List<Model.Table.Mssql.Canvas.master_u_canvas> Search(Model.Request.Standardpack.Canvas.Master_s_CanvasSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            //param.Add("@barcolor_id", $"%{d.barcolor_id.GetValue()}%");
            param.Add("@canvas_cd", $"%{d.canvas_cd.GetValue()}%");
            param.Add("@txtsearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_u_canvas WHERE " +
                //"(@barcolor_id IS NULL OR barcolor_id like @barcolor_id) AND" +
                "(@canvas_cd IS NULL OR canvas_cd like @canvas_cd) AND" +
                "(@txtSearch IS NULL OR canvas_cd like @txtSearch OR canvas_cd like @txtSearch) order by canvas_cd";

            return Query<Model.Table.Mssql.Canvas.master_u_canvas > (cmd, param).ToList();
        }
    }
}
