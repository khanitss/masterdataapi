﻿using Core.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ado.Mssql.Table.Canvas
{
    public class master_s_canvas : Base
    {
        private static master_s_canvas instant;

        public static master_s_canvas GetInstant()
        {
            if (instant == null) instant = new master_s_canvas();
            return instant;
        }
        public Model.Table.Mssql.Canvas.master_s_canvas Get(Model.Table.Mssql.Canvas.master_s_canvas d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_id", d.canvas_id);

            string cmd = "SELECT * FROM master_s_canvas WHERE canvas_id = @canvas_id ORDER BY canvas_id";

            var res = Query<Model.Table.Mssql.Canvas.master_s_canvas>(cmd, param).FirstOrDefault();
            return res;
        }
        public int Save(Model.Table.Mssql.Canvas.master_s_canvas d)
        {
            var item = Get(new Model.Table.Mssql.Canvas.master_s_canvas
            {
                canvas_id = d.canvas_id
            });
            if (item != null )
            {
                return Update(d);
            }
            else
            {
                return Insert(d);
            }
        }
        public List<Model.Table.Mssql.Canvas.master_s_canvas> Search(Model.Request.Standardpack.Canvas.Master_s_CanvasSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_cd", $"%{d.canvas_cd.GetValue()}%");
            param.Add("@canvas_id", d.canvas_id);
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_canvas WHERE " +
                "(@canvas_cd IS NULL OR canvas_cd like @canvas_cd) AND " +
                "(canvas_cd like @txtSearch OR canvas_name like @txtSearch) order by canvas_cd";

            return Query<Model.Table.Mssql.Canvas.master_s_canvas>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.Canvas.master_s_canvas> SearchView(Model.Request.Standardpack.Canvas.Master_s_CanvasSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_cd", $"%{d.canvas_cd.GetValue()}%");
            param.Add("@canvas_id", d.canvas_id);
            param.Add("@txtSearch", $"%{d.txtSearch.GetValue()}%");

            string cmd = "SELECT * FROM master_s_canvas WHERE " +
                "(@canvas_cd IS NULL OR canvas_cd like @canvas_cd) AND " +
                "(canvas_cd like @txtSearch OR canvas_name like @txtSearch) order by canvas_id";

            return Query<Model.Table.Mssql.Canvas.master_s_canvas>(cmd, param).ToList();
        }
        public List<Model.Table.Mssql.Canvas.master_s_canvas> SearchID(Model.Request.Standardpack.Canvas.Master_s_CanvasSearchReq d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_id", d.canvas_id);

            string cmd = "SELECT * FROM master_s_canvas WHERE canvas_id = @canvas_id";

            return Query<Model.Table.Mssql.Canvas.master_s_canvas>(cmd, param).ToList();
        }
        private int Insert(Model.Table.Mssql.Canvas.master_s_canvas d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_cd", d.canvas_cd);
            param.Add("@canvas_name", d.canvas_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@itemcode", d.itemcode);
            param.Add("@status", d.status);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "INSERT INTO master_s_canvas " +
                "(canvas_cd,canvas_name,effdate_st,effdate_en,itemcode,path_pic,user_id,user_name,user_date,status) " +
                "VALUES ( @canvas_cd,@canvas_name,@effdate_st,@effdate_en,@itemcode,@path_pic,@user_id,@user_name,Getdate(),@status) " +
                "SELECT SCOPE_IDENTITY(); ";

            return ExecuteScalar<int>(cmd, param);
        }
        private int Update(Model.Table.Mssql.Canvas.master_s_canvas d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_id", d.canvas_id.ToString());
            param.Add("@canvas_cd", d.canvas_cd);
            param.Add("@canvas_name", d.canvas_name);
            param.Add("@effdate_st", d.effdate_st);
            param.Add("@effdate_en", d.effdate_en);
            param.Add("@status", d.status);
            param.Add("@itemcode", d.itemcode);
            param.Add("@path_pic", Core.Util.FileUtil.SaveImageFile(d.path_pic));
            param.Add("@user_id", d.user_id);
            param.Add("@user_name", d.user_name);

            string cmd = "UPDATE master_s_canvas " +
                "SET canvas_cd = @canvas_cd , canvas_name= @canvas_name, effdate_st= @effdate_st, effdate_en = @effdate_en, " +
                "itemcode= @itemcode , path_pic = @path_pic , user_id =@user_id , user_name = @user_name , user_date = GetDate() " +
                "WHERE canvas_id =@canvas_id SELECT @canvas_id AS canvas_id ";

            var canvas_id = ExecuteScalar<int>(cmd, param);
           

            return canvas_id;
        }

        public int Move(Model.Table.Mssql.Canvas.master_s_canvas d)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@canvas_id", d.canvas_id);
            param.Add("@canvas_cd", d.canvas_cd);
            param.Add("@cancel_id", d.user_id);
            param.Add("@cancel_name", d.user_name);

            string cmd = "INSERT INTO master_u_canvas " +
                "(canvas_id , canvas_cd , canvas_name, effdate_st , effdate_en , itemcode , path_pic , user_id , user_name , user_date ) " +
                "SELECT canvas_id, canvas_cd , canvas_name , effdate_st , effdate_en , itemcode , path_pic , user_id , user_name , user_date " +
                "FROM master_s_canvas WHERE canvas_id = @canvas_id AND canvas_cd = @canvas_cd " +
                "UPDATE master_u_canvas SET cancel_id = @cancel_id , cancel_name = @cancel_name , cancel_date = GetDate(), status = 'C' WHERE  canvas_id = @canvas_id AND canvas_cd = @canvas_cd " +
                "UPDATE master_s_canvas SET status = 'C' WHERE canvas_id = @canvas_id AND canvas_cd = @canvas_cd";
            return ExecuteNonQuery(cmd, param);
        }
    }
}
