﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Util
{
    public static class FileUtil
    {
        private static string pathImg = "imgs";

        public static string GetImageBase64(string imgUrl)
        {
            string imageBase64 = "";
            using (WebClient client = new WebClient())
            {
                byte[] imageByteData = client.DownloadData(imgUrl);
                imageBase64 = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(imageByteData));
            }
            return imageBase64;
        }

        public static string SaveImageFile(string base64Image)
        {
            string filename = EncryptUtil.MD5(Guid.NewGuid().ToString());

            //string Path = Environment.CurrentDirectory + "\\wwwroot\\" + pathImg + "\\";
            string Path = "C:\\inetpub\\masterdata\\masterdataapp" + "\\wwwroot\\" + pathImg + "\\";
            

            try
            {
                Directory.CreateDirectory(Path);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (!string.IsNullOrWhiteSpace(base64Image))
            {
                string type = "jpg";// Regex.Replace(base64Image, @"\b^data:image/([a-zA-Z]+).*$", "$1");
                var tmp = base64Image.Split(",");
                if (tmp.Length == 2)
                {
                    Path += filename + "." + type;
                    var bytes = Convert.FromBase64String(tmp[tmp.Length - 1]);
                    using (var imageFile = new FileStream(Path, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                    return "/" + pathImg + "/" + filename + "." + type;
                }
            }

            return base64Image;
        }

        public static string SaveImageFile(string base64Image, string pathImg, string filename)
        {

            string Path = Environment.CurrentDirectory + "\\wwwroot\\" + pathImg + "\\";

            try
            {
                Directory.CreateDirectory(Path);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (!string.IsNullOrWhiteSpace(base64Image))
            {
                string type = "jpg";// Regex.Replace(base64Image, @"\b^data:image/([a-zA-Z]+).*$", "$1");
                var tmp = base64Image.Split(",");
                if (tmp.Length == 2)
                {
                    Path += filename + "." + type;
                    var bytes = Convert.FromBase64String(tmp[tmp.Length - 1]);
                    using (var imageFile = new FileStream(Path, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                    return "/" + pathImg + "/" + filename + "." + type;
                }
            }

            return base64Image;
        }
    }
}
